package br.com.bioz4life.biomonitor.utils;

import java.util.Random;

public class NumberGenerator {

    public static String generateNumber(){

        final int min = 1000;
        final int max = 9999;
        return String.valueOf(new Random().nextInt(max - min) + min);
    }
}
