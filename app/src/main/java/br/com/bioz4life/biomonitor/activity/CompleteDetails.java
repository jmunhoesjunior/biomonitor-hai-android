package br.com.bioz4life.biomonitor.activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.adapter.AdapterBg;
import br.com.bioz4life.biomonitor.adapter.AdapterBp;
import br.com.bioz4life.biomonitor.adapter.AdapterBpHr;
import br.com.bioz4life.biomonitor.adapter.AdapterBt;
import br.com.bioz4life.biomonitor.adapter.AdapterSPO2;
import br.com.bioz4life.biomonitor.adapter.AdapterSPO2Hr;
import br.com.bioz4life.biomonitor.database.RealmUtilityBg;
import br.com.bioz4life.biomonitor.database.RealmUtilityBp;
import br.com.bioz4life.biomonitor.database.RealmUtilityBt;
import br.com.bioz4life.biomonitor.database.RealmUtilitySPO2;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class CompleteDetails extends BaseActivity {

    private Realm realm;

    private String type;

    @BindView(R.id.rvDetails)
    RecyclerView rvDetails;

    @BindView(R.id.tvType)
    TextView tvType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_complete_details);

        ButterKnife.bind(this);


        realm = Realm.getDefaultInstance();

        type = getIntent().getStringExtra("type");
        rvDetails.setLayoutManager(new LinearLayoutManager(this));

        loadData();
    }

    private void loadData(){
        switch (type){
            case "bp":
                setUpActionBar("Histórico Pressão Arterial (mmHg)", getString(R.string.app_name), true);
                tvType.setText("SBP/DBP\n(mmHg/mmHg)");
                AdapterBp adapterBp = new AdapterBp(CompleteDetails.this,RealmUtilityBp.getAll(realm, LocalPrefs.getPhoneNumber(this)));
                rvDetails.setAdapter(adapterBp);
                break;

            case "bt":
                setUpActionBar("Histórico Temperatura (ºC)", getString(R.string.app_name), true);
                tvType.setText("Temperatura\n(ºC)");
                AdapterBt adapterBt = new AdapterBt(CompleteDetails.this,RealmUtilityBt.getAll(realm, LocalPrefs.getPhoneNumber(this)));
                rvDetails.setAdapter(adapterBt);
                break;

            case "bp_hr":
                setUpActionBar("Histórico Freq. Cardíaca (bpm)", getString(R.string.app_name), true);
                tvType.setText("Freq. Cardíaca\n(bpm)");
                AdapterBpHr adapterBphr = new AdapterBpHr(CompleteDetails.this,RealmUtilityBp.getAll(realm, LocalPrefs.getPhoneNumber(this)));
                rvDetails.setAdapter(adapterBphr);
                break;

            case "spo2":
                setUpActionBar("Histórico SpO₂ (%)", getString(R.string.app_name), true);
                tvType.setText("SpO₂\n(%)");
                AdapterSPO2 adapterSPO2 = new AdapterSPO2(CompleteDetails.this, RealmUtilitySPO2.getAll(realm, LocalPrefs.getPhoneNumber(this)));
                rvDetails.setAdapter(adapterSPO2);
                break;

            case "bg":
                setUpActionBar("Histórico Glicose (mmol/L)", getString(R.string.app_name), true);
                tvType.setText("Glicose\n(mmol/L)");
                AdapterBg adapterBg = new AdapterBg(CompleteDetails.this, RealmUtilityBg.getAll(realm, LocalPrefs.getPhoneNumber(this)));
                rvDetails.setAdapter(adapterBg);
                break;

            case "spo2_hr":
                setUpActionBar("Histórico Freq. Cardíaca (bpm)", getString(R.string.app_name), true);
                tvType.setText("Freq. Cardíaca\n(bpm)");
                AdapterSPO2Hr adapterSPO2Hr = new AdapterSPO2Hr(CompleteDetails.this, RealmUtilitySPO2.getAll(realm, LocalPrefs.getPhoneNumber(this)));
                rvDetails.setAdapter(adapterSPO2Hr);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();

        super.onDestroy();
    }
}
