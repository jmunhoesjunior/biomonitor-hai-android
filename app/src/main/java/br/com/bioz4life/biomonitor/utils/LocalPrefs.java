package br.com.bioz4life.biomonitor.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalPrefs {

    private static final String key_prefs = "prefs";


    public static void storePhoneNumber(Context mContext, String value){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(key_prefs, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("userPhone",value).commit();
    }
    public static String getPhoneNumber(Context mContext){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(key_prefs, Context.MODE_PRIVATE);
        return sharedPreferences.getString("userPhone","");
    }


    public static void storeValue(Context mContext, String value,String key){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(key_prefs, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(key,value).commit();
    }
    public static String getValue(Context mContext,String key){
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(key_prefs, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key,"");
    }
}
