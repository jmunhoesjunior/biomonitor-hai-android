package br.com.bioz4life.biomonitor.carenet;

import br.com.bioz4life.biomonitor.carenet.model.CarenetSender;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ICarenet {
    @POST("api/v2/vital-sign")
    Call<ResponseBody> add(@Body CarenetSender request);
}
