package br.com.bioz4life.biomonitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.ECG;
import br.com.bioz4life.biomonitor.widget.EcgWaveView;
import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by ccl on 2017/2/8.
 */

public class ECGLargeActivity extends BaseActivity {

    @BindView(R.id.ewv_ecg_large)
    EcgWaveView ewvECGLarge;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ecg_large);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        int pagerSpeed = intent.getIntExtra("pagerSpeed", 1);
        float gain = intent.getFloatExtra("gain", 1.0f);
        ECG model = (ECG) intent.getSerializableExtra("model");

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Eletrocardiograma");
            actionBar.setDisplayHomeAsUpEnabled(true);
            if (model != null) {
                if (model.getCreatedDate() != null) {
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM HH:mm");

                    //to convert Date to String, use format method of SimpleDateFormat class.
                    String strDate = dateFormat.format(model.getCreatedDate());
                    actionBar.setSubtitle(strDate);
                }
            }
        }

        ewvECGLarge.setPagerSpeed(pagerSpeed);
        ewvECGLarge.setGain(gain);

        showChart(model.getWave());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showChart(String wave) {
        Observable.just(wave)
                .subscribeOn(Schedulers.newThread()) // 指定 subscribe() 发生在子线程
                .map(new Func1<String, String[]>() {
                    @Override
                    public String[] call(String s) {
                        return s.split(",");
                    }
                })
                .map(new Func1<String[], List<Float>>() {
                    @Override
                    public List<Float> call(String[] strings) {
                        final List<Float> list = new ArrayList<>();
                        for (String str : strings) {
                            list.add(Float.valueOf(str));
                        }
                        return list;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())// 指定 Subscriber 的回调发生在UI程
                .subscribe(new Subscriber<List<Float>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Float> list) {
                        ewvECGLarge.preparePoints(list);
                    }
                });

    }
}
