package br.com.bioz4life.biomonitor.activity;

import android.view.Menu;
import android.view.MenuItem;

import br.com.bioz4life.biomonitor.R;

public abstract class ExtentionMenuContact extends BaseActivity {

    public abstract void onClickAddContact();
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        }
        else if(itemId == R.id.add_contact){
            onClickAddContact();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
