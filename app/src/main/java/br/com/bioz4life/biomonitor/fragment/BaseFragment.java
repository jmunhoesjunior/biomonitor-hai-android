package br.com.bioz4life.biomonitor.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import br.com.bioz4life.biomonitor.activity.HealthMonitorActivity;
import br.com.bioz4life.biomonitor.health.App;
import br.com.bioz4life.biomonitor.health.HcService;
import br.com.bioz4life.biomonitor.utils.UToast;
import butterknife.ButterKnife;

/**
 * Created by ccl on 2017/2/7.
 */

public abstract class BaseFragment extends Fragment {

    protected HealthMonitorActivity mActivity;
    protected HcService mHcService;

    public BaseFragment() {
    }

    public abstract String getTitle();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  onCreateBindingView(inflater, container, savedInstanceState).getRoot();
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mActivity = (HealthMonitorActivity) getActivity();
        if (App.isUseCustomBleDevService)
            mHcService = mActivity.mHcService;
    }

    protected ViewDataBinding onCreateBindingView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }

    protected <T extends ViewDataBinding> T setBindingContentView(LayoutInflater inflater, int layoutId, @Nullable ViewGroup parent) {
        return DataBindingUtil.inflate(inflater, layoutId, parent, false);
    }

    public abstract void reset();


    protected void toast(@NonNull String text) {
        UToast.show(getActivity(), text);
    }
}
