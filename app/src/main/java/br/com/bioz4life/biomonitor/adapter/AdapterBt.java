package br.com.bioz4life.biomonitor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.Bt;

public class AdapterBt extends RecyclerView.Adapter {

    private Context mContext;
    private List<Bt> mItems;

    private class Holder extends RecyclerView.ViewHolder {

        TextView tvData;
        TextView tvValues;
        TextView tvStatus;

        public Holder(@NonNull View itemView) {
            super(itemView);

            tvData = (TextView)itemView.findViewById(R.id.tvData);
            tvValues = (TextView) itemView.findViewById(R.id.tvValues);
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
        }
    }
    public AdapterBt(Context ctx, List<Bt> items){
        this.mContext = ctx;
        this.mItems = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(mContext)
                .inflate(R.layout.item_bt, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder,final int i) {

        Holder holder = (Holder)viewHolder;
        Bt bt = mItems.get(i);

        holder.tvStatus.setText("");
        holder.tvValues.setText("");
        holder.tvData.setText("");

        if (bt.getCreatedDate() != null) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM HH:mm");

            //to convert Date to String, use format method of SimpleDateFormat class.
            String strDate = dateFormat.format(bt.getCreatedDate());
            holder.tvData.setText(strDate);
        }

        holder.tvValues.setText(String.valueOf(bt.getTemp()));

        String tempStatus = "";
        if(bt.getTemp() >=36.5 && bt.getTemp() <= 37.5){
            tempStatus = "Normal";
        }
        else{
            tempStatus = "Irregular";
        }
        holder.tvStatus.setText(tempStatus);

    }

    @Override
    public int getItemCount() {
        if(mItems == null)
            return 0;

        return mItems.size();
    }
}
