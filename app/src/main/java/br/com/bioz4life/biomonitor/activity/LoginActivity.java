package br.com.bioz4life.biomonitor.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.utils.UToast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by ccl on 2017/2/15.
 */
public class LoginActivity extends BaseActivity {

    @BindView(R.id.etCountry)
    TextInputEditText etCountry;

    @BindView(R.id.etDDD)
    TextInputEditText etDDD;

    @BindView(R.id.etPhone)
    TextInputEditText etPhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        setUpActionBar("Login", getString(R.string.app_name), false);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        etCountry.setText("+55");

    }

    /**
     * Evento de cliques de login (não registre a versão com o servidor)
     */
    @OnClick(R.id.btLogin)
    public void doLogin(View v) {

        if(etCountry.getText().toString().equals("")){
            UToast.show(this,"O campo código do país é obrigatório");
        }
        else if(etDDD.getText().toString().equals("") || etDDD.getText().toString().length() != 2){
            UToast.show(this,"O campo código DDD é obrigatório");

        }
        else if(etPhone.getText().toString().equals("")){
            UToast.show(this,"O campo Telefone é obrigatório");
        }
        else{

            showProgressDialog(getResources().getString(R.string.sending_sms));

            String phone = etCountry.getText().toString() + etDDD.getText().toString() + etPhone.getText().toString();
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    phone,        // Phone number to verify
                    60,                 // Timeout duration
                    TimeUnit.SECONDS,   // Unit of timeout
                    this,
                    new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                        @Override
                        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {

                        }

                        @Override
                        public void onVerificationFailed(@NonNull FirebaseException e) {
                            dismissProgressDialog();

                            if (e.getMessage() != null){
                                UToast.show(LoginActivity.this,e.getMessage());
                            }
                        }

                        @Override
                        public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                            super.onCodeSent(verificationId, forceResendingToken);

                            dismissProgressDialog();

                            Intent intent = new Intent(LoginActivity.this, VerificationCodeActivity.class);
                            intent.putExtra("verificationId",verificationId);
                            intent.putExtra("phone", phone);
                            startActivity(intent);
                        }
                    });
        }
    }
}
