package br.com.bioz4life.biomonitor.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.linktop.MonitorDataTransmissionManager;
import com.linktop.infs.OnBpResultListener;
import com.linktop.whealthService.MeasureType;
import com.linktop.whealthService.task.BpTask;
import com.timqi.sectorprogressview.ColorfulRingProgressView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.Bp;
import br.com.bioz4life.biomonitor.database.BpRealm;
import br.com.bioz4life.biomonitor.database.RealmUtilityBp;
import br.com.bioz4life.biomonitor.databinding.FragmentBpBinding;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.UToast;
import br.com.bioz4life.biomonitor.utils.Utility;
import butterknife.BindView;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by ccl on 2017/2/7.
 * Interface de medição de pressão arterial
 */

public class BpFragment extends MeasureFragment
        implements OnBpResultListener {

    private Bp model;
    private BpTask mBpTask;

    @BindView(R.id.crpv)
    ColorfulRingProgressView crpv;

    @BindView(R.id.seekSBP)
    SeekBar seekSBP;

    @BindView(R.id.seekDBP)
    SeekBar seekDBP;

    @BindView(R.id.tvSBP)
    TextView tvSBP;

    @BindView(R.id.tvDBP)
    TextView tvDBP;

    @BindView(R.id.tvBPM)
    TextView tvBPM;

    @BindView(R.id.btSyncData)
    Button btSyncData;

    public BpFragment() {
    }

    @Override
    public boolean startMeasure() {

        btSyncData.setEnabled(false);

        if (mBpTask != null) {
            if (mHcService.getBleDevManager().getBatteryTask().getPower() < 20) {
                toast("O dispositivo está acabando, por favor carregue.");
                return false;
            }
            seekSBP.setProgress(0);
            seekDBP.setProgress(0);
            tvBPM.setText(String.valueOf(0));
            tvSBP.setText(String.valueOf(0));
            tvDBP.setText(String.valueOf(0));

            crpv.animateIndeterminate();
            mBpTask.start();
        }
        else {
            if (MonitorDataTransmissionManager.getInstance().getBatteryValue() < 20) {
                toast("O dispositivo está acabando, por favor carregue.");
                return false;
            }

            seekSBP.setProgress(0);
            seekDBP.setProgress(0);
            tvBPM.setText(String.valueOf(0));
            tvSBP.setText(String.valueOf(0));
            tvDBP.setText(String.valueOf(0));
            crpv.animateIndeterminate();
            MonitorDataTransmissionManager.getInstance().startMeasure(MeasureType.BP);
        }
        return true;
    }

    @Override
    public void stopMeasure() {

        btSyncData.setEnabled(true);

        if (mBpTask != null) {
            crpv.stopAnimateIndeterminate();
            mBpTask.stop();
        }
        else {
            crpv.stopAnimateIndeterminate();
            MonitorDataTransmissionManager.getInstance().stopMeasure();
        }
    }

    @Override
    public String getTitle() {
        return "Pressão Arterial";
    }

    @Override
    protected ViewDataBinding onCreateBindingView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentBpBinding binding = setBindingContentView(inflater, R.layout.fragment_bp, container);
        binding.setContent(this);
        this.btnMeasure = binding.btnMeasure;
        model = new Bp();
        binding.setModel(model);
        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mHcService != null) {
            mBpTask = mHcService.getBleDevManager().getBpTask();
            mBpTask.setOnBpResultListener(this);
        }
        else {
            //Definir a interface de retorno de chamada de medição da pressão arterial
            MonitorDataTransmissionManager.getInstance().setOnBpResultListener(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void reset() {
        model.reset();
    }

    @Override
    public void onBpResult(final int systolicPressure, final int diastolicPressure, final int heartRate) {

        // Tempo de medição (incluindo o tempo de medição de outros itens de medição desta demo),
        // que pode ser baseado no momento em que o botão é clicado para iniciar o teste.
        // também pode ser baseado nos resultados da medição, ver como os requisitos são definidos
        // A demo de demonstração aqui, por conveniência, esta última.
        model.setTs(System.currentTimeMillis() / 1000L);
        model.setSbp(systolicPressure);
        model.setDbp(diastolicPressure);
        model.setHr(heartRate);

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btSyncData.setEnabled(true);

                try{
                    if(model.getSbp() > 250)
                        seekSBP.setProgress(250);
                    else
                        seekSBP.setProgress((int)model.getSbp());

                    if(model.getDbp() > 150)
                        seekDBP.setProgress(150);
                    else
                        seekDBP.setProgress((int)model.getDbp());

                    tvBPM.setText(String.valueOf(model.getHr()));
                    tvSBP.setText(String.valueOf(model.getSbp()));
                    tvDBP.setText(String.valueOf(model.getDbp()));

                    // Salvar no banco esta amostra
                    BpRealm bpRealm = new BpRealm();
                    bpRealm.id = UUID.randomUUID().toString();
                    bpRealm.createdDate = new Date();
                    bpRealm.user = LocalPrefs.getPhoneNumber(mActivity);
                    bpRealm.dbp = model.getDbp();
                    bpRealm.hr = model.getHr();
                    bpRealm.sbp = model.getSbp();
                    bpRealm.ts = model.getTs();
                    mActivity.getRealm().beginTransaction();
                    mActivity.getRealm().copyToRealm(bpRealm);
                    mActivity.getRealm().commitTransaction();
                }
                finally {
                    crpv.stopAnimateIndeterminate();
                    resetState();
                }
            }
        });
    }

    @Override
    public void onBpResultError() {
        crpv.stopAnimateIndeterminate();
        btSyncData.setEnabled(true);

        resetState();
        Toast.makeText(getContext(), getString(R.string.cant_complete), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLeakError(int errorType) {
        resetState();
        Observable.just(errorType)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Integer>() {
                    @Override
                    public void call(Integer error) {

                        crpv.stopAnimateIndeterminate();
                        btSyncData.setEnabled(true);

                        int textId = 0;
                        switch (error) {
                            case 0:
                                textId = R.string.leak_and_check;
                                break;
                            case 1:
                                textId = R.string.measurement_void;
                                break;
                            default:
                                break;
                        }
                        if (textId != 0)
                            Toast.makeText(getContext(), getString(textId), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void sendData(){
        mActivity.showProgressDialog(getResources().getString(R.string.sending_data));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        final Bp item = RealmUtilityBp.getUnsyncBp(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        String createDate = df.format("yyyy-MM-dd HH:mm:ss", item.getCreatedDate()).toString();

        Map<String, Object> map = new HashMap<>();
        map.put("ts",item.getTs());
        map.put("sbp",item.getSbp());
        map.put("dbp",item.getDbp());
        map.put("hr",item.getHr());
        map.put("device_id",LocalPrefs.getValue(mActivity,mActivity.device_id));
        map.put("device_key",LocalPrefs.getValue(mActivity,mActivity.device_key));
        map.put("createdDate",createDate);

        db.collection("biomonitor")
                .document(LocalPrefs.getPhoneNumber(mActivity))
                .collection("exams_bp").document(item.getId()).set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                RealmUtilityBp.updateStatus(mActivity.getRealm(), item.getId(), LocalPrefs.getPhoneNumber(mActivity));

                mActivity.dismissProgressDialog();

                syncData();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mActivity.dismissProgressDialog();
                new AlertDialogBuilder(mActivity)
                        .setTitle("Erro")
                        .setMessage(e.getMessage())
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
    }

    @OnClick(R.id.btSyncData)
    public void syncData(){

        if (Utility.isOnline()) {

            Bp bpToBeSyncronized = RealmUtilityBp.getUnsyncBp(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

            //1.1 Sincronizar um por um
            if (bpToBeSyncronized != null) {
                sendData();
            }
            else{
                //1.2 Atualizar lista
                mActivity.showProgressDialog(getResources().getString(R.string.loading_data));

                String userPhone = LocalPrefs.getPhoneNumber(mActivity);

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                CollectionReference collectionReference = db.collection("biomonitor")
                        .document(userPhone)
                        .collection("exams_bp");

                collectionReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mActivity.dismissProgressDialog();

                            // apagar
                            RealmUtilityBp.deleteDataSyncronized(mActivity.getRealm(),userPhone);

                            int count = task.getResult().size();

                            if (count > 0){
                                ArrayList<Bp> exams = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    //Log.d(TAG, document.getId() + " => " + document.getData());

                                    Map<String, Object> map = document.getData();

                                    Bp item =  new Bp();
                                    item.setId(document.getId());
                                    item.setTs((long) map.get("ts"));
                                    item.setSbp((long) map.get("sbp"));
                                    item.setDbp((long) map.get("dbp"));
                                    item.setHr((long) map.get("hr"));
                                    Date createdDate = Utility.stringToDate((String) map.get("createdDate"),"yyyy-MM-dd HH:mm:ss");
                                    item.setCreatedDate(createdDate);

                                    exams.add(item);
                                }
                                // add data do server
                                RealmUtilityBp.addFromServer(mActivity.getRealm(),exams,userPhone);
                            }
                        }
                        else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                            mActivity.dismissProgressDialog();

                            UToast.show(mActivity,task.getException().getMessage());
                        }
                    }
                });
            }
        }
        else{
            UToast.show(getActivity(),getResources().getString(R.string.no_connection));
        }
    }
}
