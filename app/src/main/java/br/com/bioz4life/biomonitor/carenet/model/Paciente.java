package br.com.bioz4life.biomonitor.carenet.model;

import java.io.Serializable;
import java.util.List;

public class Paciente implements Serializable {
    public String pid;
    public String nome;
    public String sexo;
    public String dataNascimento;
    public List<Resultado> resultados;
}
