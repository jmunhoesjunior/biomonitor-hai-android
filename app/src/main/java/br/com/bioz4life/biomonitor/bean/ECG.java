package br.com.bioz4life.biomonitor.bean;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ccl on 2017/2/8.
 */

public class ECG implements Serializable {

    private String id;

    private Date createdDate;


    private long ts;
    private long duration;
    private long rrMax;
    private long rrMin;
    private long hr;
    private long hrv;
    private long mood;
    private long br;
    private String wave;
    private long pagerSpeed;
    private double gain;

    public ECG() {
    }

    public ECG(long ts, long duration, long rrMax, long rrMin, long hr, long hrv, long mood, long br, String wave,long pagerSpeed,double gain) {
        this.ts = ts;
        this.duration = duration;
        this.rrMax = rrMax;
        this.rrMin = rrMin;
        this.hr = hr;
        this.hrv = hrv;
        this.mood = mood;
        this.br = br;
        this.wave = wave;
        this.pagerSpeed = pagerSpeed;
        this.gain = gain;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public long getPagerSpeed() {
        return pagerSpeed;
    }

    public void setPagerSpeed(long pagerSpeed) {
        this.pagerSpeed = pagerSpeed;
    }

    public double getGain() {
        return gain;
    }

    public void setGain(double gain) {
        this.gain = gain;
    }

    public long getRrMax() {
        return rrMax;
    }


    public void setRrMax(long rrMax) {
        this.rrMax = rrMax;
        //notifyChange();
    }


    public long getRrMin() {
        return rrMin;
    }


    public void setRrMin(long rrMin) {
        this.rrMin = rrMin;
        //notifyChange();
    }


    public long getHr() {
        return hr;
    }


    public void setHr(long hr) {
        this.hr = hr;
        //notifyChange();
    }


    public long getHrv() {
        return hrv;
    }


    public void setHrv(long hrv) {
        this.hrv = hrv;
        //notifyChange();
    }


    public long getMood() {
        return mood;
    }

    public void setMood(long mood) {
        this.mood = mood;
       // notifyChange();
    }

    public long getBr() {
        return br;
    }


    public void setBr(long br) {
        this.br = br;
        //notifyChange();
    }


    public long getDuration() {
        return duration;
    }


    public void setDuration(long duration) {
        this.duration = duration;
    }


    public void setWave(String wave) {
        this.wave = wave;
    }


    public String getWave() {
        return wave;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public void reset() {
        rrMax = 0;
        rrMin = 0;
        hr = 0;
        hrv = 0;
        mood = 0;
        br = 0;
        duration = 0L;
        ts = 0L;
        wave = "";
        //notifyChange();
    }

    public boolean isEmptyData() {
        return rrMax == 0 ||
                rrMin == 0 ||
                hr == 0 ||
                hrv == 0 ||
                mood == 0 ||
                br == 0 ||
                duration == 0L ||
                ts == 0L ||
                TextUtils.isEmpty(wave);
    }
}
