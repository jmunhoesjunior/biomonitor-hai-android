package br.com.bioz4life.biomonitor.database;

import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.bean.Bt;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmUtilityBt {

    public static List<Bt>getAll(Realm realm, String user){
        List<Bt> result = new ArrayList<>();
        RealmResults<BtRealm> realmRealmResults = realm
                .where(BtRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .findAll();

        if (realmRealmResults != null) {
            for(BtRealm item : realmRealmResults){
                Bt bt = new Bt();
                bt.setCreatedDate(item.createdDate);
                bt.setTs(item.ts);
                bt.setId(item.id);
                bt.setTemp(item.temp);
                result.add(bt);
            }
        }

        return result;
    }
    public static List<Bt> getLastSamplesLimit5(Realm realm, String user){

        List<Bt> result = new ArrayList<>();
        RealmResults<BtRealm> realmRealmResults = realm
                .where(BtRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .limit(5)
                .findAll();

        if (realmRealmResults != null) {
            for(BtRealm item : realmRealmResults){
                Bt bt = new Bt();
                bt.setCreatedDate(item.createdDate);
                bt.setTs(item.ts);
                bt.setId(item.id);
                bt.setTemp(item.temp);
                result.add(bt);
            }
        }

        return result;
    }

    public static Bt getLastSample(Realm realm, String user){

        Bt result = null;
        RealmResults<BtRealm> realmRealmResults = realm
                .where(BtRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .limit(1)
                .findAll();

        if (realmRealmResults != null) {
            for(BtRealm item : realmRealmResults){
                result = new Bt();
                result.setCreatedDate(item.createdDate);
                result.setTs(item.ts);
                result.setId(item.id);
                result.setTemp(item.temp);
            }
        }

        return result;
    }

    public static Bt getUnsyncBt(Realm realm, String user){
        Bt bt = null;
        BtRealm itemRealm = realm
                .where(BtRealm.class)
                .equalTo("user", user)
                .equalTo("isSync",false)
                .sort("createdDate", Sort.ASCENDING)
                .findFirst();

        if (itemRealm != null) {
            bt = new Bt();
            bt.setCreatedDate(itemRealm.createdDate);
            bt.setId(itemRealm.id);

            bt.setTs(itemRealm.ts);
            bt.setTemp(itemRealm.temp);
        }
        return bt;
    }

    public static void addFromServer(Realm realm, List<Bt> list, String user){
        if (list != null) {
            realm.beginTransaction();
            for(Bt c : list){
                BtRealm btRealm = new BtRealm();

                btRealm.id = c.getId();
                btRealm.user = user;
                btRealm.createdDate = c.getCreatedDate();
                btRealm.temp = c.getTemp();
                btRealm.ts = c.getTs();
                btRealm.isSync = true;
                realm.copyToRealm(btRealm);
            }
            realm.commitTransaction();
        }
    }

    public static void deleteDataSyncronized(Realm realm, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<BtRealm> results = realm
                        .where(BtRealm.class)
                        .equalTo("user", user)
                        .equalTo("isSync",true)
                        .findAll();

                if (results != null) {
                    results.deleteAllFromRealm();
                }
            }
        });
    }

    public static void updateStatus(Realm realm, final String id, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                BtRealm itemRealm = realm
                        .where(BtRealm.class)
                        .equalTo("id", id)
                        .equalTo("user", user)
                        .findFirst();

                if(itemRealm != null){
                    itemRealm.isSync = true;
                }
            }
        });
    }
}
