package br.com.bioz4life.biomonitor.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.adapter.AdapterECG;
import br.com.bioz4life.biomonitor.bean.ECG;
import br.com.bioz4life.biomonitor.database.RealmUtilityECG;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class CompleteListECG extends BaseActivity implements AdapterECG.OnClickECG {

    private Realm realm;

    @BindView(R.id.rvList)
    RecyclerView rvList;

    AdapterECG adapterECG;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_complete_list_ecg);

        ButterKnife.bind(this);

        setUpActionBar("Histórico de amostras ECG", getString(R.string.app_name), true);

        realm = Realm.getDefaultInstance();

        List<ECG> listECG = RealmUtilityECG.getAll(realm, LocalPrefs.getPhoneNumber(this));

        rvList.setLayoutManager(new LinearLayoutManager(this));
        rvList.setNestedScrollingEnabled(false);
        rvList.setHasFixedSize(false);

        adapterECG = new AdapterECG(this,listECG,this);

        rvList.setAdapter(adapterECG);
    }

    @Override
    protected void onDestroy() {
        realm.close();

        super.onDestroy();
    }

    @Override
    public void onClickECGItem(int position, ECG ecg) {
        Intent intent = new Intent(this, ECGLargeActivity.class);
        intent.putExtra("pagerSpeed", ecg.getPagerSpeed());
        intent.putExtra("gain", ecg.getGain());
        intent.putExtra("model", ecg);
        startActivity(intent);
    }
}
