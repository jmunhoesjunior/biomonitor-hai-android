package br.com.bioz4life.biomonitor.utils;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.bioz4life.biomonitor.database.RealmUtilityUserAccount;
import br.com.bioz4life.biomonitor.database.UserAccountRealm;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

public class Utility {

    public static void loadUserProfile(final Context mContext, final CircleImageView imageView, String name, String photo){

        if(name != null){
            name = name.replace(" ","+");
        }
        String url = "https://ui-avatars.com/api/?name="+name+"&size=96&font-size=0.33&color=fff&background=a0a0a0";
        if (photo != null) {
            if(!photo.equals("")){
                url = photo;
            }
        }
        Glide.with(mContext).
                load(url)

                .into(new GlideDrawableImageViewTarget(imageView) {
                    @Override public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                        // here it's similar to RequestListener, but with less information (e.g. no model available)
                        super.onResourceReady(resource, animation);
                        // here you can be sure it's already set
                    }
                    // +++++ OR +++++
                    @Override protected void setResource(GlideDrawable resource) {
                        // this.getView().setImageDrawable(resource); is about to be called
                        super.setResource(resource);
                        // here you can be sure it's already set
                    }
                });
    }

    // Check Connection
    public static boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }

    public static double celsiusToFahrenheit(double Celsius){
        return (Celsius/1.0f * (9.0f/5.0f)) + 32.0f;
    }
    public static double fahrenheitTocelsius(double Fahrenheit){
        return ((Fahrenheit - 32.0f) * (5.0f/9.0f));
    }

    public static int getAge(String birthday, String formatInput) {

        if (birthday == null) {
            return 0;
        }
        if (birthday.isEmpty()) {
            return 0;
        }
        try{

            Calendar dob = Calendar.getInstance();
            Calendar today = Calendar.getInstance();

            int year = Integer.valueOf(formatDate(birthday,formatInput, "yyyy"));
            int month = Integer.valueOf(formatDate(birthday,formatInput, "MM"));
            int day = Integer.valueOf(formatDate(birthday,formatInput, "dd"));

            dob.set(year, month, day);

            int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

            if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
                age--;
            }

            //Integer ageInt = new Integer(age);
            //String ageS = ageInt.toString();

            return age;

        }catch (Exception e){}

        return 0;
    }
    public static String formatDate(String dateInput, String formatInput, String endInput){
        String result = "";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formatInput);
        try {
            Date date = simpleDateFormat.parse(dateInput);
            SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(endInput);
            result = simpleDateFormat2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Date stringToDate(String date, String intputFormat){
        SimpleDateFormat format = new SimpleDateFormat(intputFormat);
        try {
            return format.parse(date);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static double mmolToMg(double mmol){
        return mmol * 18.018;
    }


    public static boolean hasUserInfo(Realm realm, Context mContext) {
        UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm,LocalPrefs.getPhoneNumber(mContext));

        if (userAccountRealm == null) {
            return false;
        }
        else if (userAccountRealm.Birthday == null ||userAccountRealm.Cpf == null || userAccountRealm.FullName == null || userAccountRealm.Gender == null) {
            return false;
        }
        else if (userAccountRealm.Birthday.equals("") ||userAccountRealm.Cpf.equals("") || userAccountRealm.FullName.equals("") || userAccountRealm.Gender.equals("")){
            return false;
        }
        return true;
    }

    // Function to validate URL
    // using regular expression
    public static boolean onlyDigits(String str)
    {
        // Regex to check string
        // contains only digits
        String regex = "[0-9]+";

        // Compile the ReGex
        Pattern p = Pattern.compile(regex);

        // If the string is empty
        // return false
        if (str == null) {
            return false;
        }

        // Find match between given string
        // and regular expression
        // using Pattern.matcher()
        Matcher m = p.matcher(str);

        // Return if the string
        // matched the ReGex
        return m.matches();
    }
}
