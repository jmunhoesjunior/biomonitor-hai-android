package br.com.bioz4life.biomonitor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.bioz4life.biomonitor.R;

public class AdapterString extends RecyclerView.Adapter {

    private Context mContext;
    private List<String> mItems;
    private OnClickItemProfile onClickItemProfile;

    public interface OnClickItemProfile{
        void onClickProfile(int position);
    }
    private class Holder extends RecyclerView.ViewHolder {

        TextView tvDescription;
        RelativeLayout relativeMain;

        public Holder(@NonNull View itemView) {
            super(itemView);

            tvDescription = (TextView)itemView.findViewById(R.id.tvDescription);
            relativeMain = (RelativeLayout) itemView.findViewById(R.id.relativeMain);
        }
    }
    public AdapterString(Context ctx, List<String> items, OnClickItemProfile onClickItemProfile){
        this.mContext = ctx;
        this.mItems = items;
        this.onClickItemProfile =onClickItemProfile;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(mContext)
                .inflate(R.layout.item_string, viewGroup, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder,final int i) {

        Holder holder = (Holder)viewHolder;
        if (mItems.get(i) != null) {
            holder.tvDescription.setText(mItems.get(i));
        }

        holder.relativeMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickItemProfile.onClickProfile(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mItems == null)
            return 0;

        return mItems.size();
    }
}
