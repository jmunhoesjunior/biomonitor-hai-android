package br.com.bioz4life.biomonitor.hai.models;

import java.io.Serializable;

public class HaiRoot implements Serializable {
    public String id;
    public HaiData data;
}
