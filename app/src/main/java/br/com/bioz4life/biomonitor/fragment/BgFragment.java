package br.com.bioz4life.biomonitor.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.linktop.MonitorDataTransmissionManager;
import com.linktop.constant.BgPagerCaliCode;
import com.linktop.infs.OnBgResultListener;
import com.linktop.whealthService.MeasureType;
import com.linktop.whealthService.task.BgTask;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.Bg;
import br.com.bioz4life.biomonitor.binding.ObservableString;
import br.com.bioz4life.biomonitor.database.BgRealm;
import br.com.bioz4life.biomonitor.database.RealmUtilityBg;
import br.com.bioz4life.biomonitor.databinding.FragmentBgBinding;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.UToast;
import br.com.bioz4life.biomonitor.utils.Utility;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ccl on 2018/1/23.
 * Interface de medição de glicose no sangue
 */

public class BgFragment extends MeasureFragment implements OnBgResultListener {

    private Bg model;
    private ObservableString event = new ObservableString("");
    private BgTask mBgTask;

    @BindView(R.id.tvValueMgDL)
    TextView tvValueMgDLl;

    @BindView(R.id.tvValueMMol)
    TextView tvValueMMol;

    @BindView(R.id.btSyncData)
    Button btSyncData;

    public BgFragment() {
    }

    @Override
    protected ViewDataBinding onCreateBindingView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentBgBinding binding = setBindingContentView(inflater, R.layout.fragment_bg, container);
        binding.setContent(this);
        this.btnMeasure = binding.btnMeasure;
        model = new Bg();
        binding.setModel(model);
        binding.setEvent(event);
        final Context context = getContext();
        if (context != null) {
            final String[] codes = BgPagerCaliCode.values();
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, Arrays.asList(codes));
            binding.spin1.setAdapter(adapter);
            binding.spin1.setSelection(20, true);
            binding.spin1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    MonitorDataTransmissionManager.getInstance().setBgPagerCaliCode(codes[position]);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        tvValueMgDLl.setText("0 mg/dL");
        tvValueMMol.setText("0.0 mmol/L");
        super.onActivityCreated(savedInstanceState);
        if (mHcService != null) {
            mBgTask = mHcService.getBleDevManager().getBsTask();
            mBgTask.setOnBgResultListener(this);
        }
        else {
            MonitorDataTransmissionManager.getInstance().setOnBgResultListener(this);
        }
    }

    @Override
    public boolean startMeasure() {
        btSyncData.setEnabled(false);

        model.setTs(System.currentTimeMillis());
        if (mBgTask != null) {
            if (mBgTask.isModuleExist()) {
                mBgTask.start();
                return true;
            } else {
                toast("This Device's Bg module is not exist.");
                return false;
            }
        } else {
            if (MonitorDataTransmissionManager.getInstance().isBsModuleExist()) {
                MonitorDataTransmissionManager.getInstance().startMeasure(MeasureType.BG);
                return true;
            } else {
                toast("This Device's Bg module is not exist.");
                return false;
            }
        }
    }

    @Override
    public void stopMeasure() {
        btSyncData.setEnabled(true);

        if (mBgTask != null) {
            mBgTask.stop();
        }
        else {
            MonitorDataTransmissionManager.getInstance().stopMeasure();
        }
        event.set("");
    }

    @Override
    public String getTitle() {
        return "Glicose";
    }

    @Override
    public void reset() {
        model.reset();
    }

    /**
     * Evento de retorno
     */
    @Override
    public void onBgEvent(int eventId, Object obj) {
        switch (eventId) {
            case BgTask.EVENT_PAGER_IN:
                event.set("Tira de teste foi inserida");
                break;
            case BgTask.EVENT_PAGER_READ:
                event.set("A tira de teste está pronta, por favor, coloque a amostra de sangue na tira de teste.");
                break;
            case BgTask.EVENT_BLOOD_SAMPLE_DETECTING:
                event.set("Colete amostras de sangue, medição de glicose no sangue, por favor aguarde ...");
                break;
            case BgTask.EVENT_TEST_RESULT:
                event.set("Resultados do teste de glicose no sangue:");

                double value = (double) obj;
                model.setValue(value);
                tvValueMMol.setText(String.format("%.1f mmol/L",value));

                double mgdl = 18.018 * value;
                int mgdlInt = (int)Math.round(mgdl);
                tvValueMgDLl.setText(String.valueOf(mgdlInt) + " mg/dL");
                resetState();

                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btSyncData.setEnabled(true);

                        // Salvar no banco esta amostra
                        BgRealm bgRealm = new BgRealm();
                        bgRealm.id = UUID.randomUUID().toString();
                        bgRealm.createdDate = new Date();
                        bgRealm.user = LocalPrefs.getPhoneNumber(mActivity);
                        bgRealm.value = model.getValue();
                        bgRealm.ts = model.getTs();
                        mActivity.getRealm().beginTransaction();
                        mActivity.getRealm().copyToRealm(bgRealm);
                        mActivity.getRealm().commitTransaction();
                    }
                });

                break;
            default:
                Log.e("onBsEvent", "eventId:" + eventId + ", obj:" + obj);
                break;
        }
    }

    /**
     * 返回异常
     * 当返回事件{@link BgTask#EVENT_PAGER_IN} 后任意时候拔出试纸，则报异常{@link BgTask#EXCEPTION_PAGER_OUT};
     * 当返回事件{@link BgTask#EVENT_PAGER_IN} 后设备检测到试纸是被使用过的，则报异常{@link BgTask#EXCEPTION_PAPER_USED};
     * 当返回事件{@link BgTask#EVENT_PAGER_READ}后30secs內未向试纸滴入血液样本，则报异常{@link BgTask#EXCEPTION_TIMEOUT_FOR_CHECK_BLOOD_SAMPLE};
     * 当返回事件{@link BgTask#EVENT_BLOOD_SAMPLE_DETECTING}后60secs內未向试纸滴入血液样本，则报异常{@link BgTask#EXCEPTION_TIMEOUT_FOR_DETECT_BLOOD_SAMPLE};
     * 所有异常上报的同时，SDK内部将自动结束血糖的测试。
     */
    @Override
    public void onBgException(int exception) {
        switch (exception) {
            case BgTask.EXCEPTION_PAGER_OUT:
                toast("A tira de teste não está inserida");
                break;
            case BgTask.EXCEPTION_PAPER_USED:
                toast("A tira de teste foi usada, substitua por uma nova.");
                break;
            case BgTask.EXCEPTION_TESTING_PAPER_OUT:
                toast("O papel de teste foi retirado durante o teste");
                break;

            case BgTask.EXCEPTION_TIMEOUT_FOR_DETECT_BLOOD_SAMPLE:
                toast("Calcular o tempo limite do valor da glicose no sangue da amostra");
                break;
            default:
                Log.e("onBsException", "exception:" + exception);
                break;
        }
        btSyncData.setEnabled(true);
        event.set("");
        resetState();
    }

    private List<String> getCalibrationCodeList(BgPagerCaliCode[] codes) {
        List<String> list = new ArrayList<>();
        for (BgPagerCaliCode code : codes) {
            list.add(code.toString());
        }
        return list;
    }

    private void sendData(){
        mActivity.showProgressDialog(getResources().getString(R.string.sending_data));

        final Bg item = RealmUtilityBg.getUnsyncBg(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        String createDate = df.format("yyyy-MM-dd HH:mm:ss", item.getCreatedDate()).toString();

        Map<String, Object> map = new HashMap<>();
        map.put("ts",item.getTs());
        map.put("value",item.getValue());
        map.put("device_id",LocalPrefs.getValue(mActivity,mActivity.device_id));
        map.put("device_key",LocalPrefs.getValue(mActivity,mActivity.device_key));
        map.put("createdDate",createDate);

        db.collection("biomonitor")
                .document(LocalPrefs.getPhoneNumber(mActivity))
                .collection("exams_bg").document(item.getId()).set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                RealmUtilityBg.updateStatus(mActivity.getRealm(), item.getId(), LocalPrefs.getPhoneNumber(mActivity));

                mActivity.dismissProgressDialog();

                syncData();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mActivity.dismissProgressDialog();
                new AlertDialogBuilder(mActivity)
                        .setTitle("Erro")
                        .setMessage(e.getMessage())
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
    }

    @OnClick(R.id.btSyncData)
    public void syncData(){
        if (Utility.isOnline()) {

            Bg bgToBeSyncronized = RealmUtilityBg.getUnsyncBg(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

            //1.1 Sincronizar um por um
            if (bgToBeSyncronized != null) {
                sendData();
            }
            else{
                //1.2 Atualizar lista
                mActivity.showProgressDialog(getResources().getString(R.string.loading_data));

                String userPhone = LocalPrefs.getPhoneNumber(mActivity);

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                CollectionReference collectionReference = db.collection("biomonitor")
                        .document(userPhone)
                        .collection("exams_bg");

                collectionReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            mActivity.dismissProgressDialog();

                            // apagar
                            RealmUtilityBg.deleteDataSyncronized(mActivity.getRealm(),userPhone);

                            int count = task.getResult().size();

                            if (count > 0){
                                ArrayList<Bg> exams = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    //Log.d(TAG, document.getId() + " => " + document.getData());

                                    Map<String, Object> map = document.getData();

                                    Bg item =  new Bg();
                                    item.setId(document.getId());
                                    item.setTs((long) map.get("ts"));
                                    item.setValue((Double) map.get("value"));
                                    Date createdDate = Utility.stringToDate((String) map.get("createdDate"),"yyyy-MM-dd HH:mm:ss");
                                    item.setCreatedDate(createdDate);

                                    exams.add(item);
                                }
                                // add data do server
                                RealmUtilityBg.addFromServer(mActivity.getRealm(),exams,userPhone);
                            }
                        }
                        else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                            mActivity.dismissProgressDialog();

                            UToast.show(mActivity,task.getException().getMessage());
                        }
                    }
                });
            }
        }
        else{
            UToast.show(getActivity(),getResources().getString(R.string.no_connection));
        }
    }
}
