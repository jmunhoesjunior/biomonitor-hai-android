package br.com.bioz4life.biomonitor.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.linktop.MonitorDataTransmissionManager;
import com.linktop.infs.OnBtResultListener;
import com.linktop.whealthService.MeasureType;
import com.linktop.whealthService.task.BtTask;
import com.timqi.sectorprogressview.ColorfulRingProgressView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.Bt;
import br.com.bioz4life.biomonitor.database.BtRealm;
import br.com.bioz4life.biomonitor.database.RealmUtilityBt;
import br.com.bioz4life.biomonitor.databinding.FragmentBtBinding;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.UToast;
import br.com.bioz4life.biomonitor.utils.Utility;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ccl on 2017/2/7.
 * Interface de medição da temperatura corporal
 */

public class BtFragment extends MeasureFragment
        implements OnBtResultListener {

    private Bt model;
    private BtTask mBtTask;

    @BindView(R.id.crpv)
    ColorfulRingProgressView crpv;

    @BindView(R.id.tvType)
    TextView tvType;

    @BindView(R.id.tvTemperature)
    TextView tvTemperature;

    @BindView(R.id.seekTemperature)
    SeekBar seekTemperature;

    @BindView(R.id.seekTemperatureF)
    SeekBar seekTemperatureF;

    @BindView(R.id.btSyncData)
    Button btSyncData;

    public BtFragment() {

    }

    @Override
    public boolean startMeasure() {
        btSyncData.setEnabled(false);

        if(mBtTask!= null) {

            tvTemperature.setText("0");
            seekTemperature.setProgress(0);
            seekTemperatureF.setProgress(0);

            crpv.animateIndeterminate();
            mBtTask.start();
        }
        else {
            tvTemperature.setText("0");

            seekTemperature.setProgress(0);
            seekTemperatureF.setProgress(0);
            crpv.animateIndeterminate();
            MonitorDataTransmissionManager.getInstance().startMeasure(MeasureType.BT);
        }
        return true;
    }

    @Override
    public void stopMeasure() {
        btSyncData.setEnabled(true);

        crpv.stopAnimateIndeterminate();

        //BT module is not have method stop().Because it will return result in 2~4 seconds when you click to start measure.

//        if(mBtTask!= null) {
//            mBtTask.stop();
//        } else {
//            MonitorDataTransmissionManager.getInstance().stopMeasure();
//        }
    }

    @Override
    public String getTitle() {
        return "Temperatura";
    }

    @Override
    protected ViewDataBinding onCreateBindingView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentBtBinding binding = setBindingContentView(inflater, R.layout.fragment_bt, container);
        binding.setContent(this);
        this.btnMeasure = binding.btnMeasure;
        model = new Bt();
        binding.setModel(model);
        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mHcService != null) {
            mBtTask = mHcService.getBleDevManager().getBtTask();
            mBtTask.setOnBtResultListener(this);
        }
        else {
            MonitorDataTransmissionManager.getInstance().setOnBtResultListener(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void reset() {
        model.reset();
    }

    /*
    * A unidade de retorno padrão é o valor de temperatura em graus Celsius Se o valor de temperatura de Fahrenheit for necessário,
    * ele será convertido de acordo com a fórmula.
    * Esta demonstração usa o método Databinding Para detalhes, por favor veja o controle de exibição de temperatura TextView do layout.
    * */
    @Override
    public void onBtResult(double tempValue) {

        model.setTemp(tempValue);
        model.setTs(System.currentTimeMillis() / 1000L);

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    btSyncData.setEnabled(true);

                    tvTemperature.setText(String.format("%.1f",model.getTemp()));

                    if(model.getTemp() > 100){
                        seekTemperature.setProgress(100);
                        seekTemperatureF.setProgress(212);
                    }
                    else{
                        seekTemperature.setProgress(new Double(model.getTemp()).intValue());
                        seekTemperatureF.setProgress(new Double(Utility.celsiusToFahrenheit(model.getTemp())).intValue());
                    }

                    // Salvar no banco esta amostra
                    BtRealm btRealm = new BtRealm();
                    btRealm.id = UUID.randomUUID().toString();
                    btRealm.createdDate = new Date();
                    btRealm.temp = model.getTemp();
                    btRealm.ts = model.getTs();
                    btRealm.user = LocalPrefs.getPhoneNumber(mActivity);
                    mActivity.getRealm().beginTransaction();
                    mActivity.getRealm().copyToRealm(btRealm);
                    mActivity.getRealm().commitTransaction();
                }
                finally {
                    crpv.stopAnimateIndeterminate();
                    resetState();
                }
            }
        });
    }

    private void sendData(){
        mActivity.showProgressDialog(getResources().getString(R.string.sending_data));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        final Bt item = RealmUtilityBt.getUnsyncBt(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        String createDate = df.format("yyyy-MM-dd HH:mm:ss", item.getCreatedDate()).toString();

        Map<String, Object> map = new HashMap<>();
        map.put("ts",item.getTs());
        map.put("temp",item.getTemp());
        map.put("device_id",LocalPrefs.getValue(mActivity,mActivity.device_id));
        map.put("device_key",LocalPrefs.getValue(mActivity,mActivity.device_key));
        map.put("createdDate",createDate);

        db.collection("biomonitor")
                .document(LocalPrefs.getPhoneNumber(mActivity))
                .collection("exams_bt").document(item.getId()).set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                RealmUtilityBt.updateStatus(mActivity.getRealm(), item.getId(), LocalPrefs.getPhoneNumber(mActivity));

                mActivity.dismissProgressDialog();

                syncData();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mActivity.dismissProgressDialog();
                new AlertDialogBuilder(mActivity)
                        .setTitle("Erro")
                        .setMessage(e.getMessage())
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
    }

    @OnClick(R.id.btSyncData)
    public void syncData(){

        if (Utility.isOnline()) {

            Bt btToBeSyncronized = RealmUtilityBt.getUnsyncBt(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

            //1.1 Sincronizar um por um
            if (btToBeSyncronized != null) {
                sendData();
            }
            else{
                //1.2 Atualizar lista
                mActivity.showProgressDialog(getResources().getString(R.string.loading_data));

                String userPhone = LocalPrefs.getPhoneNumber(mActivity);

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                CollectionReference collectionReference = db.collection("biomonitor")
                        .document(userPhone)
                        .collection("exams_bt");

                collectionReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            mActivity.dismissProgressDialog();

                            // apagar
                            RealmUtilityBt.deleteDataSyncronized(mActivity.getRealm(),userPhone);

                            int count = task.getResult().size();

                            if (count > 0){
                                ArrayList<Bt> exams = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    //Log.d(TAG, document.getId() + " => " + document.getData());

                                    Map<String, Object> map = document.getData();

                                    Bt item =  new Bt();
                                    item.setId(document.getId());
                                    item.setTs((long) map.get("ts"));
                                    item.setTemp((Double) map.get("temp"));
                                    Date createdDate = Utility.stringToDate((String) map.get("createdDate"),"yyyy-MM-dd HH:mm:ss");
                                    item.setCreatedDate(createdDate);

                                    exams.add(item);
                                }
                                // add data do server
                                RealmUtilityBt.addFromServer(mActivity.getRealm(),exams,userPhone);
                            }
                        }
                        else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                            mActivity.dismissProgressDialog();

                            UToast.show(mActivity,task.getException().getMessage());
                        }
                    }
                });
            }
        }
        else{
            UToast.show(getActivity(),getResources().getString(R.string.no_connection));
        }
    }
}
