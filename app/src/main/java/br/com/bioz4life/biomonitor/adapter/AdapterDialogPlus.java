package br.com.bioz4life.biomonitor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.bioz4life.biomonitor.R;

public class AdapterDialogPlus extends BaseAdapter {

    private Context mContext;
    private List<String> mItems;


    public AdapterDialogPlus(Context ctx, List<String> mItems){
        this.mContext = ctx;
        this.mItems = mItems;

    }
    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).
                    inflate(R.layout.item_string, parent, false);
        }

        TextView tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
        tvDescription.setText(mItems.get(position));

        return convertView;
    }
}
