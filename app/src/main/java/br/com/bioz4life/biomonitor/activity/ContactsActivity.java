package br.com.bioz4life.biomonitor.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.adapter.AdapterMembers;
import br.com.bioz4life.biomonitor.database.RealmUtilityContact;
import br.com.bioz4life.biomonitor.model.Contact;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.UToast;
import br.com.bioz4life.biomonitor.utils.Utility;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class ContactsActivity extends ExtentionMenuContact implements AdapterMembers.OnClickMember {

    @BindView(R.id.rvListContacts)
    RecyclerView rvListContacts;

    AdapterMembers adapterMembers;

    Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_members);

        ButterKnife.bind(this);

        setUpActionBar("Contatos", getString(R.string.app_name), true);

        realm = Realm.getDefaultInstance();

        rvListContacts.setLayoutManager(new LinearLayoutManager(this));

        refresh();
    }

    public void sendData(){
        showProgressDialog(getResources().getString(R.string.sending_data));

        String userPhone = LocalPrefs.getPhoneNumber(this);
        final Contact item = RealmUtilityContact.getUnsyncContacts(realm,
                userPhone);

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        Map<String, Object> map = new HashMap<>();
        map.put("email",item.email);
        map.put("favorite_yesno",item.isFavorite);
        map.put("fullname",item.fullname);
        map.put("notify_yesno",item.isNotify);
        map.put("phone",item.phone);
        map.put("physician_yesno",item.isPhysician);
        map.put("cpf",item.cpf);
        map.put("relationship",item.relationship);

        db.collection("biomonitor").document(userPhone)
                .collection("contacts")
                .document(item.id)
                .set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                RealmUtilityContact.updateStatus(realm, item.id, userPhone);

                dismissProgressDialog();

                refresh();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dismissProgressDialog();

                loadData();
            }
        });
    }

    public void refresh(){
        // Com conexao:
        // 1.1 - Tentar enviar dados nao sincronizados, e depois atualizar lista de contatos offline
        // 1.2 -  OBS.: (apagar da lista apenas contatos que ja foram sincronizados). Desta forma evita duplicidade
        if (Utility.isOnline()) {
            Contact contactToBeSyncronize = RealmUtilityContact.getUnsyncContacts(realm,
                                                                    LocalPrefs.getPhoneNumber(this));
            //1.1 Sincronizar um por um
            if(contactToBeSyncronize != null){
                sendData();
            }
            else{
                //1.2 Atualizar lista
                showProgressDialog(getResources().getString(R.string.loading_data));

                String userPhone = LocalPrefs.getPhoneNumber(this);

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                db.collection("biomonitor")
                        .document(userPhone)
                        .collection("contacts")
                        .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            dismissProgressDialog();

                            // apagar
                            RealmUtilityContact.deleteDataSyncronized(realm,userPhone);

                            int count = task.getResult().size();

                            if (count > 0){
                                ArrayList<Contact> contacts = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    //Log.d(TAG, document.getId() + " => " + document.getData());

                                    Map<String, Object> map = document.getData();

                                    Contact item =  new Contact();
                                    item.id = document.getId();
                                    item.phone = (String) map.get("phone");
                                    item.relationship = (String) map.get("relationship");
                                    item.email = (String) map.get("email");
                                    item.cpf = (String) map.get("cpf");
                                    item.isFavorite = (Boolean) map.get("favorite_yesno");
                                    item.isNotify = (Boolean) map.get("notify_yesno");
                                    item.isPhysician = (Boolean) map.get("physician_yesno");
                                    item.fullname = (String) map.get("fullname");

                                    contacts.add(item);
                                }
                                // add data do server
                                RealmUtilityContact.addFromServer(realm,contacts,userPhone);
                            }

                            loadData();
                        }
                        else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                            dismissProgressDialog();

                            UToast.show(ContactsActivity.this,task.getException().getMessage());

                            loadData();
                        }
                    }
                });
            }
        }
        else{
            loadData();
        }
    }
    public void loadData(){
        adapterMembers = new AdapterMembers(this,
                RealmUtilityContact.getContacts(realm,
                        LocalPrefs.getPhoneNumber(this)),
                this);
        rvListContacts.setAdapter(adapterMembers);
    }

    @Override
    public void onClickAddContact(){

        Intent intent = new Intent(this, NewContactActivity.class);
        startActivityForResult(intent,1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1000 && resultCode == RESULT_OK){
            // Reload data
            refresh();
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onClickMember(int position) {
        Contact contact = adapterMembers.getContact(position);
        Intent intent = new Intent(this, NewContactActivity.class);
        intent.putExtra("isEdit",true);
        intent.putExtra("contact", new Gson().toJson(contact));
        startActivityForResult(intent,1000);
    }
}
