package br.com.bioz4life.biomonitor.database;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UserAccountRealm extends RealmObject {

    @PrimaryKey
    public String id;

    public String Nickname;
    public String FullName;
    public String FullAddress;
    public String Email;
    public String Gender;
    public String Birthday;
    public String Photo;
    public String RegCode;
    public String Cpf;
}
