package br.com.bioz4life.biomonitor.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.linktop.DeviceType;
import com.linktop.MonitorDataTransmissionManager;
import com.linktop.constant.BluetoothState;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.adapter.FragmentsAdapter;
import br.com.bioz4life.biomonitor.database.RealmUtilityUserAccount;
import br.com.bioz4life.biomonitor.database.UserAccountRealm;
import br.com.bioz4life.biomonitor.fragment.BaseFragment;
import br.com.bioz4life.biomonitor.fragment.BgFragment;
import br.com.bioz4life.biomonitor.fragment.BpFragment;
import br.com.bioz4life.biomonitor.fragment.BtFragment;
import br.com.bioz4life.biomonitor.fragment.ECGFragment;
import br.com.bioz4life.biomonitor.fragment.HistoryFragment;
import br.com.bioz4life.biomonitor.fragment.MonitorInfoFragment;
import br.com.bioz4life.biomonitor.fragment.ProfileFragment;
import br.com.bioz4life.biomonitor.fragment.SPO2HFragment;
import br.com.bioz4life.biomonitor.health.App;
import br.com.bioz4life.biomonitor.health.HcService;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.Utility;
import br.com.bioz4life.biomonitor.widget.CustomViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import im.delight.android.location.SimpleLocation;
import io.realm.Realm;

/**
 * Created by ccl on 2017/2/7.
 * Interface de demonstração do testador de integridade
 */

public class HealthMonitorActivity extends BaseActivity
        implements MonitorDataTransmissionManager.OnServiceBindListener,
        TabLayout.OnTabSelectedListener, ViewPager.OnPageChangeListener, ServiceConnection {

    public static final int REQUEST_ACTIVITY_ACCOUNT = 5000;
    public static final int REQUEST_ACTIVITY_MEMBERS = 5001;

    public static final String device_id="deviceId";
    public static final String device_key="deviceKey";

    private SimpleLocation location;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.view_pager)
    public CustomViewPager viewPager;

    @BindView(R.id.view_focus)
    public View viewFocus;

    @BindView(R.id.tvPhone)
    TextView tvPhone;

    @BindView(R.id.tvNick)
    TextView tvNick;

    @BindView(R.id.imgProfile)
    CircleImageView imgProfile;

    @BindView(R.id.tvBatery)
    TextView tvBatery;

    @BindView(R.id.imgBattery)
    ImageView imgBattery;

    private int batteryValue = 0;
    private String fragmentLocation = "";
    private final SparseArray<BaseFragment> sparseArray = new SparseArray<>();
    public HcService mHcService;
    private MonitorInfoFragment mMonitorInfoFragment;

    private Realm realm;

    public Realm getRealm() {
        return realm;
    }

    private SimpleLocation.Listener listener;
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HcService.BLE_STATE:
                    final int state = (int) msg.obj;
                    Log.e("Message", "receive state:" + state);
                    if (state == BluetoothState.BLE_NOTIFICATION_ENABLED) {
                        mHcService.dataQuery(HcService.DATA_QUERY_SOFTWARE_VER);
                    }
                    else {
                        mMonitorInfoFragment.onBleState(state);
                    }
                    break;
            }
        }
    };

    public void setBatery(final String batery, final boolean isVisible){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!isVisible) {
                    tvBatery.setVisibility(View.GONE);
                    imgBattery.setVisibility(View.GONE);
                    if(batery != null)
                        tvBatery.setText("Bateria: " + batery);
                }
                else{
                    tvBatery.setVisibility(View.VISIBLE);
                    imgBattery.setVisibility(View.VISIBLE);

                    if(batery != null) {
                        String value = batery.replace("%","").replace(" ","");
                        int percent =Integer.valueOf(value);
                        if(percent > 70){
                            imgBattery.setImageResource(R.drawable.battery1);
                        }
                        else if(percent>= 30 && percent <=70){
                            imgBattery.setImageResource(R.drawable.battery2);
                        }
                        else{
                            imgBattery.setImageResource(R.drawable.battery3);
                        }
                        tvBatery.setText("Bateria: " + batery);
                    }
                }
            }
        });

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_health_monitor);
        ButterKnife.bind(this);

        realm = Realm.getDefaultInstance();

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        /*if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setTitle("Monitor de Sinais Vitais");
            actionBar.setSubtitle("BioMonitor by BioZ4Life (C) 2019");
        }*/
        actionBar.hide();


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH)
                        != PackageManager.PERMISSION_GRANTED  ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.BLUETOOTH,
                            Manifest.permission.BLUETOOTH_ADMIN,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    100);
        }

        // construct a new instance of SimpleLocation
        boolean requireFineGranularity = false;
        boolean passiveMode = false;
        long updateIntervalInMilliseconds = 10 * 60 * 1000;
        boolean requireNewLocation = true;
        location = new SimpleLocation(this, requireFineGranularity, passiveMode, updateIntervalInMilliseconds, requireNewLocation);
        // if we can't access the location yet
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        }

        listener = new SimpleLocation.Listener() {
            @Override
            public void onPositionChanged() {
                location.endUpdates();
                dismissProgressDialog();
                if(fragmentLocation.equals("MonitorInfoFragment")){
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                    // send battery, latitude longitude
                }
            }
        };
        location.setListener(listener);

        tvNick.setText("");
        tvPhone.setText("");

        viewPager.setOffscreenPageLimit(4);
        viewPager.setPageMargin(10);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(this);
        viewPager.addOnPageChangeListener(this);
        viewPager.setCurrentItem(0, false);

        //Bind service about Bluetooth connection.
        if (App.isUseCustomBleDevService) {
            Intent serviceIntent = new Intent(this, HcService.class);
            bindService(serviceIntent, this, BIND_AUTO_CREATE);
        }
        else {
            //Vincular o serviço
            //type é HealthMonitor (HealthMonitor Health Detector)
            MonitorDataTransmissionManager.getInstance().bind(DeviceType.HealthMonitor, this);
        }
        tvBatery.setVisibility(View.GONE);
        imgBattery.setVisibility(View.GONE);
    }

    public void getUserLocationMonitorInfo(int batteryValue){
        // if we can't access the location yet
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        }
        else{
            fragmentLocation = "MonitorInfoFragment";
            this.batteryValue = batteryValue;
            showProgressDialog(getResources().getString(R.string.get_location));
            location.beginUpdates();
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm,LocalPrefs.getPhoneNumber(this));
        if(userAccountRealm != null){
            if (userAccountRealm.id != null) {
                tvPhone.setText(userAccountRealm.id);
            }
            if (userAccountRealm.FullName != null) {
                tvNick.setText(userAccountRealm.FullName);
            }
            Utility.loadUserProfile(HealthMonitorActivity.this,imgProfile,userAccountRealm.FullName,userAccountRealm.Photo);
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();

        if (App.isUseCustomBleDevService) {
            unbindService(this);
        }
        else {
            //Serviço não vinculado
            MonitorDataTransmissionManager.getInstance().unBind();
        }
        App.isShowUploadButton.set(false);
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!App.isUseCustomBleDevService) {
            if (MonitorDataTransmissionManager.getInstance().isMeasuring()) {
                new AlertDialogBuilder(this)
                        .setTitle("Prompt")
                        .setMessage("Na medição, se você quiser sair da interface, pare a medição primeiro.")
                        .setPositiveButton("Ok", null)
                        .create().show();
                return;
            }
            else{
                new AlertDialogBuilder(this)
                        .setTitle("Confirmação")
                        .setMessage("Tem certeza que deseja fazer o logout?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                LocalPrefs.storePhoneNumber(HealthMonitorActivity.this, null);

                                Intent intent = new Intent(getBaseContext(), LauncherActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        }
        //super.onBackPressed();
    }

    @Override
    public void onServiceBind() {
        /*
        * Para evitar que determinados dispositivos que não são o que você deseja apareçam na lista de verificação de dispositivos Bluetooth, você precisa chamar essa API para definir a lista de permissões de prefixo de nome de dispositivo Bluetooth.
        * Quando um dispositivo Bluetooth varre, o dispositivo com o campo de lista de desbloqueio como prefixo do nome do dispositivo será adicionado à lista de varredura do dispositivo Bluetooth e o restante será filtrado.
        * PS: se você não usar essa API, a lista de dispositivos não será filtrada.
        * Se você usar esta API, insira um ID de recurso válido.
        * */
        MonitorDataTransmissionManager.getInstance().setScanDevNamePrefixWhiteList(R.array.health_monitor_dev_name_prefixes);
        //  MonitorDataTransmissionManager.getInstance().setStrongEcgGain(true);//Definir ganho de ECG aprimorado
        //Carregue cada interface de medição depois que a ligação de serviço for bem-sucedida.
        FragmentsAdapter adapter = new FragmentsAdapter(getSupportFragmentManager());
        getMenusFragments();
        adapter.setFragments(sparseArray);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onServiceUnbind() {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if(position == 1){
            HistoryFragment historyFragment = (HistoryFragment) sparseArray.get(1);
            historyFragment.loadData();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    private void getMenusFragments() {
        mMonitorInfoFragment = new MonitorInfoFragment();
        sparseArray.put(0, new ProfileFragment());
        sparseArray.put(1, new HistoryFragment());
        sparseArray.put(2, mMonitorInfoFragment);
        sparseArray.put(3, new BpFragment());
        sparseArray.put(4, new BtFragment());
        sparseArray.put(5, new SPO2HFragment());
        sparseArray.put(6, new BgFragment());
        sparseArray.put(7, new ECGFragment());
    }

    public void reset() {
        viewPager.setCanScroll(true);
        viewFocus.setVisibility(View.GONE);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mHcService = ((HcService.LocalBinder) service).getService();
        mHcService.setHandler(mHandler);
        mHcService.initBluetooth();
        //UI
        FragmentsAdapter adapter = new FragmentsAdapter(getSupportFragmentManager());
        getMenusFragments();
        adapter.setFragments(sparseArray);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        mHcService = null;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_ACTIVITY_ACCOUNT && resultCode == RESULT_OK){

        }
    }

    /*ECGRealm ecgRealm = new ECGRealm();
        ecgRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        ecgRealm.createdDate = new Date();
        ecgRealm.id = UUID.randomUUID().toString();
        ecgRealm.ts = 1000;
        ecgRealm.pagerSpeed = 5;
        ecgRealm.gain = 2;
        ecgRealm.wave = "teste";
        ecgRealm.rrMin = 100;
        ecgRealm.rrMax = 200;
        ecgRealm.mood = 1000;
        ecgRealm.hrv = 3;
        ecgRealm.duration = 7000;
        ecgRealm.hr = 10;
        ecgRealm.br = 13;

        realm.beginTransaction();
        realm.copyToRealm(ecgRealm);
        realm.commitTransaction();*/

        /*
        BgRealm bgRealm = new BgRealm();
        bgRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        bgRealm.createdDate = new Date();
        bgRealm.id = UUID.randomUUID().toString();
        bgRealm.ts = 1000;
        bgRealm.value = 5;
        realm.beginTransaction();
        realm.copyToRealm(bgRealm);
        realm.commitTransaction();

        bgRealm = new BgRealm();
        bgRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        bgRealm.createdDate = new Date();
        bgRealm.id = UUID.randomUUID().toString();
        bgRealm.ts = 1000;
        bgRealm.value = 7;
        realm.beginTransaction();
        realm.copyToRealm(bgRealm);
        realm.commitTransaction();

        bgRealm = new BgRealm();
        bgRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        bgRealm.createdDate = new Date();
        bgRealm.id = UUID.randomUUID().toString();
        bgRealm.ts = 1000;
        bgRealm.value = 2;
        realm.beginTransaction();
        realm.copyToRealm(bgRealm);
        realm.commitTransaction();

        bgRealm = new BgRealm();
        bgRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        bgRealm.createdDate = new Date();
        bgRealm.id = UUID.randomUUID().toString();
        bgRealm.ts = 1000;
        bgRealm.value = 3;
        realm.beginTransaction();
        realm.copyToRealm(bgRealm);
        realm.commitTransaction();

        bgRealm = new BgRealm();
        bgRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        bgRealm.createdDate = new Date();
        bgRealm.id = UUID.randomUUID().toString();
        bgRealm.ts = 1000;
        bgRealm.value = 9;
        realm.beginTransaction();
        realm.copyToRealm(bgRealm);
        realm.commitTransaction();


        BpRealm bpRealm = new BpRealm();
        bpRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        bpRealm.ts = 1000;
        bpRealm.sbp = 150;
        bpRealm.dbp = 92;
        bpRealm.hr = 97;
        bpRealm.createdDate = new Date();
        bpRealm.id = UUID.randomUUID().toString();
        realm.beginTransaction();
        realm.copyToRealm(bpRealm);
        realm.commitTransaction();

        bpRealm = new BpRealm();
        bpRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        bpRealm.ts = 1000;
        bpRealm.sbp = 110;
        bpRealm.dbp = 89;
        bpRealm.hr = 91;
        bpRealm.createdDate = new Date();
        bpRealm.id = UUID.randomUUID().toString();
        realm.beginTransaction();
        realm.copyToRealm(bpRealm);
        realm.commitTransaction();

        bpRealm = new BpRealm();
        bpRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        bpRealm.ts = 1000;
        bpRealm.sbp = 125;
        bpRealm.dbp = 75;
        bpRealm.hr = 98;
        bpRealm.createdDate = new Date();
        bpRealm.id = UUID.randomUUID().toString();
        realm.beginTransaction();
        realm.copyToRealm(bpRealm);
        realm.commitTransaction();

        SPO2Realm spo2Realm = new SPO2Realm();
        spo2Realm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        spo2Realm.id = UUID.randomUUID().toString();
        spo2Realm.createdDate = new Date();
        spo2Realm.ts = 1000;
        spo2Realm.value = 97;
        spo2Realm.hr = 95;
        realm.beginTransaction();
        realm.copyToRealm(spo2Realm);
        realm.commitTransaction();

        spo2Realm = new SPO2Realm();
        spo2Realm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        spo2Realm.id = UUID.randomUUID().toString();
        spo2Realm.createdDate = new Date();
        spo2Realm.ts = 1000;
        spo2Realm.value = 92;
        spo2Realm.hr = 85;
        realm.beginTransaction();
        realm.copyToRealm(spo2Realm);
        realm.commitTransaction();

        spo2Realm = new SPO2Realm();
        spo2Realm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        spo2Realm.id = UUID.randomUUID().toString();
        spo2Realm.createdDate = new Date();
        spo2Realm.ts = 1000;
        spo2Realm.value = 77;
        spo2Realm.hr = 81;
        realm.beginTransaction();
        realm.copyToRealm(spo2Realm);
        realm.commitTransaction();

        BtRealm btRealm = new BtRealm();
        btRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        btRealm.id = UUID.randomUUID().toString();
        btRealm.createdDate = new Date();
        btRealm.ts = 1000;
        btRealm.temp= 41;
        realm.beginTransaction();
        realm.copyToRealm(btRealm);
        realm.commitTransaction();

        btRealm = new BtRealm();
        btRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        btRealm.id = UUID.randomUUID().toString();
        btRealm.createdDate = new Date();
        btRealm.ts = 1000;
        btRealm.temp= 26;
        realm.beginTransaction();
        realm.copyToRealm(btRealm);
        realm.commitTransaction();

        btRealm = new BtRealm();
        btRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        btRealm.id = UUID.randomUUID().toString();
        btRealm.createdDate = new Date();
        btRealm.ts = 1000;
        btRealm.temp= 29;
        realm.beginTransaction();
        realm.copyToRealm(btRealm);
        realm.commitTransaction();

        btRealm = new BtRealm();
        btRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        btRealm.id = UUID.randomUUID().toString();
        btRealm.createdDate = new Date();
        btRealm.ts = 1000;
        btRealm.temp= 18;
        realm.beginTransaction();
        realm.copyToRealm(btRealm);
        realm.commitTransaction();

        btRealm = new BtRealm();
        btRealm.user = LocalPrefs.getUserAccount(HealthMonitorActivity.this).getUserID();
        btRealm.id = UUID.randomUUID().toString();
        btRealm.createdDate = new Date();
        btRealm.ts = 1000;
        btRealm.temp= 36;
        realm.beginTransaction();
        realm.copyToRealm(btRealm);
        realm.commitTransaction();*/
}
