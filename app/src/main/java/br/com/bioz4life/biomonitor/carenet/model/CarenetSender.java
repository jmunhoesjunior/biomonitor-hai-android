package br.com.bioz4life.biomonitor.carenet.model;

import java.io.Serializable;

public class CarenetSender implements Serializable {
    public String fabricante;
    public String modelo;
    public String dataHoraMensagem;
    public String uti;
    public String tipoMensagem;
    public String idiomaMensagem;
    public String leito;
    public String hospital;
    public Paciente paciente;
    public String leitoStatus;
}
