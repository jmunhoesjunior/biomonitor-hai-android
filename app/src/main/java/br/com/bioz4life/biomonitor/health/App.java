package br.com.bioz4life.biomonitor.health;

import android.app.Application;

import androidx.databinding.ObservableBoolean;

import com.linktop.MonitorDataTransmissionManager;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by ccl on 2017/2/7.
 */

public class App extends Application {

    //public final static ObservableBoolean isLogin = new ObservableBoolean(false);

    public final static ObservableBoolean isShowUploadButton = new ObservableBoolean(false);
    //If you are using your own custom Bluetooth connection code，set this parameter is true;
    //If you are using Bluetooth connection code had been built in SDK library,set this parameter is false;
    //You can see in this demo project separately how to build the code when this boolean parameter value is true or false.
    public final static boolean isUseCustomBleDevService = false;

    @Override
    public void onCreate() {
        //true: enable SDK logs,false :disable SDK logs
        MonitorDataTransmissionManager.isDebug(true);

        /* Database Realm */
        Realm.init(this);
        // Sempre que enviar uma versao nova com a base alterada é necessario ou implementar uma migracao do banco,
        // ou apagar o banco
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("bioz4life.biomotor.realm")
                //.encryptionKey(getKey())
                .schemaVersion(23)
                //.modules(new MySchemaModule())
                //.migration(new MyMigration())
                .deleteRealmIfMigrationNeeded()
                .build();

        // Use the config
        Realm.setDefaultConfiguration(config);

        super.onCreate();
    }
}
