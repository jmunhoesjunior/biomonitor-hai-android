package br.com.bioz4life.biomonitor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.ECG;
import br.com.bioz4life.biomonitor.widget.EcgWaveView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class AdapterECG extends RecyclerView.Adapter {

    private Context mContext;
    private List<ECG> mItems;
    private OnClickECG onClickECG;
    public interface OnClickECG{
        void onClickECGItem(int position, ECG ecg);
    }
    public AdapterECG(Context mContext, List<ECG> mItems, OnClickECG onClickECG){
        this.mContext = mContext;
        this.mItems = mItems;
        this.onClickECG = onClickECG;
    }

    private class Holder extends RecyclerView.ViewHolder {

        TextView tvDate;
        TextView tvrr_max_value;
        TextView tvrr_min_value;

        TextView tvduration_value;
        TextView tvhr_value;

        TextView tvhrv_value;
        TextView tvmood_value;

        TextView tvSpeed;
        TextView tvGain;

        TextView tvbr;

        CardView cardView;

        EcgWaveView ecg_draw_chart;

        public Holder(@NonNull View itemView) {
            super(itemView);

            tvDate = (TextView)itemView.findViewById(R.id.tvDate);
            tvrr_max_value = (TextView)itemView.findViewById(R.id.tvrr_max_value);
            tvrr_min_value = (TextView) itemView.findViewById(R.id.tvrr_min_value);

            tvduration_value = (TextView) itemView.findViewById(R.id.tvduration_value);
            tvhr_value = (TextView) itemView.findViewById(R.id.tvhr_value);

            tvhrv_value = (TextView) itemView.findViewById(R.id.tvhrv_value);
            tvmood_value = (TextView) itemView.findViewById(R.id.tvmood_value);

            tvSpeed = (TextView) itemView.findViewById(R.id.tvSpeed);
            tvGain = (TextView) itemView.findViewById(R.id.tvGain);
            tvbr = (TextView) itemView.findViewById(R.id.tvbr);

            cardView = (CardView)itemView.findViewById(R.id.cardView);
            ecg_draw_chart = (EcgWaveView)itemView.findViewById(R.id.ecg_draw_chart);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(mContext)
                .inflate(R.layout.item_ecg, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder,final int i) {

        final ECG ecg = mItems.get(i);
        Holder holder  = (Holder)viewHolder;

        holder.tvrr_max_value.setText("RR max: " + String.valueOf(ecg.getRrMax()) + " ms");
        holder.tvrr_min_value.setText("RR min: " + String.valueOf(ecg.getRrMin()) + " ms");
        holder.tvduration_value.setText("Duração: " + String.valueOf(ecg.getDuration()) + " seg.");
        holder.tvhr_value.setText("Freq. Cardíaca: " + String.valueOf(ecg.getHr()) + " bpm");
        holder.tvhrv_value.setText("Variabilidade da frequência cardíaca: " + String.valueOf(ecg.getHrv()));
        holder.tvbr.setText("Freq. Respiratória: " + String.valueOf(ecg.getBr()));
        holder.tvmood_value.setText("Índice BioZ: " + String.valueOf(ecg.getMood()));

        /*float gain;
        int pager;

        if(ecg.getGain() == 0.5f){
            gain = 5;
        }
        else if(ecg.getGain() == 1){
            gain = 10;
        }
        else{
            gain = 20;
        }

        if(ecg.getPagerSpeed() == 1){
            pager = 25;
        }
        else{
            pager = 50;
        }*/
        holder.tvGain.setText("Amplitude: " + String.valueOf(Double.valueOf(ecg.getGain()).intValue()) + " mm/mV");
        holder.tvSpeed.setText("Velocidade: " + String.valueOf(ecg.getPagerSpeed()) + " mm/s");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM HH:mm");

        //to convert Date to String, use format method of SimpleDateFormat class.
        String strDate = dateFormat.format(ecg.getCreatedDate());
        holder.tvDate.setText("Data: " + strDate);

        float gain;
        int pager;

        if(ecg.getGain() == 5){
            gain = 0.5f;
        }
        else if(ecg.getGain() == 10){
            gain = 1;
        }
        else{
            gain = 2;
        }

        if(ecg.getPagerSpeed() == 25){
            pager = 1;
        }
        else{
            pager = 2;
        }
        holder.ecg_draw_chart.setPagerSpeed(pager);
        holder.ecg_draw_chart.setGain(gain);

        showChart(ecg.getWave(),holder.ecg_draw_chart);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickECG.onClickECGItem(i,ecg);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mItems == null)
            return 0;
        return mItems.size();
    }

    private void showChart(String wave, final EcgWaveView ewvECGLarge) {
        Observable.just(wave)
                .subscribeOn(Schedulers.newThread()) // 指定 subscribe() 发生在子线程
                .map(new Func1<String, String[]>() {
                    @Override
                    public String[] call(String s) {
                        return s.split(",");
                    }
                })
                .map(new Func1<String[], List<Float>>() {
                    @Override
                    public List<Float> call(String[] strings) {
                        final List<Float> list = new ArrayList<>();
                        for (String str : strings) {
                            list.add(Float.valueOf(str));
                        }
                        return list;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())// 指定 Subscriber 的回调发生在UI程
                .subscribe(new Subscriber<List<Float>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Float> list) {
                        ewvECGLarge.preparePoints(list);
                    }
                });

    }
}
