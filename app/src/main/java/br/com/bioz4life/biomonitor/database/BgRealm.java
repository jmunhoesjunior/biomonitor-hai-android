package br.com.bioz4life.biomonitor.database;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BgRealm extends RealmObject {

    @PrimaryKey
    public String id;
    // CountryCode+DDD+Phone
    public String user;
    public Date createdDate;
    public boolean isSync;

    public long ts = 0;
    public double value = 0.0d;
}
