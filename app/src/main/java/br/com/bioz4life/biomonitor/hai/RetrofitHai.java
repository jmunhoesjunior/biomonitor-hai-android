package br.com.bioz4life.biomonitor.hai;

import com.google.gson.JsonElement;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import br.com.bioz4life.biomonitor.hai.models.HaiRoot;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHai {

    private static Retrofit retrofit = null;
    private String baseUrl = "http://206.189.196.226/hai/";
    private Retrofit getClient(){

        if (retrofit == null) {

            OkHttpClient client = new OkHttpClient.Builder()
                    .followRedirects(false)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public okhttp3.Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            okhttp3.Response response = chain.proceed(chain.request());
                            if (response.code() == 307 || response.code() == 308) {
                                request = request.newBuilder()
                                        .url(response.header("Location"))
                                        .build();
                                response = chain.proceed(request);
                            }
                            return response;
                        }
                    })
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    public void send(HaiRoot request, final OnResponseListener listener) {

        //String json = new Gson().toJson(request);

        IHai carenet = new RetrofitHai().getClient().create(IHai.class);

        Call<JsonElement> call = carenet.add(request);

        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.code() == 200) {
                    listener.onResponse(true, null);
                } else {
                    String error = response.errorBody().toString();
                    listener.onResponse(false, error);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                String error =  t.getMessage();
                listener.onResponse(false, error);
            }
        });
    }


    public interface OnResponseListener {
        void onResponse(Boolean success, String message);
    }
}
