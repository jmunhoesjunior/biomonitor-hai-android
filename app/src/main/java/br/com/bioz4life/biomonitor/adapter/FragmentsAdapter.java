package br.com.bioz4life.biomonitor.adapter;

import android.util.SparseArray;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import br.com.bioz4life.biomonitor.fragment.BaseFragment;

/**
 * Created by ccl on 2016/4/29.
 */
public class FragmentsAdapter extends FragmentPagerAdapter {

    private SparseArray<BaseFragment> fragmentSparseArr;

    public FragmentsAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setFragments(SparseArray<BaseFragment> fragmentSparseArray) {
        this.fragmentSparseArr = fragmentSparseArray;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        if (null != fragmentSparseArr) return fragmentSparseArr.get(position);
        return null;
    }

    @Override
    public int getCount() {
        if (null != fragmentSparseArr) return fragmentSparseArr.size();
        return 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (fragmentSparseArr == null)
            return "";
        if (fragmentSparseArr.get(position) == null)
            return "";
        return fragmentSparseArr.get(position).getTitle();
    }
}
