package br.com.bioz4life.biomonitor.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.hendrix.pdfmyxml.PdfDocument;
import com.hendrix.pdfmyxml.viewRenderer.AbstractViewRenderer;
import com.pdfview.PDFView;

import java.io.File;
import java.util.Date;
import java.util.UUID;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.Bg;
import br.com.bioz4life.biomonitor.bean.Bp;
import br.com.bioz4life.biomonitor.bean.Bt;
import br.com.bioz4life.biomonitor.bean.ECG;
import br.com.bioz4life.biomonitor.bean.SPO2H;
import br.com.bioz4life.biomonitor.carenet.model.Resultado;
import br.com.bioz4life.biomonitor.database.RealmUtilityBg;
import br.com.bioz4life.biomonitor.database.RealmUtilityBp;
import br.com.bioz4life.biomonitor.database.RealmUtilityBt;
import br.com.bioz4life.biomonitor.database.RealmUtilityECG;
import br.com.bioz4life.biomonitor.database.RealmUtilitySPO2;
import br.com.bioz4life.biomonitor.database.RealmUtilityUserAccount;
import br.com.bioz4life.biomonitor.database.UserAccountRealm;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.Utility;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class PdfViewActivity extends BaseActivity {


    @BindView(R.id.pdfView)
    PDFView pdfView;

    private String path;

    public static final String READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
    public static final String WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";

    public static final int PERMISSION_CODE = 1000;

    Realm realm;

    PdfDocument doc;

    UserAccountRealm userAccountRealm;
    Bp bp;
    Bt bt;
    Bg bg;
    SPO2H spo2H;
    ECG ecg;

    String name = "";
    String cpf = "";
    String gender = "";
    String birth = "";

    String data = "";

    String bpValue = "";
    String spo2Value = "";
    String tempValue = "";
    String bgValue = "";

    String brValue = "";
    String ecgHrValue = "";
    String tsValue = "";
    String rrMinValue = "";
    String rrMaxValue = "";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pdf_view);

        setUpActionBar("Pdf", getString(R.string.app_name), true);

        ButterKnife.bind(this);

        realm = realm.getDefaultInstance();


        // buscar o ultimo resultado de exames e os dados do usuário
        userAccountRealm = RealmUtilityUserAccount.find(realm, LocalPrefs.getPhoneNumber(PdfViewActivity.this));

        // bp
        bp = RealmUtilityBp.getLastSample(realm, LocalPrefs.getPhoneNumber(PdfViewActivity.this));

        // bt
        bt = RealmUtilityBt.getLastSample(realm, LocalPrefs.getPhoneNumber(PdfViewActivity.this));

        // bg
        bg = RealmUtilityBg.getLastSample(realm, LocalPrefs.getPhoneNumber(PdfViewActivity.this));

        // spo2h
        spo2H = RealmUtilitySPO2.getLastSample(realm, LocalPrefs.getPhoneNumber(PdfViewActivity.this));

        // ecg
        ecg = RealmUtilityECG.getLastSample(realm, LocalPrefs.getPhoneNumber(PdfViewActivity.this));

        if (userAccountRealm != null) {
            if (userAccountRealm.FullName != null) {
                name = userAccountRealm.FullName;
            }
            if (userAccountRealm.Cpf != null) {
                cpf = userAccountRealm.Cpf;
            }
            if(userAccountRealm.Gender != null) {
                gender = userAccountRealm.Gender;
            }
            if (userAccountRealm.Birthday != null) {
                birth = Utility.formatDate(userAccountRealm.Birthday,"yyyy-MM-dd","dd/MM/yyyy");
                if (!birth.isEmpty()) {
                    // calcula a idade (29a)
                    int age = Utility.getAge(userAccountRealm.Birthday,"yyyy-MM-dd");
                    birth = birth + "("+ String.valueOf(age) + "a)";
                }
            }
        }

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        Date now = new java.util.Date();
        data = df.format("dd/MM/yyyy", now).toString();
        data = data + " as " + df.format("hh:mm",now).toString();

        if (bp != null) {
            bpValue = String.valueOf(bp.getSbp()) + "/" + String.valueOf(bp.getDbp()) + "mmHg (" + String.valueOf(bp.getHr()) + "bpm)";
        }
        if (bt != null) {
            tempValue = String.valueOf(bt.getTemp()) + "oC";
        }
        if(spo2H != null) {
            spo2Value = String.valueOf(spo2H.getValue()) + "% (" + String.valueOf(spo2H.getHr())+"bpm)";
        }
        if (bg != null) {
            Resultado r1 = new Resultado();
            double mgdl = 18.018 * bg.getValue();
            int mgdlInt = (int)Math.round(mgdl);
            bgValue = String.valueOf(mgdlInt) + "mg/dL";
        }
        if(ecg != null) {
            // freq respiratoria
            brValue = String.valueOf(ecg.getBr()) + "rpm";

            // freq cardiaca
            ecgHrValue = String.valueOf(ecg.getHr())+"bpm";

            // variabilidade
            tsValue = String.valueOf(ecg.getTs())+"ms";

            //rr min
            rrMinValue = String.valueOf(ecg.getRrMin())+"ms";

            // rr max
            rrMaxValue = String.valueOf(ecg.getRrMax()) +"ms";
        }
    }

    public void createPdf() {
        AbstractViewRenderer page = new AbstractViewRenderer(this, R.layout.pdf_layout) {
            @Override
            protected void initView(View view) {
                /* Infos user */

                TextView tvName = (TextView)view.findViewById(R.id.tvName);
                TextView tvCpf = (TextView)view.findViewById(R.id.tvCpf);
                TextView tvGender = (TextView)view.findViewById(R.id.tvGender);
                TextView tvBirthday = (TextView)view.findViewById(R.id.tvBirthday);
                TextView tvDate = (TextView)view.findViewById(R.id.tvDate);


                tvName.setText("Nome: " + name);
                tvCpf.setText("Cpf: " + cpf);
                tvGender.setText("Sexo: " + gender);
                tvBirthday.setText("Dt.Nasc.: " + birth);
                tvDate.setText("Data: " + data);

                /* Infos indicators selection */

                TextView tvBp = (TextView)view.findViewById(R.id.tvBp);
                TextView tvSpoh = (TextView)view.findViewById(R.id.tvSpoh);
                TextView tvBt = (TextView)view.findViewById(R.id.tvBt);
                TextView tvBg = (TextView)view.findViewById(R.id.tvBg);
                TextView tvEcgHr = (TextView)view.findViewById(R.id.tvEcgHr);
                TextView tvEcgBr = (TextView)view.findViewById(R.id.tvEcgBr);
                TextView tvEcgTs = (TextView)view.findViewById(R.id.tvEcgTs);
                TextView tvEcgRrMax = (TextView)view.findViewById(R.id.tvEcgRrMax);
                TextView tvEcgRrMin = (TextView)view.findViewById(R.id.tvEcgRrMin);

                tvBp.setText("PRESSAO ARTERIAL  " + bpValue);
                tvSpoh.setText("OXIMETRIA  " + spo2Value);
                tvBt.setText("TEMPERATURA  " + tempValue);
                tvBg.setText("GLICEMIA  " + bgValue);

                tvEcgHr.setText("Freq.Cardiaca  " + ecgHrValue);
                tvEcgBr.setText("Freq. Respiratoria  " +  brValue);
                tvEcgTs.setText("Variabilidade FC  " + tsValue);
                tvEcgRrMax.setText("R-R maximo  " + rrMaxValue);
                tvEcgRrMin.setText("R-R minimo  " + rrMinValue);
            }
        };

        // you can reuse the bitmap if you want
        page.setReuseBitmap(true);

        doc = new PdfDocument(this);

        // add as many pages as you have
        doc.addPage(page);

        doc.setRenderWidth(1240);
        doc.setRenderHeight(1754);
        doc.setOrientation(PdfDocument.A4_MODE.PORTRAIT);
        doc.setProgressTitle(R.string.gen_please_wait);
        doc.setProgressMessage(R.string.gen_pdf_file);
        doc.setFileName("printer-"+ UUID.randomUUID().toString());
        doc.setSaveDirectory(getExternalFilesDir(null));
        doc.setInflateOnMainThread(false);
        doc.setListener(new PdfDocument.Callback() {
            @Override
            public void onComplete(File file) {
                Log.i(PdfDocument.TAG_PDF_MY_XML, "Complete");
                path = doc.getFile().getAbsolutePath();

                pdfView.fromFile(new File(path)).show();
            }

            @Override
            public void onError(Exception e) {

                Log.i(PdfDocument.TAG_PDF_MY_XML, "Error");
            }
        });

        doc.createPdf(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PERMISSION_CODE) {
            if (resultCode == RESULT_OK) {
                createPdf();
            }
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        int permissionRead = ContextCompat.checkSelfPermission(this,
                READ_EXTERNAL_STORAGE);

        int permissionWrite = ContextCompat.checkSelfPermission(this,
                WRITE_EXTERNAL_STORAGE);

        if (permissionRead != PackageManager.PERMISSION_GRANTED || permissionWrite != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE},
                    PERMISSION_CODE
            );

            return;
        } else {
            createPdf();
        }
    }

    @OnClick(R.id.btExport)
    public void onExport() {
        //File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "example.pdf");
        Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", new File(path));

        Intent share = new Intent();
        share.setAction(Intent.ACTION_SEND);
        share.setType("application/pdf");
        share.putExtra(Intent.EXTRA_STREAM, uri);
        //share.setPackage("com.whatsapp");

        startActivity(share);
    }

    @Override
    protected void onDestroy() {
        if(realm != null) {
            realm.close();
        }
        if(doc != null){
            doc.dispose();
        }
        super.onDestroy();
    }
}
