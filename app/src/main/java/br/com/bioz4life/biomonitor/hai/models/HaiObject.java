package br.com.bioz4life.biomonitor.hai.models;

import java.io.Serializable;

public class HaiObject implements Serializable {
    public String label;
    public String value;
    public String unit;
}
