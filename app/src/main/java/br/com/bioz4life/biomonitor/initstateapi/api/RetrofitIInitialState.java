package br.com.bioz4life.biomonitor.initstateapi.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import br.com.bioz4life.biomonitor.carenet.RetrofitCarenet;
import br.com.bioz4life.biomonitor.initstateapi.model.ModelInitialState;
import br.com.bioz4life.biomonitor.initstateapi.model.ModelPayloadInitialState;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitIInitialState {

    private Retrofit retrofit = null;
    private String baseUrl = "https://groker.init.st/";

    private Retrofit getClient(String phone){

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        Request.Builder builder = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("X-IS-AccessKey", "ist_epNoRUZh4An4uRrf-65wD9OBGG5eQkva")
                                .header("Accept-Version", "~0")
                                .method(original.method(), original.body());
                        if(phone != null) {
                            builder.header("X-IS-BucketKey", phone);
                        }
                        return chain.proceed(builder.build());
                    }
                })
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }

    public void createBucket(ModelPayloadInitialState request, RetrofitCarenet.OnResponseListener listener) {

        IApiInitialState api = new RetrofitIInitialState().getClient(null).create(IApiInitialState.class);

        Call<ResponseBody> call = api.createBucket(request);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.code() == 201 || response.code() == 204) {
                    listener.onResponse(true);
                } else {
                    listener.onResponse(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onResponse(false);
            }
        });
    }

    public void sendData(String phone, ArrayList<ModelInitialState> request, RetrofitCarenet.OnResponseListener listener) {
        IApiInitialState api = new RetrofitIInitialState().getClient(phone).create(IApiInitialState.class);

        Call<ResponseBody> call = api.sendData(request);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if (response.code() == 200 || response.code() == 204) {
                    listener.onResponse(true);
                } else {
                    listener.onResponse(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onResponse(false);
            }
        });
    }
}
