package br.com.bioz4life.biomonitor.database;

import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.bean.Bp;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmUtilityBp {

    public static List<Bp>getAll(Realm realm, String user){
        List<Bp> result = new ArrayList<>();
        RealmResults<BpRealm> realmRealmResults = realm
                .where(BpRealm.class)
                .equalTo("user", user)
                .sort("createdDate",Sort.DESCENDING)
                .findAll();

        if (realmRealmResults != null) {
            for(BpRealm item : realmRealmResults){
                Bp bp = new Bp();
                bp.setCreatedDate(item.createdDate);
                bp.setDbp(item.dbp);
                bp.setHr(item.hr);
                bp.setSbp(item.sbp);
                bp.setTs(item.ts);
                bp.setId(item.id);
                result.add(bp);
            }
        }

        return result;
    }
    public static List<Bp> getLastSamplesLimit5(Realm realm, String user){

        List<Bp> result = new ArrayList<>();
        RealmResults<BpRealm> realmRealmResults = realm
                .where(BpRealm.class)
                .equalTo("user", user)
                .sort("createdDate",Sort.DESCENDING)
                .limit(5)
                .findAll();

        if (realmRealmResults != null) {
            for(BpRealm item : realmRealmResults){
                Bp bp = new Bp();
                bp.setCreatedDate(item.createdDate);
                bp.setDbp(item.dbp);
                bp.setHr(item.hr);
                bp.setSbp(item.sbp);
                bp.setTs(item.ts);
                bp.setId(item.id);
                result.add(bp);
            }
        }

        return result;
    }

    public static Bp getLastSample(Realm realm, String user){

        Bp result = null;
        RealmResults<BpRealm> realmRealmResults = realm
                .where(BpRealm.class)
                .equalTo("user", user)
                .sort("createdDate",Sort.DESCENDING)
                .limit(1)
                .findAll();

        if (realmRealmResults != null) {
            for(BpRealm item : realmRealmResults){
                result = new Bp();
                result.setCreatedDate(item.createdDate);
                result.setDbp(item.dbp);
                result.setHr(item.hr);
                result.setSbp(item.sbp);
                result.setTs(item.ts);
                result.setId(item.id);
            }
        }

        return result;
    }


    public static Bp getUnsyncBp(Realm realm, String user){
        Bp bp = null;
        BpRealm itemRealm = realm
                .where(BpRealm.class)
                .equalTo("user", user)
                .equalTo("isSync",false)
                .sort("createdDate", Sort.ASCENDING)
                .findFirst();

        if (itemRealm != null) {
            bp = new Bp();
            bp.setCreatedDate(itemRealm.createdDate);
            bp.setId(itemRealm.id);
            bp.setTs(itemRealm.ts);
            bp.setSbp(itemRealm.sbp);
            bp.setHr(itemRealm.hr);
            bp.setDbp(itemRealm.dbp);
        }
        return bp;
    }

    public static void addFromServer(Realm realm, List<Bp> list, String user){
        if (list != null) {
            realm.beginTransaction();
            for(Bp c : list){
                BpRealm bpRealm = new BpRealm();

                bpRealm.id = c.getId();
                bpRealm.user = user;
                bpRealm.createdDate = c.getCreatedDate();
                bpRealm.hr = c.getHr();
                bpRealm.dbp = c.getDbp();
                bpRealm.sbp = c.getSbp();
                bpRealm.ts = c.getTs();
                bpRealm.isSync = true;
                realm.copyToRealm(bpRealm);
            }
            realm.commitTransaction();
        }
    }

    public static void deleteDataSyncronized(Realm realm, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<BpRealm> results = realm
                        .where(BpRealm.class)
                        .equalTo("user", user)
                        .equalTo("isSync",true)
                        .findAll();

                if (results != null) {
                    results.deleteAllFromRealm();
                }
            }
        });
    }

    public static void updateStatus(Realm realm, final String id, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                BpRealm itemRealm = realm
                        .where(BpRealm.class)
                        .equalTo("id", id)
                        .equalTo("user", user)
                        .findFirst();

                if(itemRealm != null){
                    itemRealm.isSync = true;
                }
            }
        });
    }
}

