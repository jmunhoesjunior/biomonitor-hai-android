package br.com.bioz4life.biomonitor.database;

import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.bean.SPO2H;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmUtilitySPO2 {

    public static List<SPO2H>getAll(Realm realm, String user){
        List<SPO2H> result = new ArrayList<>();
        RealmResults<SPO2Realm> realmRealmResults = realm
                .where(SPO2Realm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .findAll();

        if (realmRealmResults != null) {
            for(SPO2Realm item : realmRealmResults){
                SPO2H spo2H = new SPO2H();
                spo2H.setCreatedDate(item.createdDate);
                spo2H.setTs(item.ts);
                spo2H.setId(item.id);

                spo2H.setValue(item.value);
                spo2H.setHr(item.hr);
                result.add(spo2H);
            }
        }

        return result;
    }
    public static List<SPO2H> getLastSamplesLimit5(Realm realm, String user){

        List<SPO2H> result = new ArrayList<>();
        RealmResults<SPO2Realm> realmRealmResults = realm
                .where(SPO2Realm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .limit(5)
                .findAll();

        if (realmRealmResults != null) {
            for(SPO2Realm item : realmRealmResults){
                SPO2H spo2H = new SPO2H();
                spo2H.setCreatedDate(item.createdDate);
                spo2H.setTs(item.ts);
                spo2H.setId(item.id);
                spo2H.setValue(item.value);
                spo2H.setHr(item.hr);
                result.add(spo2H);
            }
        }

        return result;
    }

    public static SPO2H getLastSample(Realm realm, String user){

        SPO2H result = null;
        RealmResults<SPO2Realm> realmRealmResults = realm
                .where(SPO2Realm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .limit(1)
                .findAll();

        if (realmRealmResults != null) {
            for(SPO2Realm item : realmRealmResults){
                result = new SPO2H();
                result.setCreatedDate(item.createdDate);
                result.setTs(item.ts);
                result.setId(item.id);
                result.setValue(item.value);
                result.setHr(item.hr);
            }
        }

        return result;
    }

    public static SPO2H getUnsyncSpo2(Realm realm, String user){
        SPO2H spo2H = null;
        SPO2Realm itemRealm = realm
                .where(SPO2Realm.class)
                .equalTo("user", user)
                .equalTo("isSync",false)
                .sort("createdDate", Sort.ASCENDING)
                .findFirst();

        if (itemRealm != null) {
            spo2H = new SPO2H();
            spo2H.setCreatedDate(itemRealm.createdDate);
            spo2H.setId(itemRealm.id);
            spo2H.setTs(itemRealm.ts);
            spo2H.setHr(itemRealm.hr);
            spo2H.setValue(itemRealm.value);
        }
        return spo2H;
    }

    public static void addFromServer(Realm realm, List<SPO2H> list, String user){
        if (list != null) {
            realm.beginTransaction();
            for(SPO2H c : list){
                SPO2Realm spo2Realm = new SPO2Realm();

                spo2Realm.id = c.getId();
                spo2Realm.user = user;
                spo2Realm.createdDate = c.getCreatedDate();
                spo2Realm.value = c.getValue();
                spo2Realm.ts = c.getTs();
                spo2Realm.isSync = true;
                spo2Realm.hr = c.getHr();
                realm.copyToRealm(spo2Realm);
            }
            realm.commitTransaction();
        }
    }

    public static void deleteDataSyncronized(Realm realm, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<SPO2Realm> results = realm
                        .where(SPO2Realm.class)
                        .equalTo("user", user)
                        .equalTo("isSync",true)
                        .findAll();

                if (results != null) {
                    results.deleteAllFromRealm();
                }
            }
        });
    }

    public static void updateStatus(Realm realm, final String id, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                SPO2Realm itemRealm = realm
                        .where(SPO2Realm.class)
                        .equalTo("id", id)
                        .equalTo("user", user)
                        .findFirst();

                if(itemRealm != null){
                    itemRealm.isSync = true;
                }
            }
        });
    }
}
