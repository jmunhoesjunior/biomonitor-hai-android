package br.com.bioz4life.biomonitor.widget.wave;

import android.graphics.Canvas;
import android.graphics.Paint;

import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.widget.EcgBackgroundView;


/**
 * Created by ccl on 2017/8/30.
 * 画ECG波形图实例
 */

public class ECGDrawWave extends DrawWave<Integer> {

    public final static int ECG_PAGER_SPEED_25 = 1;
    public final static int ECG_PAGER_SPEED_50 = 2;

    public final static float ECG_CALIBRATION_5 = 0.5f;
    public final static float ECG_CALIBRATION_10 = 1.0f;
    public final static float ECG_CALIBRATION_20 = 2.0f;

    //定义ECG波的颜色
    private final static int waveColor = 0xffa80bfd;
    //定义波的线粗
    private final static float waveStrokeWidth = 2f;

    /**
     * Y轴增益
     * calibration = 0.5  5mm/mV 波形增幅5毫米左右
     * calibration = 1.0 10mm/mV 波形增幅10毫米左右
     * calibration = 2.0 20mm/mV 波形增幅20毫米左右
     */
    private float calibration = 1.0f;// 0.5;1 ;2

    private float mViewWidth;
    private float mViewHeight;
    private float xS;
    private float dataSpacing;
    private Paint mWavePaint;

    public ECGDrawWave() {
        super();
        mWavePaint = newPaint(waveColor, waveStrokeWidth);
    }

    @Override
    public void initWave(float width, float height) {
        mViewWidth = width;
        mViewHeight = height;
        xS = EcgBackgroundView.xS;//控件每毫米的像素宽
        final float dataPerLattice = EcgBackgroundView.DATA_PER_SEC / (25.0f);//每格波形数据点数
        allDataSize = (int) (EcgBackgroundView.totalLattices * dataPerLattice);
        dataSpacing = xS / dataPerLattice;//每个数据点间距。
    }

    @Override
    public int getWidthMeasureSpec() {
        return (int) ((2 + dataList.size()) * dataSpacing);
    }

    @Override
    public void drawWave(Canvas canvas) {
        final List<Integer> list = new ArrayList<>();
        list.addAll(dataList);
        int size = list.size();
        if (size >= 2) {
            for (int i = 0; i < size - 1; i++) {
                Integer dataCurr;
                Integer dataNext;
                try {
                    dataCurr = list.get(i);
                } catch (IndexOutOfBoundsException e) {
                    dataCurr = list.get(i - 1);
                }
                try {
                    dataNext = list.get(i + 1);
                } catch (IndexOutOfBoundsException e) {
                    dataNext = list.get(i);
                }
                float x1 = getX(i, size);
                float x2 = getX(i + 1, size);
                float y1 = getY(dataCurr);
                float y2 = getY(dataNext);
                canvas.drawLine(x1, y1, x2, y2, mWavePaint);
            }
        }
    }

    @Override
    float getX(int value, int size) {
        try {
            return mViewWidth - (dataSpacing * (size - 1 - value));
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    float getY(Integer data) {
        try {
            return (float) (mViewHeight / 2 + data * 18.3 / 128 * xS / 100 * calibration);
        } catch (NullPointerException e) {
            return 0;
        }
    }

    /**
     * 设置增益
     *
     * @param calibration 增益值
     */
    public void setCalibration(float calibration) {
        this.calibration = calibration;
        if (view != null) view.postInvalidate();
    }

    /**
     * 设置时间基准（走纸速度）
     *
     * @param pagerSpeed 时间基准（走纸速度）值
     */
    public void setPagerSpeed(int pagerSpeed) {
        /*
      X轴时间基准（走纸速度）
      pagerSpeed = 1: 25mm/s 25毫米（小格）每秒
      pagerSpeed = 2 50mm/s  50毫米（小格）每秒
     */
        final float dataPerLattice = EcgBackgroundView.DATA_PER_SEC / (25.0f * pagerSpeed);//每格波形数据点数
        allDataSize = (int) (EcgBackgroundView.totalLattices * dataPerLattice);
        dataSpacing = xS / dataPerLattice;//每个数据点间距。
        if (view != null) view.postInvalidate();
    }

}
