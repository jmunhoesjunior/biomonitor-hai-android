package br.com.bioz4life.biomonitor.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.ObservableInt;
import androidx.databinding.ViewDataBinding;

import com.linktop.MonitorDataTransmissionManager;
import com.linktop.constant.BluetoothState;
import com.linktop.constant.DeviceInfo;
import com.linktop.constant.WareType;
import com.linktop.infs.OnBatteryListener;
import com.linktop.infs.OnBleConnectListener;
import com.linktop.infs.OnDeviceInfoListener;
import com.linktop.infs.OnDeviceVersionListener;
import com.linktop.whealthService.BleDevManager;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.binding.ObservableString;
import br.com.bioz4life.biomonitor.databinding.FragmentMonitorInfoBinding;
import br.com.bioz4life.biomonitor.health.App;
import br.com.bioz4life.biomonitor.health.HcService;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.PermissionManager;
import im.delight.android.location.SimpleLocation;
import rx.Subscription;

/**
 * Created by ccl on 2017/2/7.
 * MonitorInfoFragment
 * 健康检测仪基本操作Demo页面（除测量外）
 */

public class MonitorInfoFragment extends BaseFragment
        implements OnDeviceVersionListener, OnBleConnectListener, OnBatteryListener, OnDeviceInfoListener {

    private static final int REQUEST_OPEN_BT = 0x23;

    private final ObservableString btnText = new ObservableString("Ligue o Bluetooth");
    private final ObservableString power = new ObservableString("");
    private final ObservableString id = new ObservableString("");//ID do dispositivo atualmente selecionado
    private final ObservableString key = new ObservableString("");//Chave de dispositivo atualmente selecionada
    private final ObservableString softVer = new ObservableString("");
    private final ObservableString hardVer = new ObservableString("");
    private final ObservableString firmVer = new ObservableString("");
    //private final ObservableBoolean isLogin = App.isLogin;
    private final ObservableInt isDevBind = new ObservableInt(0);
    private boolean showScanList;
    private BleDeviceListDialogFragment mBleDeviceListDialogFragment;

    //private BindDevListAdapter mAdapter;

    private Subscription subscription;

    private SimpleLocation.Listener listener;

    public MonitorInfoFragment() {
    }

    @Override
    public String getTitle() {
        return "Conexão/Dispositivo";
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected ViewDataBinding onCreateBindingView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentMonitorInfoBinding binding = setBindingContentView(inflater, R.layout.fragment_monitor_info, container);
        binding.setFrag(this);
        binding.setBtnText(btnText);
        binding.setPower(power);
        binding.setId(id);
        binding.setKey(key);
        binding.setSoftVer(softVer);
        binding.setHardVer(hardVer);
        binding.setFirmVer(firmVer);
        //binding.setIsLogin(isLogin);
        binding.setIsBind(isDevBind);
        binding.radioG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                showScanList = checkedId == R.id.radioB1;
            }
        });
        if (!App.isUseCustomBleDevService) {
            onBleState(MonitorDataTransmissionManager.getInstance().getBleState());
        }
        /*if (isLogin.get()) {
            mAdapter = new BindDevListAdapter(getContext(), id);
            binding.setRecyclerAdapter(mAdapter);
            getDevList(false);
        }*/
        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (App.isUseCustomBleDevService) {
            BleDevManager bleDevManager = mHcService.getBleDevManager();
            mHcService.setOnDeviceVersionListener(this);
            bleDevManager.getBatteryTask().setBatteryStateListener(this);
            bleDevManager.getDeviceTask().setOnDeviceInfoListener(this);
        }
        else {
            MonitorDataTransmissionManager.getInstance().setOnBleConnectListener(this);
            MonitorDataTransmissionManager.getInstance().setOnBatteryListener(this);
            MonitorDataTransmissionManager.getInstance().setOnDevIdAndKeyListener(this);
            MonitorDataTransmissionManager.getInstance().setOnDeviceVersionListener(this);
        }
    }

    @Override
    public void onDestroy() {
        if (App.isUseCustomBleDevService) {
            if (mHcService.isConnected) {
                mHcService.disConnect();
            }
        }
        else {
            //A demonstração usa a interface como interface principal (ao contrário do detector de integridade,
            // é claro, também pode ser considerada como a atividade superior), e a conexão Bluetooth deve ser
            // interrompida antes de destruir a interface principal.
            if (MonitorDataTransmissionManager.getInstance().isConnected())
                MonitorDataTransmissionManager.getInstance().disConnectBle();
        }
        /*if (isLogin.get()) {
            //A conexão longa é iniciada quando o dispositivo está conectado ao Bluetooth e o dispositivo é desconectado do Bluetooth.
            //HmLoadDataTool.getInstance().destroyCssSocket();
            isDevBind.set(0);
        }*/
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permissionGranted = PermissionManager.isPermissionGranted(grantResults);
        switch (requestCode) {
            case PermissionManager.requestCode_location:
                if (permissionGranted) {
                    try {
                        Thread.sleep(1000L);
                        clickConnect(null);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getContext(), "Nenhum direito de segmentação", Toast.LENGTH_SHORT).show();
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_OPEN_BT:
                // Resultado de inicialização do Bluetooth
                // Resultado de inicialização do Bluetooth
                Toast.makeText(getContext(), resultCode == Activity.RESULT_OK ? "Bluetooth está ligado" : "Bluetooth não conseguiu abrir", Toast.LENGTH_SHORT).show();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void reset() {
        power.set("");
        id.set("");
        key.set("");
        softVer.set("");
        hardVer.set("");
        firmVer.set("");
        /*if (mActivity != null) {
            mActivity.setBatery(null,false);
            mActivity.setDeviceID(null,false);
        }*/
    }

    /**
     * 设备版本号
     *
     * @param wareType 版本类型
     *                 {@link WareType#VER_FIRMWARE 固件版本}
     *                 {@link WareType#VER_HARDWARE 硬件版本}
     *                 {@link WareType#VER_SOFTWARE 软件版本}
     */
    @Override
    public void onDeviceVersion(@WareType int wareType, String version) {
        switch (wareType) {
            case WareType.VER_SOFTWARE:
                softVer.set(version);
                if (mHcService != null) {
                    mHcService.dataQuery(HcService.DATA_QUERY_HARDWARE_VER);
                }
                break;
            case WareType.VER_HARDWARE:
                hardVer.set(version);
                if (mHcService != null) {
                    mHcService.dataQuery(HcService.DATA_QUERY_FIRMWARE_VER);
                }
                break;
            case WareType.VER_FIRMWARE:
                firmVer.set(version);
                if (mHcService != null) {
                    mHcService.dataQuery(HcService.DATA_QUERY_CONFIRM_ECG_MODULE_EXIST);
                }
                break;
        }

    }

    /******

     Os dois valores de retorno de chamada acima podem ser salvos no SP de acordo com o ID do dispositivo.
     * Isso pode ser feito com algum dispositivo não conectado, mas o ID do dispositivo é conhecido.
     * Obter e exibir o número da versão do software e hardware do dispositivo diretamente
     * No entanto, lembre-se de que o hardware e o software de atualização do dispositivo atualizarão o número da versão, portanto, toda vez que você se conectar a um dispositivo Bluetooth, deverá ler o número da versão do software e do hardware.
     * Se você fizer o salvamento local e atualizar o salvamento local a tempo, você pode garantir que o número da versão esteja atualizado sob quaisquer circunstâncias.
     **************************************************************/

    @Override
    public void onBLENoSupported() {
        Toast.makeText(getContext(), "Bluetooth não suporta", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOpenBLE() {
        startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), REQUEST_OPEN_BT);
    }

    @Override
    public void onBleState(int bleState) {
        switch (bleState) {
            case BluetoothState.BLE_CLOSED:
                btnText.set("Ligue o Bluetooth");
                reset();
                isDevBind.set(0);
                break;
            case BluetoothState.BLE_OPENED_AND_DISCONNECT:
                btnText.set("Clique para conectar o dispositivo");
                reset();
                isDevBind.set(0);
                break;
            case BluetoothState.BLE_CONNECTING_DEVICE:
                btnText.set("Em conexão...");
                break;
            case BluetoothState.BLE_CONNECTED_DEVICE:
                btnText.set("CONECTADO, clique para desconectar.");
                break;
        }
    }

    @Override
    public void onUpdateDialogBleList() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mBleDeviceListDialogFragment != null && mBleDeviceListDialogFragment.isShowing()) {
                    mBleDeviceListDialogFragment.refresh();
                }
            }
        });
    }

    /*
    * O dispositivo está conectado ao cabo de carregamento USB e não está totalmente carregado.
     *
    *
    * */
    @Override
    public void onBatteryCharging() {
        power.set("Carregando ...");
        /*if (mActivity != null) {
            mActivity.setBatery(power.get(),true);
        }*/
    }

    /*
    * Desconecte o cabo de carregamento USB e use-o normalmente.
     * */
    @Override
    public void onBatteryQuery(int batteryValue) {
        power.set(batteryValue + "%");

        if (mActivity != null) {
            mActivity.setBatery(power.get(),true);

            mActivity.getUserLocationMonitorInfo(batteryValue);
        }
    }

    /*
    * O dispositivo está conectado ao cabo de carregamento USB e está totalmente carregado.
     * */
    @Override
    public void onBatteryFull() {
        power.set("Cheio de");
        if (mActivity != null) {
            mActivity.setBatery("100",true);
        }
    }


    /**
     * Clique para mudar o status da conexão Bluetooth
     */
    public void clickConnect(View v) {
        if (App.isUseCustomBleDevService) {
            final boolean isObtain = PermissionManager.isObtain(this, PermissionManager.PERMISSION_LOCATION, PermissionManager.requestCode_location);
            if (!isObtain) {
                return;
            }
            else {
                if (!PermissionManager.canScanBluetoothDevice(getContext())) {
                    new AlertDialogBuilder(mActivity)
                            .setTitle("Prompt")
                            .setMessage("O Android 6.0 e superior requer o GPS para ativar dispositivos Bluetooth.")
                            .setNegativeButton(android.R.string.cancel, null)
                            .setPositiveButton("Ligue o GPS", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    PermissionManager.openGPS(mActivity);
                                }
                            }).create().show();
                    return;
                }
            }
            if (mHcService.isConnected) {
                mHcService.disConnect();
            }
            else {
                final int bluetoothEnable = mHcService.isBluetoothEnable();
                if (bluetoothEnable == -1) {
                    onBLENoSupported();
                } else if (bluetoothEnable == 0) {
                    onOpenBLE();
                } else {
                    mHcService.quicklyConnect();
                }
            }
        }
        else
        {
            final int bleState = MonitorDataTransmissionManager.getInstance().getBleState();
            Log.e("clickConnect", "bleState:" + bleState);
            switch (bleState) {
                case BluetoothState.BLE_CLOSED:
                    MonitorDataTransmissionManager.getInstance().bleCheckOpen();
                    break;
                case BluetoothState.BLE_OPENED_AND_DISCONNECT:
                    if (MonitorDataTransmissionManager.getInstance().isScanning()) {
                        new AlertDialogBuilder(mActivity)
                                .setTitle("Prompt")
                                .setMessage("Dispositivo de digitalização, por favor aguarde ...")
                                .setNegativeButton(android.R.string.cancel, null)
                                .setPositiveButton("Parar de escanear", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        MonitorDataTransmissionManager.getInstance().scan(false);
                                    }
                                }).create().show();
                    } else {
                        final boolean isObtain = PermissionManager.isObtain(this, PermissionManager.PERMISSION_LOCATION, PermissionManager.requestCode_location);
                        if (isObtain) {
                            if (PermissionManager.canScanBluetoothDevice(getContext())) {
                                if (showScanList) {
                                    connectByDeviceList();
                                }
                                else {
                                    MonitorDataTransmissionManager.getInstance().scan(true);
                                }
                            } else {
                                new AlertDialogBuilder(mActivity)
                                        .setTitle("Prompt")
                                        .setMessage("Android 6.0E acima, o sistema precisa ligar o GPS para escanear o dispositivo Bluetooth.")
                                        .setNegativeButton(android.R.string.cancel, null)
                                        .setPositiveButton("Ligue o GPS", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                PermissionManager.openGPS(mActivity);
                                            }
                                        }).create().show();
                            }
                        }
                    }
                    break;
                case BluetoothState.BLE_CONNECTING_DEVICE:
                    Toast.makeText(mActivity, "Conexão Bluetooth ...", Toast.LENGTH_SHORT).show();
                    break;
                case BluetoothState.BLE_CONNECTED_DEVICE:
                case BluetoothState.BLE_NOTIFICATION_DISABLED:
                case BluetoothState.BLE_NOTIFICATION_ENABLED:
                    MonitorDataTransmissionManager.getInstance().disConnectBle();
                    break;
            }
        }

    }


    public void clickShowDownloadData(View v) {

    }

    public void clickGetFamilyMember(View v) {
        // ID de família única para obter membros da família
    }

    /*
    * Obter uma casa com um único dispositivo ID * Selecione uma conexão de dispositivo da lista de dispositivos
    * (para vários dispositivos Bluetooth do mesmo modelo no ambiente circundante, evitando erros)
    * */
    private void connectByDeviceList() {
        mBleDeviceListDialogFragment = new BleDeviceListDialogFragment();
        mBleDeviceListDialogFragment.show(mActivity.getSupportFragmentManager(), "");
    }

    /**
     * Solicitar um dispositivo de interesse da conta principal
     *Se a solicitação for bem-sucedida, o servidor enviará um código de verificação de mensagem curta ao número
     * de telefone celular correspondente à conta principal, o que é um processo demorado.
     */
    private void requestPrimaryAcc() {

    }

    @Override
    public void onDeviceInfo(DeviceInfo device) {
        Log.e("onDeviceInfo", device.toString());
        String deviceId = device.getDeviceId();
        String deviceKey = device.getDeviceKey();
        // Se você precisar de letras minúsculas nos parâmetros id e key, você pode converter da seguinte maneira
        deviceId = deviceId.toLowerCase();
        deviceKey = deviceKey.toLowerCase();
        id.set(deviceId);
        key.set(deviceKey);
        // Store data
        LocalPrefs.storeValue(mActivity,deviceId,mActivity.device_id);
        LocalPrefs.storeValue(mActivity,deviceKey,mActivity.device_key);

        if (mHcService != null) {
            mHcService.dataQuery(HcService.DATA_QUERY_BATTERY_INFO);
        }
    }

    @Override
    public void onReadDeviceInfoFailed() {
        id.set("Não é possível obter o ID do dispositivo");
        key.set("Não é possível obter a chave do dispositivo");
        if (mHcService != null) {
            mHcService.dataQuery(HcService.DATA_QUERY_BATTERY_INFO);
        }
    }
}
