package br.com.bioz4life.biomonitor.carenet;

import java.util.concurrent.TimeUnit;

import br.com.bioz4life.biomonitor.carenet.model.CarenetSender;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitCarenet {

    private static Retrofit retrofit = null;
    //private String baseUrl = "https://ezvision.crnt.com.br/octopus/"
    private String baseUrl = "https://clinicalcare.crnt.com.br/octopus/";
    private Retrofit getClient(){

        if (retrofit == null) {

            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new BasicAuthInterceptor("administrador", "genius@2019"))
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    public void send(CarenetSender request, final OnResponseListener listener) {

        ICarenet carenet = new RetrofitCarenet().getClient().create(ICarenet.class);

        Call<ResponseBody> call = carenet.add(request);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    listener.onResponse(true);
                } else {
                    listener.onResponse(false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                listener.onResponse(false);
            }
        });
    }


    public interface OnResponseListener {
        void onResponse(Boolean success);
    }
}


