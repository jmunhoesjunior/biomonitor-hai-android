package br.com.bioz4life.biomonitor.carenet.model;

import java.io.Serializable;

public class Resultado implements Serializable {
    public String valor;
    public String typeResult;
    public String units;
    public String initials;
}
