package br.com.bioz4life.biomonitor.fragment;

import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.renderer.XAxisRenderer;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;
import com.github.mikephil.charting.utils.Utils;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.activity.CompleteDetails;
import br.com.bioz4life.biomonitor.activity.CompleteListECG;
import br.com.bioz4life.biomonitor.activity.ECGLargeActivity;
import br.com.bioz4life.biomonitor.adapter.AdapterECG;
import br.com.bioz4life.biomonitor.bean.Bg;
import br.com.bioz4life.biomonitor.bean.Bp;
import br.com.bioz4life.biomonitor.bean.Bt;
import br.com.bioz4life.biomonitor.bean.ECG;
import br.com.bioz4life.biomonitor.bean.SPO2H;
import br.com.bioz4life.biomonitor.database.RealmUtilityBg;
import br.com.bioz4life.biomonitor.database.RealmUtilityBp;
import br.com.bioz4life.biomonitor.database.RealmUtilityBt;
import br.com.bioz4life.biomonitor.database.RealmUtilityECG;
import br.com.bioz4life.biomonitor.database.RealmUtilitySPO2;
import br.com.bioz4life.biomonitor.databinding.FragmentHistoryBinding;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import butterknife.BindView;
import butterknife.OnClick;

public class HistoryFragment extends BaseFragment implements AdapterECG.OnClickECG {

    @BindView(R.id.chartBp)
    LineChart chartBp;

    @BindView(R.id.chartHr)
    LineChart chartHr;

    @BindView(R.id.chartBt)
    LineChart chartBt;

    @BindView(R.id.chartSPO2H)
    LineChart chartSPO2H;

    @BindView(R.id.chartHrSPO2H)
    LineChart chartHrSPO2H;

    @BindView(R.id.chartBg)
    LineChart chartBg;

    @BindView(R.id.rvListECG)
    RecyclerView rvListECG;

    AdapterECG adapterECG;

    //@BindView(R.id.cardBp)
    //CardView cardBp;

    @BindView(R.id.tvEmptyECG)
    TextView tvEmptyECG;

    @BindView(R.id.relativeEmpty)
    RelativeLayout relativeEmpty;

    @Override
    public String getTitle() {
        return "Histórico";
    }

    @Override
    public void reset() {

    }

    @Override
    protected ViewDataBinding onCreateBindingView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentHistoryBinding binding = setBindingContentView(inflater, R.layout.fragment_history, container);

        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rvListECG.setLayoutManager(new LinearLayoutManager(mActivity));
        rvListECG.setNestedScrollingEnabled(false);
        rvListECG.setHasFixedSize(false);

        loadData();
    }

    @Override
    public void onClickECGItem(int position, ECG ecg) {
        Intent intent = new Intent(mActivity, ECGLargeActivity.class);
        int pagerSpeed = 0;
        float gain = 0.0f;

        if(ecg.getGain() == 5){
            gain  = 0.5f;
        }
        else if(ecg.getGain() == 10){
            gain  = 1;
        }
        else{
            gain  = 2;
        }

        if(ecg.getPagerSpeed() == 25){
            pagerSpeed  = 1;
        }
        else{
            pagerSpeed  = 2;
        }
        intent.putExtra("pagerSpeed", pagerSpeed);
        intent.putExtra("gain", gain);
        intent.putExtra("model", ecg);
        startActivity(intent);
    }

    public void loadData(){
        //mActivity.showProgressDialog(getResources().getString(R.string.loading_data));
        showGraphBp();

        //Body Temperature
        showBt();

        //Oxymetry
        showSPO2H();

        //Blood glic.
        showBg();

        //ECG
        showECG();
    }

    public void showGraphBp(){
        Description description = new Description();
        //description.setText("Sample text Title");
        description.setText("");
        chartBp.setDescription(description);
        chartBp.setNoDataText("Nenhum dado encontrado.");
        chartBp.setTouchEnabled(false);
        chartBp.setScaleEnabled(false);
        chartBp.setScaleXEnabled(false);
        chartBp.setScaleYEnabled(false);
        chartBp.setPinchZoom(false);
        chartBp.setXAxisRenderer(new CustomXAxisRenderer(chartBp.getViewPortHandler(), chartBp.getXAxis(), chartBp.getTransformer(YAxis.AxisDependency.LEFT)));
        chartBp.setExtraOffsets(0,0,25,12);
        List<Bp>mItemsBp = RealmUtilityBp.getLastSamplesLimit5(mActivity.getRealm(), LocalPrefs.getPhoneNumber(mActivity));
        if (mItemsBp.size() > 0) {
            ArrayList<Entry> entries = new ArrayList<>();
            ArrayList<Entry> entries2 = new ArrayList<>();
            final ArrayList<String> createdDatesXaxis = new ArrayList<>();

            for(int i = 0;i<mItemsBp.size();i++){
                entries.add(new Entry(i, mItemsBp.get(i).getSbp()));
                entries2.add(new Entry(i, mItemsBp.get(i).getDbp()));
                DateFormat dateFormat = new SimpleDateFormat("dd/MM\nHH:mm");
                //to convert Date to String, use format method of SimpleDateFormat class.
                String strDate = dateFormat.format(mItemsBp.get(i).getCreatedDate());
                createdDatesXaxis.add(strDate);
            }

            List<ILineDataSet> datasets = new ArrayList<>();

            LineDataSet dataSet1 = new LineDataSet(entries, "SBP");
            dataSet1.setValueTextSize(14);
            dataSet1.setColor(ContextCompat.getColor(mActivity, R.color.yellow));
            dataSet1.setValueTextColor(ContextCompat.getColor(mActivity, R.color.yellow));
            dataSet1.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return String.format("%.0f",value) ;
                }
            });
            datasets.add(dataSet1);

            LineDataSet dataSet2 = new LineDataSet(entries2, "DBP");
            dataSet2.setValueTextSize(14);
            dataSet2.setColor(ContextCompat.getColor(mActivity, R.color.black));
            dataSet2.setValueTextColor(ContextCompat.getColor(mActivity, R.color.black));
            dataSet2.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return String.format("%.0f",value) ;
                }
            });
            datasets.add(dataSet2);

            //****
            // Controlling X axis
            XAxis xAxis = chartBp.getXAxis();
            xAxis.setTextSize(14);
            // Set the xAxis position to bottom. Default is top
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            //Customizing x axis value
            //final String[] createdDatesXaxis2 = new String[]{"25/09 10:01"};
            IAxisValueFormatter formatter = new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {

                    if(value >=0 && value < createdDatesXaxis.size())
                        return createdDatesXaxis.get((int)value);
                    return "";
                }
            };
            xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
            xAxis.setValueFormatter(formatter);

            //***
            // Controlling right side of y axis
            YAxis yAxisRight = chartBp.getAxisRight();
            yAxisRight.setTextSize(14);
            yAxisRight.setEnabled(false);

            //***
            // Controlling left side of y axis
            YAxis yAxisLeft = chartBp.getAxisLeft();
            yAxisLeft.setTextSize(14);
            yAxisLeft.setLabelCount(5,true);
            yAxisLeft.setXOffset(10);
            yAxisLeft.setGranularity(1f);

            // Draw Line Top limit
            LimitLine ll1 = new LimitLine(140, "");//Upper Limit
            ll1.setLineWidth(4f);
            ll1.setLineColor(ContextCompat.getColor(mActivity, R.color.light_gray));
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);
            yAxisLeft.addLimitLine(ll1);

            // Draw Line Bottom limit
            LimitLine ll2 = new LimitLine(100, "");//Bottom Limit
            ll2.setLineWidth(4f);
            ll2.setLineColor(ContextCompat.getColor(mActivity, R.color.light_gray));
            ll2.enableDashedLine(10f, 10f, 0f);
            ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll2.setTextSize(10f);
            yAxisLeft.addLimitLine(ll2);

            // Setting Data
            LineData data = new LineData(datasets);
            chartBp.setData(data);
            chartBp.animateX(2500);
            //refresh
            chartBp.invalidate();

            // Heart Rate - Blood Presure
            showHrBp(mItemsBp,createdDatesXaxis);
        }
    }

    public void showHrBp(List<Bp> mItemsBp, final ArrayList<String> createdDatesXaxis){
        Description description = new Description();
        //description.setText("Sample text Title");
        description.setText("");
        chartHr.setDescription(description);
        chartHr.setNoDataText("Nenhum dado encontrado.");
        chartHr.setTouchEnabled(false);
        chartHr.setScaleEnabled(false);
        chartHr.setScaleXEnabled(false);
        chartHr.setScaleYEnabled(false);
        chartHr.setPinchZoom(false);
        chartHr.setXAxisRenderer(new CustomXAxisRenderer(chartHr.getViewPortHandler(), chartHr.getXAxis(), chartHr.getTransformer(YAxis.AxisDependency.LEFT)));
        chartHr.setExtraOffsets(0,0,25,12);
        ArrayList<Entry> entries = new ArrayList<>();
        for(int i = 0;i<mItemsBp.size();i++){
            entries.add(new Entry(i, mItemsBp.get(i).getHr()));
        }

        List<ILineDataSet> datasets = new ArrayList<>();

        LineDataSet dataSet1 = new LineDataSet(entries, "BPM");
        dataSet1.setValueTextSize(14);
        dataSet1.setColor(ContextCompat.getColor(mActivity, R.color.black));
        dataSet1.setValueTextColor(ContextCompat.getColor(mActivity, R.color.black));
        dataSet1.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return String.format("%.0f",value) ;
            }
        });
        datasets.add(dataSet1);

        //****
        // Controlling X axis
        XAxis xAxis = chartHr.getXAxis();
        xAxis.setTextSize(14);
        // Set the xAxis position to bottom. Default is top
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //Customizing x axis value
        //final String[] months = new String[]{"25/09 10:01", "25/09 10:02", "25/09 10:10", "25/09 11:00","25/09 11:07"};

        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if(value >=0 && value < createdDatesXaxis.size())
                    return createdDatesXaxis.get((int)value);
                return "";
                //return months[(int) value];
            }
        };
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);

        //***
        // Controlling right side of y axis
        YAxis yAxisRight = chartHr.getAxisRight();
        yAxisRight.setTextSize(14);
        yAxisRight.setEnabled(false);

        //***
        // Controlling left side of y axis
        YAxis yAxisLeft = chartHr.getAxisLeft();
        yAxisLeft.setTextSize(14);
        yAxisLeft.setLabelCount(5,true);
        yAxisLeft.setXOffset(10);
        yAxisLeft.setGranularity(1f);


        // Setting Data
        LineData data = new LineData(datasets);
        chartHr.setData(data);
        chartHr.animateX(2500);
        //refresh
        chartHr.invalidate();
    }

    public void showBt(){
        Description description = new Description();
        //description.setText("Sample text Title");
        description.setText("");
        chartBt.setDescription(description);
        chartBt.setNoDataText("Nenhum dado encontrado.");
        chartBt.setTouchEnabled(false);
        chartBt.setScaleEnabled(false);
        chartBt.setScaleXEnabled(false);
        chartBt.setScaleYEnabled(false);
        chartBt.setPinchZoom(false);
        chartBt.setXAxisRenderer(new CustomXAxisRenderer(chartBt.getViewPortHandler(), chartBt.getXAxis(), chartBt.getTransformer(YAxis.AxisDependency.LEFT)));
        chartBt.setExtraOffsets(0,0,25,12);
        List<Bt>mItemsBt = RealmUtilityBt.getLastSamplesLimit5(mActivity.getRealm(), LocalPrefs.getPhoneNumber(mActivity));
        if (mItemsBt.size() > 0) {
            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<String> createdDatesXaxis = new ArrayList<>();
            for(int i = 0;i<mItemsBt.size();i++){
                entries.add(new Entry(i, Double.valueOf(mItemsBt.get(i).getTemp()).floatValue()));
                DateFormat dateFormat = new SimpleDateFormat("dd/MM\nHH:mm");

                //to convert Date to String, use format method of SimpleDateFormat class.
                String strDate = dateFormat.format(mItemsBt.get(i).getCreatedDate());
                createdDatesXaxis.add(strDate);
            }
            /*entries.add(new Entry(0, 37));
            entries.add(new Entry(1, 36));
            entries.add(new Entry(2, 32));
            entries.add(new Entry(3, 33));
            entries.add(new Entry(4, 35));*/

            List<ILineDataSet> datasets = new ArrayList<>();

            LineDataSet dataSet1 = new LineDataSet(entries, "ºC");
            dataSet1.setValueTextSize(14);
            dataSet1.setColor(ContextCompat.getColor(mActivity, R.color.yellow));
            dataSet1.setValueTextColor(ContextCompat.getColor(mActivity, R.color.yellow));
            dataSet1.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return String.format("%.1f",value) ;
                }
            });
            datasets.add(dataSet1);

            //****
            // Controlling X axis
            XAxis xAxis = chartBt.getXAxis();
            xAxis.setTextSize(14);
            // Set the xAxis position to bottom. Default is top
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            //Customizing x axis value
            //final String[] months = new String[]{"25/09 10:01", "25/09 10:02", "25/09 10:10", "25/09 11:00","25/09 11:07"};

            IAxisValueFormatter formatter = new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    if(value >=0 && value < createdDatesXaxis.size()) {
                        return createdDatesXaxis.get((int) value);
                    }
                    return "";
                }
            };
            xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
            xAxis.setValueFormatter(formatter);

            //***
            // Controlling right side of y axis
            YAxis yAxisRight = chartBt.getAxisRight();
            yAxisRight.setTextSize(14);
            yAxisRight.setEnabled(false);

            //***
            // Controlling left side of y axis
            YAxis yAxisLeft = chartBt.getAxisLeft();
            yAxisLeft.setTextSize(14);
            yAxisLeft.setLabelCount(5,true);
            yAxisLeft.setXOffset(10);
            yAxisLeft.setGranularity(1f);

            // Draw Line Top limit
            LimitLine ll1 = new LimitLine(38, "");//Upper Limit
            ll1.setLineWidth(4f);
            ll1.setLineColor(ContextCompat.getColor(mActivity, R.color.light_gray));
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);
            yAxisLeft.addLimitLine(ll1);


            // Setting Data
            LineData data = new LineData(datasets);
            chartBt.setData(data);
            chartBt.animateX(2500);
            //refresh
            chartBt.invalidate();
        }
    }

    public void showSPO2H(){
        Description description = new Description();
        //description.setText("Sample text Title");
        description.setText("");
        chartSPO2H.setDescription(description);
        chartSPO2H.setNoDataText("Nenhum dado encontrado.");
        chartSPO2H.setTouchEnabled(false);
        chartSPO2H.setScaleEnabled(false);
        chartSPO2H.setScaleXEnabled(false);
        chartSPO2H.setScaleYEnabled(false);
        chartSPO2H.setPinchZoom(false);
        chartSPO2H.setXAxisRenderer(new CustomXAxisRenderer(chartSPO2H.getViewPortHandler(), chartSPO2H.getXAxis(), chartSPO2H.getTransformer(YAxis.AxisDependency.LEFT)));
        chartSPO2H.setExtraOffsets(0,0,25,12);

        List<SPO2H>mItemsSPO2 = RealmUtilitySPO2.getLastSamplesLimit5(mActivity.getRealm(), LocalPrefs.getPhoneNumber(mActivity));
        if(mItemsSPO2.size()> 0){
            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<String> createdDatesXaxis = new ArrayList<>();
            for(int i = 0;i<mItemsSPO2.size();i++){
                entries.add(new Entry(i, mItemsSPO2.get(i).getValue()));
                DateFormat dateFormat = new SimpleDateFormat("dd/MM\nHH:mm");

                //to convert Date to String, use format method of SimpleDateFormat class.
                String strDate = dateFormat.format(mItemsSPO2.get(i).getCreatedDate());
                createdDatesXaxis.add(strDate);
            }
            /*entries.add(new Entry(0, 99));
            entries.add(new Entry(1, 96));
            entries.add(new Entry(2, 95));
            entries.add(new Entry(3, 93));
            entries.add(new Entry(4, 97));*/

            List<ILineDataSet> datasets = new ArrayList<>();

            LineDataSet dataSet1 = new LineDataSet(entries, "SpO₂ %");
            dataSet1.setColor(ContextCompat.getColor(mActivity, R.color.yellow));
            dataSet1.setValueTextSize(14);
            dataSet1.setValueTextColor(ContextCompat.getColor(mActivity, R.color.yellow));
            dataSet1.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    return String.format("%.0f",value) ;
                }
            });
            datasets.add(dataSet1);

            //****
            // Controlling X axis
            XAxis xAxis = chartSPO2H.getXAxis();
            xAxis.setTextSize(14);
            // Set the xAxis position to bottom. Default is top
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            //Customizing x axis value
            //final String[] months = new String[]{"25/09 10:01", "25/09 10:02", "25/09 10:10", "25/09 11:00","25/09 11:07"};

            IAxisValueFormatter formatter = new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    if(value >=0 && value < createdDatesXaxis.size())
                        return createdDatesXaxis.get((int)value);
                    return "";
                }
            };

            xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
            xAxis.setValueFormatter(formatter);

            //***
            // Controlling right side of y axis
            YAxis yAxisRight = chartSPO2H.getAxisRight();
            yAxisRight.setTextSize(14);
            yAxisRight.setEnabled(false);

            //***
            // Controlling left side of y axis
            YAxis yAxisLeft = chartSPO2H.getAxisLeft();
            yAxisLeft.setTextSize(14);
            yAxisLeft.setLabelCount(5,false);
            yAxisLeft.setXOffset(10);
            yAxisLeft.setGranularity(1f);

            // Draw Line Top limit
            LimitLine ll1 = new LimitLine(95, "");//Upper Limit
            ll1.setLineWidth(4f);
            ll1.setLineColor(ContextCompat.getColor(mActivity, R.color.light_gray));
            ll1.enableDashedLine(10f, 10f, 0f);
            ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
            ll1.setTextSize(10f);
            yAxisLeft.addLimitLine(ll1);


            // Setting Data
            LineData data = new LineData(datasets);
            chartSPO2H.setData(data);
            chartSPO2H.animateX(2500);
            //refresh
            chartSPO2H.invalidate();

            showHrSPO2H(mItemsSPO2,createdDatesXaxis);
        }
    }

    public void showHrSPO2H(List<SPO2H> mItemsSPO2, final ArrayList<String> createdDatesXaxis){
        Description description = new Description();
        //description.setText("Sample text Title");
        description.setText("");
        chartHrSPO2H.setDescription(description);
        chartHrSPO2H.setNoDataText("Nenhum dado encontrado.");
        chartHrSPO2H.setTouchEnabled(false);
        chartHrSPO2H.setScaleEnabled(false);
        chartHrSPO2H.setScaleXEnabled(false);
        chartHrSPO2H.setScaleYEnabled(false);
        chartHrSPO2H.setPinchZoom(false);
        chartHrSPO2H.setXAxisRenderer(new CustomXAxisRenderer(chartHrSPO2H.getViewPortHandler(), chartHrSPO2H.getXAxis(), chartHrSPO2H.getTransformer(YAxis.AxisDependency.LEFT)));
        chartHrSPO2H.setExtraOffsets(0,0,25,12);
        ArrayList<Entry> entries = new ArrayList<>();
        for(int i = 0;i<mItemsSPO2.size();i++){
            entries.add(new Entry(i, mItemsSPO2.get(i).getHr()));
        }

        List<ILineDataSet> datasets = new ArrayList<>();

        LineDataSet dataSet1 = new LineDataSet(entries, "BPM");
        dataSet1.setValueTextSize(14);
        dataSet1.setColor(ContextCompat.getColor(mActivity, R.color.black));
        dataSet1.setValueTextColor(ContextCompat.getColor(mActivity, R.color.black));
        dataSet1.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return String.format("%.0f",value) ;
            }
        });
        datasets.add(dataSet1);

        //****
        // Controlling X axis
        XAxis xAxis = chartHrSPO2H.getXAxis();
        xAxis.setTextSize(14);
        // Set the xAxis position to bottom. Default is top
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //Customizing x axis value
        //final String[] months = new String[]{"25/09 10:01", "25/09 10:02", "25/09 10:10", "25/09 11:00","25/09 11:07"};

        IAxisValueFormatter formatter = new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if(value >=0 && value < createdDatesXaxis.size())
                    return createdDatesXaxis.get((int)value);
                return "";
            }
        };
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);

        //***
        // Controlling right side of y axis
        YAxis yAxisRight = chartHrSPO2H.getAxisRight();
        yAxisRight.setTextSize(14);
        yAxisRight.setEnabled(false);

        //***
        // Controlling left side of y axis
        YAxis yAxisLeft = chartHrSPO2H.getAxisLeft();
        yAxisLeft.setTextSize(14);
        yAxisLeft.setLabelCount(5,true);
        yAxisLeft.setXOffset(10);
        yAxisLeft.setGranularity(1f);


        // Setting Data
        LineData data = new LineData(datasets);
        chartHrSPO2H.setData(data);
        chartHrSPO2H.animateX(2500);
        //refresh
        chartHrSPO2H.invalidate();
    }

    public void showBg(){
        Description description = new Description();
        //description.setText("Sample text Title");
        description.setText("");
        chartBg.setDescription(description);
        chartBg.setNoDataText("Nenhum dado encontrado.");
        chartBg.setTouchEnabled(false);
        chartBg.setScaleEnabled(false);
        chartBg.setScaleXEnabled(false);
        chartBg.setScaleYEnabled(false);
        chartBg.setPinchZoom(false);
        chartBg.setXAxisRenderer(new CustomXAxisRenderer(chartBg.getViewPortHandler(), chartBg.getXAxis(), chartBg.getTransformer(YAxis.AxisDependency.LEFT)));
        chartBg.setExtraOffsets(0,0,25,12);
        List<Bg>mItemsBg = RealmUtilityBg.getLastSamplesLimit5(mActivity.getRealm(), LocalPrefs.getPhoneNumber(mActivity));

        if(mItemsBg.size() > 0){
            ArrayList<Entry> entries = new ArrayList<>();
            final ArrayList<String> createdDatesXaxis = new ArrayList<>();
            for(int i = 0;i<mItemsBg.size();i++){
                entries.add(new Entry(i, Double.valueOf(mItemsBg.get(i).getValue()).floatValue()));
                DateFormat dateFormat = new SimpleDateFormat("dd/MM\nHH:mm");

                //to convert Date to String, use format method of SimpleDateFormat class.
                String strDate = dateFormat.format(mItemsBg.get(i).getCreatedDate());
                createdDatesXaxis.add(strDate);
            }

            List<ILineDataSet> datasets = new ArrayList<>();

            LineDataSet dataSet1 = new LineDataSet(entries, "mg/dL");
            dataSet1.setValueTextSize(14);
            dataSet1.setColor(ContextCompat.getColor(mActivity, R.color.black));
            dataSet1.setValueTextColor(ContextCompat.getColor(mActivity, R.color.black));
            dataSet1.setValueFormatter(new IValueFormatter() {
                @Override
                public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                    double mgdl = 18.018 * value;
                    int mgdlInt = (int)Math.round(mgdl);
                    return String.valueOf(mgdlInt);
                }
            });
            datasets.add(dataSet1);

            //****
            // Controlling X axis
            XAxis xAxis = chartBg.getXAxis();
            xAxis.setTextSize(14);
            // Set the xAxis position to bottom. Default is top
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            //Customizing x axis value
            //final String[] months = new String[]{"25/09 10:01", "25/09 10:02", "25/09 10:10", "25/09 11:00","25/09 11:07"};

            IAxisValueFormatter formatter = new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    if(value >= 0 && value < createdDatesXaxis.size())
                        return createdDatesXaxis.get((int)value);
                    return "";
                }
            };
            xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
            xAxis.setValueFormatter(formatter);

            //***
            // Controlling right side of y axis
            YAxis yAxisRight = chartBg.getAxisRight();
            yAxisRight.setTextSize(14);
            yAxisRight.setEnabled(false);

            //***
            // Controlling left side of y axis
            YAxis yAxisLeft = chartBg.getAxisLeft();
            yAxisLeft.setTextSize(14);
            yAxisLeft.setLabelCount(5,true);
            yAxisLeft.setXOffset(10);
            yAxisLeft.setGranularity(1f);


            // Setting Data
            LineData data = new LineData(datasets);
            chartBg.setData(data);
            chartBg.animateX(2500);
            //refresh
            chartBg.invalidate();
        }
    }

    public void showECG(){

        List<ECG> listECG = RealmUtilityECG.getLastSamplesLimit5(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));
        if (listECG.size() > 0) {
            tvEmptyECG.setVisibility(View.GONE);
            relativeEmpty.setVisibility(View.GONE);
        }
        else{
            relativeEmpty.setVisibility(View.VISIBLE);
            tvEmptyECG.setVisibility(View.VISIBLE);
        }
        adapterECG = new AdapterECG(mActivity,listECG,this);

        rvListECG.setAdapter(adapterECG);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.cardBp)
    public void onClickBp(){
        Intent intent = new Intent(mActivity, CompleteDetails.class);
        intent.putExtra("type","bp");
        startActivity(intent);
    }
    @OnClick(R.id.cardHrBp)
    public void onClickHRBp(){
        Intent intent = new Intent(mActivity, CompleteDetails.class);
        intent.putExtra("type","bp_hr");
        startActivity(intent);
    }
    @OnClick(R.id.cardBt)
    public void onClickCardBt(){
        Intent intent = new Intent(mActivity, CompleteDetails.class);
        intent.putExtra("type","bt");
        startActivity(intent);
    }
    @OnClick(R.id.cardSpo2)
    public void onClickSpo2(){
        Intent intent = new Intent(mActivity, CompleteDetails.class);
        intent.putExtra("type","spo2");
        startActivity(intent);
    }

    @OnClick(R.id.cardSpo2Hr)
    public void onClickSpo2Hr(){
        Intent intent = new Intent(mActivity, CompleteDetails.class);
        intent.putExtra("type","spo2_hr");
        startActivity(intent);
    }

    @OnClick(R.id.cardBg)
    public void onClickBg(){
        Intent intent = new Intent(mActivity, CompleteDetails.class);
        intent.putExtra("type","bg");
        startActivity(intent);
    }

    @OnClick(R.id.btListAllECG)
    public void onClickbtListAllECG(){
        Intent intent = new Intent(mActivity, CompleteListECG.class);
        startActivity(intent);
    }

    public class CustomXAxisRenderer extends XAxisRenderer {
        public CustomXAxisRenderer(ViewPortHandler viewPortHandler, XAxis xAxis, Transformer trans) {
            super(viewPortHandler, xAxis, trans);
        }

        @Override
        protected void drawLabel(Canvas c, String formattedLabel, float x, float y, MPPointF anchor, float angleDegrees) {
            if (formattedLabel != null) {
                if(!formattedLabel.equals("")){
                    String line[] = formattedLabel.split("\n");
                    Utils.drawXAxisValue(c, line[0], x, y, mAxisLabelPaint, anchor, angleDegrees);
                    Utils.drawXAxisValue(c, line[1], x, y + mAxisLabelPaint.getTextSize(), mAxisLabelPaint, anchor, angleDegrees);
                }
            }
        }
    }
}
