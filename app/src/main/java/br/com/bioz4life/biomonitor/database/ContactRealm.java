package br.com.bioz4life.biomonitor.database;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ContactRealm extends RealmObject {

    @PrimaryKey
    public String id;
    // CountryCode+DDD+Phone
    public String user;

    public String phone;
    public String fullname;
    public String relationship;
    public String email;
    public boolean isPhysician;
    public boolean isNotify;
    public boolean isFavorite;
    public String cpf;
    public Date createdDate;// so serve pra ordenar a criacao dos contatos nao sincronizados

    public boolean isSync;
}
