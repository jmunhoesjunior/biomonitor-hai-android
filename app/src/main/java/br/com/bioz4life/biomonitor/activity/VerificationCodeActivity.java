package br.com.bioz4life.biomonitor.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.database.UserAccountRealm;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.UToast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class VerificationCodeActivity extends BaseActivity {

    @BindView(R.id.etSMS)
    TextInputEditText etSMS;

    private String verificationId;
    private String phone;
    Realm realm;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_verification_code);

        ButterKnife.bind(this);

        setUpActionBar("Verificação", getString(R.string.app_name), false);

        realm = Realm.getDefaultInstance();

        verificationId = getIntent().getStringExtra("verificationId");
        phone = getIntent().getStringExtra("phone");
    }

    @OnClick(R.id.btSend)
    public void onClickSend(){
        showProgressDialog(getResources().getString(R.string.loading_data));

        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, etSMS.getText().toString());

        FirebaseAuth.getInstance().signInWithCredential(credential).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {

                // Store number
                LocalPrefs.storePhoneNumber(VerificationCodeActivity.this, phone);

                // firestore document get by number
                FirebaseFirestore db = FirebaseFirestore.getInstance();
                DocumentReference refUser = db.collection("biomonitor").document(phone);
                refUser.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                // update document at user_info.regcode
                                //Log.d(TAG, "DocumentSnapshot data: " + document.getData());

                                Map<String, Object> map = document.getData();
                                Map<String, Object> items =  (Map<String, Object>) map.get("user_info");

                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        UserAccountRealm userAccountRealm = new UserAccountRealm();
                                        userAccountRealm.id = phone;
                                        userAccountRealm.Nickname = (String) items.get("nickname");
                                        userAccountRealm.Birthday = (String) items.get("birthday");
                                        userAccountRealm.Cpf = (String) items.get("cpf");
                                        userAccountRealm.Email = (String) items.get("email");
                                        userAccountRealm.FullAddress = (String) items.get("fulladdress");
                                        userAccountRealm.FullName = (String) items.get("fullname");
                                        userAccountRealm.Gender = (String) items.get("gender");
                                        userAccountRealm.Photo = (String) items.get("photo");
                                        userAccountRealm.RegCode = (String) items.get(etSMS.getText().toString());
                                        realm.copyToRealmOrUpdate(userAccountRealm);
                                    }
                                });

                                refUser.update("user_info.regcode",etSMS.getText().toString()).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        dismissProgressDialog();

                                        Intent intent = new Intent(getBaseContext(), HealthMonitorActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        dismissProgressDialog();
                                        UToast.show(VerificationCodeActivity.this,e.getMessage());
                                    }
                                });
                            }
                            else {
                               // Log.d(TAG, "Document does not exist, lets create one.");
                                Map<String, Object> data = new HashMap<>();

                                Map<String, Object> infos = new HashMap<>();
                                infos.put("birthday","");
                                infos.put("cpf","");
                                infos.put("email","");
                                infos.put("fulladdress","");
                                infos.put("fullname","");
                                infos.put("gender","");
                                infos.put("nickname","");
                                infos.put("phone",LocalPrefs.getPhoneNumber(VerificationCodeActivity.this));
                                infos.put("photo","");
                                infos.put("regcode",etSMS.getText().toString());

                                data.put("user_info", infos);

                                refUser.set(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        dismissProgressDialog();

                                        Intent intent = new Intent(getBaseContext(), HealthMonitorActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        dismissProgressDialog();
                                        UToast.show(VerificationCodeActivity.this,e.getMessage());
                                    }
                                });
                            }
                        }
                        else {
                            //Log.d(TAG, "get failed with ", task.getException());
                            dismissProgressDialog();
                            UToast.show(VerificationCodeActivity.this,task.getException().getMessage());
                        }
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dismissProgressDialog();
                UToast.show(VerificationCodeActivity.this,getResources().getString(R.string.number_sms_invalid));
            }
        });
    }


    @Override
    protected void onDestroy() {
        realm.close();

        super.onDestroy();
    }
}
