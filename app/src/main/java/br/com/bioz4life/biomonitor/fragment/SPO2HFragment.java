package br.com.bioz4life.biomonitor.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.linktop.MonitorDataTransmissionManager;
import com.linktop.infs.OnSpO2ResultListener;
import com.linktop.whealthService.MeasureType;
import com.linktop.whealthService.task.OxTask;
import com.timqi.sectorprogressview.ColorfulRingProgressView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.SPO2H;
import br.com.bioz4life.biomonitor.database.RealmUtilitySPO2;
import br.com.bioz4life.biomonitor.database.SPO2Realm;
import br.com.bioz4life.biomonitor.databinding.FragmentSpo2hBinding;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.UToast;
import br.com.bioz4life.biomonitor.utils.Utility;
import br.com.bioz4life.biomonitor.widget.wave.PPGDrawWave;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ccl on 2017/2/7.
 * SPO2H测量界面
 */

public class SPO2HFragment extends MeasureFragment
        implements OnSpO2ResultListener {

    private SPO2H model;
    private FragmentSpo2hBinding binding;
    private OxTask mOxTask;

    @BindView(R.id.crpv)
    ColorfulRingProgressView crpv;

    @BindView(R.id.tvSPO2)
    TextView tvSPO2;

    @BindView(R.id.tvLabelO2)
    TextView tvLabelO2;


    @BindView(R.id.seekSpo2)
    SeekBar seekSpo2;

    @BindView(R.id.tvBPM)
    TextView tvBPM;

    PPGDrawWave oxWave;

    @BindView(R.id.btSyncData)
    Button btSyncData;

    public SPO2HFragment() {
    }

    @Override
    public boolean startMeasure() {
        btSyncData.setEnabled(false);

        crpv.animateIndeterminate();
        //oxWave.setVisibility(View.VISIBLE);
        //tvSPO2.setVisibility(View.GONE);
        //tvLabelO2.setVisibility(View.GONE);
        tvSPO2.setText("0");
        tvBPM.setText("0");
        seekSpo2.setProgress(0);

        if (mOxTask != null) {
            mOxTask.start();
        }
        else {

            MonitorDataTransmissionManager.getInstance().startMeasure(MeasureType.SPO2);
        }
        return true;
    }

    @Override
    public void stopMeasure() {
        btSyncData.setEnabled(true);

        if (mOxTask != null) {
            crpv.stopAnimateIndeterminate();

            mOxTask.stop();
        }
        else {
            crpv.stopAnimateIndeterminate();

            MonitorDataTransmissionManager.getInstance().stopMeasure();
        }
    }

    @Override
    public String getTitle() {
        return "Oximetria SpO₂";
    }

    @Override
    protected ViewDataBinding onCreateBindingView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = setBindingContentView(inflater, R.layout.fragment_spo2h, container);
        binding.setContent(this);
        this.btnMeasure = binding.btnMeasure;
        model = new SPO2H();
        binding.setModel(model);
        oxWave = new PPGDrawWave();
        binding.ppgWave.setDrawWave(oxWave);
        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mHcService != null) {
            mOxTask = mHcService.getBleDevManager().getOxTask();
            mOxTask.setOnSpO2ResultListener(this);
        }
        else {
            MonitorDataTransmissionManager.getInstance().setOnSpO2ResultListener(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void reset() {
        model.reset();
        oxWave.clear();
    }


    private void sendData(){
        mActivity.showProgressDialog(getResources().getString(R.string.sending_data));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        final SPO2H item = RealmUtilitySPO2.getUnsyncSpo2(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        String createDate = df.format("yyyy-MM-dd HH:mm:ss", item.getCreatedDate()).toString();

        Map<String, Object> map = new HashMap<>();
        map.put("ts",item.getTs());
        map.put("value",item.getValue());
        map.put("hr",item.getHr());
        map.put("device_id",LocalPrefs.getValue(mActivity,mActivity.device_id));
        map.put("device_key",LocalPrefs.getValue(mActivity,mActivity.device_key));
        map.put("createdDate",createDate);

        db.collection("biomonitor")
                .document(LocalPrefs.getPhoneNumber(mActivity))
                .collection("exams_spo2h").document(item.getId()).set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                RealmUtilitySPO2.updateStatus(mActivity.getRealm(), item.getId(), LocalPrefs.getPhoneNumber(mActivity));

                mActivity.dismissProgressDialog();

                syncData();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mActivity.dismissProgressDialog();
                new AlertDialogBuilder(mActivity)
                        .setTitle("Erro")
                        .setMessage(e.getMessage())
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
    }

    @OnClick(R.id.btSyncData)
    public void syncData(){
        if (Utility.isOnline()) {

            SPO2H spo2HToBeSyncronized = RealmUtilitySPO2.getUnsyncSpo2(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

            //1.1 Sincronizar um por um
            if (spo2HToBeSyncronized != null) {
                sendData();
            }
            else{
                //1.2 Atualizar lista
                mActivity.showProgressDialog(getResources().getString(R.string.loading_data));

                String userPhone = LocalPrefs.getPhoneNumber(mActivity);

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                CollectionReference collectionReference = db.collection("biomonitor")
                        .document(userPhone)
                        .collection("exams_spo2h");

                collectionReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            mActivity.dismissProgressDialog();

                            // apagar
                            RealmUtilitySPO2.deleteDataSyncronized(mActivity.getRealm(),userPhone);

                            int count = task.getResult().size();

                            if (count > 0){
                                ArrayList<SPO2H> exams = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    //Log.d(TAG, document.getId() + " => " + document.getData());

                                    Map<String, Object> map = document.getData();

                                    SPO2H item =  new SPO2H();
                                    item.setId(document.getId());
                                    item.setTs((long) map.get("ts"));
                                    item.setValue((long) map.get("value"));
                                    item.setHr((long) map.get("hr"));
                                    Date createdDate = Utility.stringToDate((String) map.get("createdDate"),"yyyy-MM-dd HH:mm:ss");
                                    item.setCreatedDate(createdDate);

                                    exams.add(item);
                                }
                                // add data do server
                                RealmUtilitySPO2.addFromServer(mActivity.getRealm(),exams,userPhone);
                            }
                        }
                        else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                            mActivity.dismissProgressDialog();

                            UToast.show(mActivity,task.getException().getMessage());
                        }
                    }
                });
            }
        }
        else{
            UToast.show(getActivity(),getResources().getString(R.string.no_connection));
        }
    }

    @Override
    public void onSpO2Result(int spo2h, int heartRate) {
        model.setTs(System.currentTimeMillis() / 1000L);
        model.setValue(spo2h);
        model.setHr(heartRate);

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    btSyncData.setEnabled(true);

                    tvSPO2.setText(String.valueOf(model.getValue()) + "%");
                    tvBPM.setText(String.valueOf(model.getHr()));

                    if(model.getValue() > 200){
                        seekSpo2.setProgress(200);
                    }
                    else{
                        seekSpo2.setProgress((int)model.getValue());
                    }
                    // Salvar no banco esta amostra
                    SPO2Realm spo2Realm = new SPO2Realm();
                    spo2Realm.id = UUID.randomUUID().toString();
                    spo2Realm.createdDate = new Date();
                    spo2Realm.user = LocalPrefs.getPhoneNumber(mActivity);
                    spo2Realm.hr = model.getHr();
                    spo2Realm.ts = model.getTs();
                    spo2Realm.value = model.getValue();
                    mActivity.getRealm().beginTransaction();
                    mActivity.getRealm().copyToRealm(spo2Realm);
                    mActivity.getRealm().commitTransaction();
                }
                finally {
                    crpv.stopAnimateIndeterminate();

                    resetState();
                }

            }
        });
    }

    @Override
    public void onSpO2Wave(int value) {
        //oxWave.addData(value);
    }

    @Override
    public void onSpO2End() {
        model.setTs(System.currentTimeMillis() / 1000L);
        resetState();
    }

    @Override
    public void onFingerDetection(int state) {
        if (state == FINGER_NO_TOUCH) {
            stopMeasure();
            model.setValue(0);
            model.setHr(0);
            toast("No finger was detected on the SpO₂ sensor.");
        }
    }
}
