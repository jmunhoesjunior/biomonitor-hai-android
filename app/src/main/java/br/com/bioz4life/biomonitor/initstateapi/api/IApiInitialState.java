package br.com.bioz4life.biomonitor.initstateapi.api;

import java.util.ArrayList;

import br.com.bioz4life.biomonitor.initstateapi.model.ModelInitialState;
import br.com.bioz4life.biomonitor.initstateapi.model.ModelPayloadInitialState;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IApiInitialState {

    @POST("api/buckets")
    Call<ResponseBody> createBucket(@Body ModelPayloadInitialState request);

    @POST("api/events")
    Call<ResponseBody> sendData(@Body ArrayList<ModelInitialState> request);
}
