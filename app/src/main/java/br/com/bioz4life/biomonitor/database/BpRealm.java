package br.com.bioz4life.biomonitor.database;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BpRealm extends RealmObject{

    @PrimaryKey
    public String id;
    // CountryCode+DDD+Phone
    public String user;
    public Date createdDate;
    public boolean isSync;

    public long ts;
    public long sbp;
    public long dbp;
    public long hr;

}
