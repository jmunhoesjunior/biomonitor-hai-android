package br.com.bioz4life.biomonitor.database;

import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.model.Contact;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmUtilityContact {

    public static void addFromServer(Realm realm, List<Contact> list, String user){
        if (list != null) {
            realm.beginTransaction();
            for(Contact c : list){
                ContactRealm contactRealm = new ContactRealm();
                // appCONTACT nao tem createdDate
                contactRealm.id = c.id;
                contactRealm.email = c.email;
                contactRealm.isFavorite = c.isFavorite;
                contactRealm.isNotify = c.isNotify;
                contactRealm.isPhysician = c.isPhysician;
                contactRealm.phone = c.phone;
                contactRealm.cpf = c.cpf;
                contactRealm.relationship = c.relationship;
                contactRealm.fullname = c.fullname;
                contactRealm.user = user;
                contactRealm.isSync = true;
                realm.copyToRealm(contactRealm);
            }
            realm.commitTransaction();
        }
    }

    public static void deleteDataSyncronized(Realm realm, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<ContactRealm> results = realm
                        .where(ContactRealm.class)
                        .equalTo("user", user)
                        .equalTo("isSync",true)
                        .findAll();

                if (results != null) {
                    results.deleteAllFromRealm();
                }
            }
        });
    }
    public static ContactRealm find(Realm realm, String id){
        return realm
                .where(ContactRealm.class)
                .equalTo("id", id).findFirst();
    }

    public static Contact getUnsyncContacts(Realm realm, String user){
        Contact contact = null;
        ContactRealm itemRealm = realm
                .where(ContactRealm.class)
                .equalTo("user", user)
                .equalTo("isSync",false)
                .sort("createdDate", Sort.ASCENDING)
                .findFirst();

        if (itemRealm != null) {
            contact = new Contact();
            contact.relationship = itemRealm.relationship;
            contact.phone = itemRealm.phone;
            contact.email = itemRealm.email;
            contact.cpf = itemRealm.cpf;
            contact.fullname = itemRealm.fullname;
            contact.id = itemRealm.id;
            contact.isFavorite = itemRealm.isFavorite;
            contact.isNotify = itemRealm.isNotify;
            contact.isPhysician = itemRealm.isPhysician;
        }
        return contact;
    }
    public static void updateStatus(Realm realm, final String id, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                ContactRealm itemRealm = realm
                        .where(ContactRealm.class)
                        .equalTo("id", id)
                        .equalTo("user", user)
                        .findFirst();

                if(itemRealm != null){
                    itemRealm.isSync = true;
                }
            }
        });
    }
    public static List<Contact> getContacts(Realm realm, String user){
        List<Contact> list = new ArrayList<>();
        RealmResults<ContactRealm> results = realm
                                    .where(ContactRealm.class)
                                    .equalTo("user", user)
                                    .sort("fullname", Sort.ASCENDING)
                                    .findAll();

        if (results != null) {
            for(ContactRealm item : results){
                Contact contact = new Contact();
                contact.relationship = item.relationship;
                contact.phone = item.phone;
                contact.email = item.email;
                contact.fullname = item.fullname;
                contact.cpf = item.cpf;
                contact.id = item.id;
                contact.isFavorite = item.isFavorite;
                contact.isNotify = item.isNotify;
                contact.isPhysician = item.isPhysician;

                list.add(contact);
            }
        }
        return list;
    }
}
