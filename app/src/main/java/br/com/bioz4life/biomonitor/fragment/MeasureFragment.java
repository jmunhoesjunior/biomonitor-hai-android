package br.com.bioz4life.biomonitor.fragment;

import android.view.View;
import android.widget.Button;

import com.linktop.MonitorDataTransmissionManager;

import br.com.bioz4life.biomonitor.health.App;

/**
 * Created by ccl on 2017/12/27.
 */

public abstract class MeasureFragment extends BaseFragment {

    protected Button btnMeasure;

    public MeasureFragment() {
    }

    public abstract boolean startMeasure();

    public abstract void stopMeasure();


    public void clickMeasure(View v) {
        if (App.isUseCustomBleDevService) {
            if (!mHcService.isConnected) {
                    toast("Bluetooth desconectado");
                return;
            }
            //Determine se o dispositivo está carregando, não medindo ao carregar
            if (mHcService.getBleDevManager().getBatteryTask().isCharging()) {
                toast("O dispositivo está carregando. Por favor aguarde.");
                return;
            }
            if (mHcService.getBleDevManager().isMeasuring()) {
                stopMeasure();
                //Definir ViewPager para slide
                mActivity.viewPager.setCanScroll(true);
                mActivity.viewFocus.setVisibility(View.GONE);
                btnMeasure.setText("Iniciar medição");
            }
            else {
                reset();
                if (startMeasure()) {
                /*
             *
                Atenção: para evitar confusão na lógica do código, certifique-se de que o usuário não possa
                passar nenhuma rota durante o processo de medição.
             * (É claro que, se o usuário forçar o fechamento da página, não importa) Mude para a interface
             * de outros itens de medição até o final desta medição.
             */
                    //设置ViewPager不可滑动
                    mActivity.viewPager.setCanScroll(false);
                    mActivity.viewFocus.setVisibility(View.VISIBLE);
                    btnMeasure.setText("EM MEDIÇÃO, clique para terminar");
                }
            }
        }
        else {
            final MonitorDataTransmissionManager manager = MonitorDataTransmissionManager.getInstance();

            //Determine se o telefone está conectado ao dispositivo
            if (!manager.isConnected()) {
                toast("Bluetooth desconectado");
                return;
            }
            //Durante a medição, é avaliado se o dispositivo está carregando ou não.
            if (manager.isCharging()) {
                toast("O dispositivo está carregando. Por favor aguarde.");
                return;
            }
            //Determine se está medindo ...
            if (manager.isMeasuring()) {
//            if (mPosition != 2) {//Não há um método de parada para a temperatura do corpo e parar quando o clique pára a temperatura fora do corpo.
                //Pare de medir
                stopMeasure();
                //Definir ViewPager para slide
                mActivity.viewPager.setCanScroll(true);
                mActivity.viewFocus.setVisibility(View.GONE);
                btnMeasure.setText("Iniciar medição");
//            }
            }
            else {
                reset();
                //Comece a medir
                if (startMeasure()) {
                      /*
                       *
                        Atenção: para evitar confusão na lógica do código, certifique-se de que o usuário
                        não possa passar nenhuma rota durante o processo de medição.
                       * (É claro que, se o usuário forçar o fechamento da página, não importa)
                       * Mude para a interface de outros itens de medição até o final desta medição.
                       */
                    //Definir ViewPager para não deslizar
                    mActivity.viewPager.setCanScroll(false);
                    mActivity.viewFocus.setVisibility(View.VISIBLE);
                    btnMeasure.setText("EM MEDIÇÃO, clique para terminar");
                }
            }
        }
    }

    protected void resetState() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mActivity.reset();
                btnMeasure.setText("Iniciar medição");
            }
        });
        stopMeasure();
    }
}
