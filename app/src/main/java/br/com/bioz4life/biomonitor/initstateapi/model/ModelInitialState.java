package br.com.bioz4life.biomonitor.initstateapi.model;

import java.io.Serializable;

public class ModelInitialState implements Serializable {

    public String key;
    public String value;

    /*
    @SerializedName("NOME")
    public String NOME;

    @SerializedName("FREQ CARDIACA")
    public String FREQ_CARDIACA;

    @SerializedName("FREQ RESPIRATORIA")
    public String FREQ_RESPIRATORIA;

    @SerializedName("PA SISTOLICA")
    public String PA_SISTOLICA;

    @SerializedName("PA DIASTOLICA")
    public String PA_DIASTOLICA;

    @SerializedName("TEMPERATURA")
    public String TEMPERATURA;

    @SerializedName("SPO2")
    public String SPO2;

    @SerializedName("GLICOSE")
    public String GLICOSE;

    */
}
