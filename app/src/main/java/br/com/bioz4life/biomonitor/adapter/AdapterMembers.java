package br.com.bioz4life.biomonitor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.model.Contact;

public class AdapterMembers extends RecyclerView.Adapter {

    private Context mContext;
    private List<Contact> mItems;
    private OnClickMember onClickMember;

    public interface OnClickMember{
        void onClickMember(int position);
    }

    public AdapterMembers(Context ctx, List<Contact> items, OnClickMember onClickMember){
        this.mContext = ctx;
        this.mItems = items;
        this.onClickMember = onClickMember;
    }

    public Contact getContact(int position){
        return mItems.get(position);
    }
    private class Holder extends RecyclerView.ViewHolder {

        TextView tvName;
        TextView tvPhone;
        TextView tvEmail;
        TextView tvCpf;
        TextView tvRelationship;
        CheckBox checkPhysician;
        CheckBox checkFavorite;
        CheckBox checkNotify;
        LinearLayout linearMain;

        public Holder(@NonNull View itemView) {
            super(itemView);

            tvName = (TextView)itemView.findViewById(R.id.tvName);
            tvPhone = (TextView)itemView.findViewById(R.id.tvPhone);
            tvEmail = (TextView)itemView.findViewById(R.id.tvEmail);
            tvCpf = (TextView)itemView.findViewById(R.id.tvCpf);
            tvRelationship = (TextView)itemView.findViewById(R.id.tvRelationship);
            checkPhysician = (CheckBox) itemView.findViewById(R.id.checkPhysician);
            checkFavorite = (CheckBox)itemView.findViewById(R.id.checkFavorite);
            checkNotify = (CheckBox)itemView.findViewById(R.id.checkNotify);
            linearMain = (LinearLayout) itemView.findViewById(R.id.linearMain);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(mContext)
                .inflate(R.layout.item_contact, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {

        final Holder holder = (Holder)viewHolder;

        final Contact contact = mItems.get(i);
        if (contact.fullname != null) {
            holder.tvName.setText(contact.fullname);
        }
        if (contact.cpf != null) {
            holder.tvCpf.setText(contact.cpf);
        }
        holder.checkPhysician.setChecked(contact.isPhysician);
        holder.checkFavorite.setChecked(contact.isFavorite);
        holder.checkNotify.setChecked(contact.isNotify);

        holder.checkPhysician.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked != contact.isPhysician){
                    holder.checkPhysician.setChecked(contact.isPhysician);
                }
            }
        });

        holder.checkFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked != contact.isFavorite){
                    holder.checkFavorite.setChecked(contact.isFavorite);
                }
            }
        });

        holder.checkNotify.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked != contact.isNotify){
                    holder.checkNotify.setChecked(contact.isNotify);
                }
            }
        });
        if (contact.email != null) {
            holder.tvEmail.setText(contact.email);
        }
        if (contact.phone != null) {
            holder.tvPhone.setText(contact.phone);
        }
        if (contact.relationship != null) {
            holder.tvRelationship.setText(contact.relationship);
        }

        holder.linearMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                onClickMember.onClickMember(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mItems == null) {
            return  0;
        }
        return mItems.size();
    }
}
