package br.com.bioz4life.biomonitor.bean;

// Model CSServerAPI

import java.util.Date;

/**
 * Created by ccl on 2018/1/25.
 */

public class Bg {

    private String id;

    private Date createdDate;

    private long ts = 0;
    private double value = 0.0d;

    public Bg() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }


    public long getTs() {
        return ts;
    }


    public void setTs(long ts) {
        this.ts = ts;
    }


    public double getValue() {
        return value;
    }


    public void setValue(double value) {
        this.value = value;
        //notifyChange();
    }

    public void reset() {
        value = 0.0d;
        ts = 0L;
        //notifyChange();
    }

    public boolean isEmptyData() {
        return value == 0.0d || ts == 0L;
    }

    @Override
    public String toString() {
        return "Bt{" +
                "ts=" + ts +
                ", value=" + value +
                '}';
    }
}
