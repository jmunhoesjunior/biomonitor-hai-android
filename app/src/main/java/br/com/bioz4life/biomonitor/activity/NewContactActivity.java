package br.com.bioz4life.biomonitor.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;

import java.util.Date;
import java.util.UUID;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.database.ContactRealm;
import br.com.bioz4life.biomonitor.database.RealmUtilityContact;
import br.com.bioz4life.biomonitor.model.Contact;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.UToast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class NewContactActivity extends BaseActivity {

    @BindView(R.id.etNickname)
    TextInputEditText etNickname;

    @BindView(R.id.etPhone)
    TextInputEditText etPhone;

    @BindView(R.id.etEmail)
    TextInputEditText etEmail;

    @BindView(R.id.etRelationship)
    TextInputEditText etRelationship;

    @BindView(R.id.etCpf)
    TextInputEditText etCpf;

    @BindView(R.id.checkPhysician)
    CheckBox checkPhysician;

    @BindView(R.id.checkFavorite)
    CheckBox checkFavorite;

    @BindView(R.id.checkNotify)
    CheckBox checkNotify;

    private Realm realm;

    private boolean isEdit;
    private String id_edit = "";

    @BindView(R.id.btDelete)
    Button btDelete;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_contact);

        ButterKnife.bind(this);

        setUpActionBar("Novo Contato", getString(R.string.app_name), true);

        realm = Realm.getDefaultInstance();

        isEdit = getIntent().getBooleanExtra("isEdit",false);
        if (!isEdit) {
            btDelete.setVisibility(View.GONE);
        }

        if (isEdit) {
            String json  = getIntent().getStringExtra("contact");
            if (json != null) {
                if(!json.equals("")){
                    Contact contact = new Gson().fromJson(json,Contact.class);
                    if (contact != null) {
                        id_edit = contact.id;

                        if(contact.phone != null)
                            etPhone.setText(contact.phone);
                        if(contact.relationship != null)
                            etRelationship.setText(contact.relationship);
                        if(contact.fullname != null)
                            etNickname.setText(contact.fullname);
                        if(contact.email != null)
                            etEmail.setText(contact.email);

                        if(contact.cpf != null)
                            etCpf.setText(contact.cpf);

                        checkFavorite.setChecked(contact.isFavorite);
                        checkNotify.setChecked(contact.isNotify);
                        checkPhysician.setChecked(contact.isPhysician);
                    }
                }
            }
        }
    }

    @OnClick(R.id.btDelete)
    public void onDelete(){
        showProgressDialog(getResources().getString(R.string.deleting_data));

        final ContactRealm contactRealm = RealmUtilityContact.find(realm,id_edit);
        if (contactRealm != null) {

            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("biomonitor")
                    .document(LocalPrefs.getPhoneNumber(this))
                    .collection("contacts")
                    .document(contactRealm.id).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    dismissProgressDialog();

                    realm.beginTransaction();
                    contactRealm.deleteFromRealm();
                    realm.commitTransaction();
                    setResult(RESULT_OK);
                    finish();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    dismissProgressDialog();
                }
            });

        }
    }

    @OnClick(R.id.btSave)
    public void onClickSave(){

        if(etPhone.getText().toString().equals("")){
            UToast.show(this,"O campo Telefone do país é obrigatório");
        }
        else if(etNickname.getText().toString().equals("")){
            UToast.show(this,"O campo Nome do país é obrigatório");
        }
        else if(etEmail.getText().toString().equals("")){
            UToast.show(this,"O campo E-mail do país é obrigatório");
        }
        else if(etRelationship.getText().toString().equals("")){
            UToast.show(this,"O campo Relação do país é obrigatório");
        }
        else{
            ContactRealm contactRealm = RealmUtilityContact.find(realm,id_edit);
            if (contactRealm == null) {
                contactRealm = new ContactRealm();
            }
            if (!isEdit){
                contactRealm.id = UUID.randomUUID().toString();
                contactRealm.createdDate = new Date();
                contactRealm.user = LocalPrefs.getPhoneNumber(this);
            }
            realm.beginTransaction();
            contactRealm.email = etEmail.getText().toString();
            contactRealm.isFavorite = checkFavorite.isChecked();
            contactRealm.isNotify = checkNotify.isChecked();
            contactRealm.isPhysician = checkPhysician.isChecked();
            contactRealm.phone = etPhone.getText().toString();
            contactRealm.relationship = etRelationship.getText().toString();
            contactRealm.fullname = etNickname.getText().toString();
            contactRealm.cpf = etCpf.getText().toString();
            contactRealm.isSync = false;
            realm.copyToRealm(contactRealm);
            realm.commitTransaction();

            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }
}
