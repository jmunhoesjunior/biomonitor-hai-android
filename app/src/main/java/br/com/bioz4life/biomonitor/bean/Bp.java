package br.com.bioz4life.biomonitor.bean;

import android.util.Log;

import java.util.Date;

// Model CSServerAPI
/**
 * Created by ccl on 2017/2/7.
 */

public class Bp {
    private String id;

    private long ts;
    private long sbp;
    private long dbp;
    private long hr;
    private Date createdDate;

    public Bp() {
    }

    public Bp(long ts, long sbp, long dbp, long hr) {
        this.ts = ts;
        this.sbp = sbp;
        this.dbp = dbp;
        this.hr = hr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public long getSbp() {
        return sbp;
    }


    public void setSbp(long sbp) {
        this.sbp = sbp;
        //notifyChange();
    }


    public long getDbp() {
        return dbp;
    }

    public void setDbp(long dbp) {
        this.dbp = dbp;
        //notifyChange();
    }


    public long getHr() {
        return hr;
    }


    public void setHr(long hr) {
        this.hr = hr;
        //notifyChange();
    }


    public long getTs() {
        return ts;
    }


    public void setTs(long ts) {
        this.ts = ts;
    }

    public void reset() {
        Log.e("BP", "reset");
        sbp = 0;
        dbp = 0;
        hr = 0;
        ts = 0L;
        //notifyChange();
    }

    public boolean isEmptyData() {
        return sbp == 0 || dbp == 0 || hr == 0 || ts == 0L;
    }

    @Override
    public String toString(){
        return "Bp{" +
                "sbp=" + sbp +
                ", dbp=" + dbp +
                ", hr=" + hr +
                '}';
    }
}
