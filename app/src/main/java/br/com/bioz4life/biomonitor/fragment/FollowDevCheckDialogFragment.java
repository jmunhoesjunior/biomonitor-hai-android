package br.com.bioz4life.biomonitor.fragment;

import android.annotation.SuppressLint;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.linktop.MonitorDataTransmissionManager;

import br.com.bioz4life.biomonitor.BR;
import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.utils.GetVerificationCodeUtil;

/**
 * Created by ccl on 2016/11/26.
 */
@SuppressLint("ValidFragment")
public class FollowDevCheckDialogFragment extends BaseDialogFragment
        implements View.OnClickListener,
        TextWatcher {

    private String[] info;
    private GetVerificationCodeUtil verificationCodeUtil;
    private String verificationCode = "";


    public FollowDevCheckDialogFragment() {
        super();
    }

    public void setInfo(String[] info) {
        this.info = info;
    }

    @Override
    protected boolean isDataBinding() {
        return true;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_dialog_follow_dev_check;
    }

    @Override
    protected void onInit() {
        setDialogTitle("Atenção ao equipamento");
        verificationCodeUtil = new GetVerificationCodeUtil();
        mBinding.setVariable(BR.acc, info[2]);
        mBinding.setVariable(BR.verification, verificationCodeUtil);
        mBinding.setVariable(BR.verificationCodeListener, this);
        mBinding.setVariable(BR.textChangeListener, this);
        onClick(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getPositiveButton() != null) {
            getPositiveButton().setEnabled(verificationCode.length() == 4);
        }
    }

    @Override
    public void onDestroy() {
        if (verificationCodeUtil != null) verificationCodeUtil.destroy();
        super.onDestroy();
    }

    @Override
    protected int getNeutralText() {
        return 0;
    }

    @Override
    protected int getNegativeText() {
        return android.R.string.cancel;
    }

    @Override
    protected View.OnClickListener getNegativeListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MonitorDataTransmissionManager.getInstance().disConnectBle();
                //HmLoadDataTool.getInstance().destroyCssSocket();
                dismiss();
            }
        };
    }

    @Override
    protected View.OnClickListener getPositiveListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };
    }

    @Override
    public void onClick(View view) {
        verificationCodeUtil.setAccTextEnable(false);
        verificationCodeUtil.startRunning();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        verificationCode = charSequence.toString();
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (getPositiveButton() != null) {
            getPositiveButton().setEnabled(verificationCode.length() == 4);
        }
    }
}
