package br.com.bioz4life.biomonitor.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableFloat;
import androidx.databinding.ObservableInt;
import androidx.databinding.ViewDataBinding;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.linktop.MonitorDataTransmissionManager;
import com.linktop.constant.IUserProfile;
import com.linktop.infs.OnEcgResultListener;
import com.linktop.whealthService.MeasureType;
import com.linktop.whealthService.task.EcgTask;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.activity.ECGLargeActivity;
import br.com.bioz4life.biomonitor.bean.ECG;
import br.com.bioz4life.biomonitor.binding.ObservableString;
import br.com.bioz4life.biomonitor.database.ECGRealm;
import br.com.bioz4life.biomonitor.database.RealmUtilityECG;
import br.com.bioz4life.biomonitor.databinding.FragmentEcgBinding;
import br.com.bioz4life.biomonitor.model.UserProfile;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.UToast;
import br.com.bioz4life.biomonitor.utils.Utility;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ccl on 2017/2/7.
 * Interface de medição de ECG
 */

public class ECGFragment extends MeasureFragment
        implements OnEcgResultListener {

    private ECG model;
    private FragmentEcgBinding binding;
    private final ObservableInt pagerSpeed = new ObservableInt(1);
    private final ObservableFloat gain = new ObservableFloat(1.0f);
    private final ObservableString pagerSpeedStr = new ObservableString("25mm/s");
    private final ObservableString gainStr = new ObservableString("10mm/mV");
    private final StringBuilder ecgWaveBuilder = new StringBuilder();
    private EcgTask mEcgTask;

    @BindView(R.id.btSyncData)
    Button btSyncData;

    public ECGFragment() {
    }

    @Override
    public boolean startMeasure() {

        btSyncData.setEnabled(false);
        if (mEcgTask != null) {
            if (mEcgTask.isModuleExist()) {
                mActivity.showProgressDialog(getResources().getString(R.string.loading_ecg));
                mEcgTask.initEcgTg();
                mEcgTask.start();
                return true;
            }
            else {
                toast("This Device's ECG module is not exist.");
                return false;
            }
        }
        else {
            if (MonitorDataTransmissionManager.getInstance().isEcgModuleExist()) {
                MonitorDataTransmissionManager.getInstance().startMeasure(MeasureType.ECG);
                return true;
            }
            else {
                toast("This Device's ECG module is not exist.");
                return false;
            }
        }
    }

    @Override
    public void stopMeasure() {

        btSyncData.setEnabled(true);
        if (mEcgTask != null) {
            mEcgTask.stop();
        }
        else {
            MonitorDataTransmissionManager.getInstance().stopMeasure();
        }
    }

    @Override
    public String getTitle() {
        return "Eletrocardiograma";
    }

    @Override
    protected ViewDataBinding onCreateBindingView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = setBindingContentView(inflater, R.layout.fragment_ecg, container);
        binding.setContent(this);
        this.btnMeasure = binding.btnMeasure;
        model = new ECG();
        binding.setModel(model);
        binding.setPagerSpeed(pagerSpeed);
        binding.setGain(gain);
        binding.setPagerSpeedStr(pagerSpeedStr);
        binding.setGainStr(gainStr);
        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        IUserProfile userProfile = new UserProfile("ccl", 1, 27, 170, 60);

        if (mHcService != null) {
            mEcgTask = mHcService.getBleDevManager().getEcgTask();
            mEcgTask.setOnEcgResultListener(this);
            //It is better to set user information who is measuring ECG.
            //if it is not,SDK will use a default user information.
            // Of course,it may reduce the accuracy of the measurement results.
            mEcgTask.setUserProfile(userProfile);
            //mEcgTask.setUserInfo("ccl", 27, 166, 65, false);
        }
        else {
            //MonitorDataTransmissionManager.getInstance().setECGUerInfo("ccl", 27, 166, 65, false);
            MonitorDataTransmissionManager.getInstance().setUserProfile(userProfile);
            MonitorDataTransmissionManager.getInstance().setOnEcgResultListener(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void reset() {
        model.reset();
        ecgWaveBuilder.setLength(0);
        binding.ecgDrawChart.clear();
    }

    long startTs = 0L;
    int i = 0;

    /*
    * Ponto de dados de ECG
     * */
    @Override
    public synchronized void onDrawWave(int wave) {
        //        i++;
        //        if (startTs == 0L) startTs = System.currentTimeMillis();
        //Desenhar pontos de dados no controle do ECG
        binding.ecgDrawChart.preparePoint(wave);
        //Salve os pontos de dados no contêiner e visualize a imagem grande.
        ecgWaveBuilder.append(wave).append(",");
    }

    @Override
    public void onSignalQuality(int quality) {

    }

    @Override
    public void onECGValues(int key, int value) {
        switch (key) {
            case RRI_MAX:
                model.setRrMax(value);
                break;
            case RRI_MIN:
                model.setRrMin(value);
                break;
            case HR:
                model.setHr(value);
                break;
            case HRV:
                model.setHrv(value);
                break;
            case MOOD:
                model.setMood(value);
                break;
            case RR://Respiratory rate.
                model.setBr(value);
                break;
        }
    }


    /*
    * A duração da medição do ECG, uma vez disparada, indica o final de uma medição de ECG
     * */
    @Override
    public void onEcgDuration(long duration) {

        final long l = (System.currentTimeMillis() - startTs) / 1000L;
        final long l1 = i / l;
        Log.e("onEcgDuration", "" + l1);
        startTs = 0L;
        i = 0;
        model.setDuration(duration);
        model.setTs(System.currentTimeMillis() / 1000L);
        String ecgWave = ecgWaveBuilder.toString();
        ecgWave = ecgWave.substring(0, ecgWave.length() - 1);
        model.setWave(ecgWave);
        model.setCreatedDate(new Date());
        binding.invalidateAll();


        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try{
                    btSyncData.setEnabled(true);

                    // Salvar no banco esta amostra
                    ECGRealm ecgRealm = new ECGRealm();
                    ecgRealm.id = UUID.randomUUID().toString();
                    ecgRealm.createdDate = new Date();
                    ecgRealm.user = LocalPrefs.getPhoneNumber(mActivity);
                    ecgRealm.br = model.getBr();
                    ecgRealm.duration = model.getDuration();
                    ecgRealm.hr = model.getHr();
                    ecgRealm.hrv = model.getHrv();
                    ecgRealm.mood = model.getMood();
                    ecgRealm.rrMax = model.getRrMax();
                    ecgRealm.rrMin = model.getRrMin();
                    ecgRealm.ts = model.getTs();
                    ecgRealm.wave = model.getWave();

                    if(gain.get() == 0.5f){
                        ecgRealm.gain  = 5;
                    }
                    else if(gain.get() == 1){
                        ecgRealm.gain  = 10;
                    }
                    else{
                        ecgRealm.gain  = 20;
                    }

                    if(pagerSpeed.get() == 1){
                        ecgRealm.pagerSpeed  = 25;
                    }
                    else{
                        ecgRealm.pagerSpeed  = 50;
                    }
                    // Ajustar esses valores para o valor corrente atual e ajustar nas telas que havera conflito
                    //ecgRealm.gain = gain.get();
                    //ecgRealm.pagerSpeed = pagerSpeed.get();

                    mActivity.getRealm().beginTransaction();
                    mActivity.getRealm().copyToRealm(ecgRealm);
                    mActivity.getRealm().commitTransaction();
                }
                finally {

                    resetState();
                }
            }
        });
    }

    public void openECGLarge(View v) {
        Intent intent = new Intent(mActivity, ECGLargeActivity.class);
        intent.putExtra("pagerSpeed", pagerSpeed.get());
        intent.putExtra("gain", gain.get());
        intent.putExtra("model", model);
        startActivity(intent);
    }

    /*
    * Clique para definir a base de tempo (velocidade do papel)
    * Este valor reflete a amplitude do eixo x do ECG，O valor definido não é salvo aqui.，
    * Por favor, salve você mesmo para que os valores salvos sejam automaticamente definidos na próxima vez que você iniciar a página.
    * */
    public void clickSetPagerSpeed(View v) {
        int checkedItem = pagerSpeed.get() - 1;
        onShowSingleChoiceDialog(true, "Velocidade", R.array.ecg_pager_speed, checkedItem);
    }

    /*
    * Clique para definir o ganho
    * Este valor reflete a amplitude do eixo y do ECG, o valor ajustado não é salvo aqui, salve você mesmo.
    * Para que os valores salvos sejam automaticamente definidos na próxima vez que você iniciar a página.
    * */
    public void clickSetGain(View v) {
        int checkedItem = gain.get() == 0.5f ? 0 : (int) gain.get();
        onShowSingleChoiceDialog(false, "Ganho", R.array.ecg_gain, checkedItem);
    }

    private void onShowSingleChoiceDialog(final boolean isTimeRef, String titleResId, int itemsId, int checkedItem) {
        final String[] items = getResources().getStringArray(itemsId);
        new AlertDialogBuilder(mActivity)
                .setTitle(titleResId)
                .setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isTimeRef) {
                            pagerSpeed.set(which + 1);
                            pagerSpeedStr.set(items[which]);
                        }
                        else {
                            gain.set(which == 0 ? 0.5f : which);
                            gainStr.set(items[which]);
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.close, null)
                .create()
                .show();
    }

    private void sendData(){
        mActivity.showProgressDialog(getResources().getString(R.string.sending_data));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        final ECG item = RealmUtilityECG.getUnsyncECG(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        String createDate = df.format("yyyy-MM-dd HH:mm:ss", item.getCreatedDate()).toString();

        Map<String, Object> map = new HashMap<>();
        map.put("ts",item.getTs());
        map.put("duration",item.getDuration());
        map.put("rrMax",item.getRrMax());
        map.put("rrMin",item.getRrMin());
        map.put("hr",item.getHr());
        map.put("hrv",item.getHrv());
        map.put("mood",item.getMood());
        map.put("br",item.getBr());
        map.put("wave",item.getWave());
        map.put("pagerSpeed",item.getPagerSpeed());
        map.put("gain",item.getGain());
        map.put("device_id",LocalPrefs.getValue(mActivity,mActivity.device_id));
        map.put("device_key",LocalPrefs.getValue(mActivity,mActivity.device_key));
        map.put("createdDate",createDate);

        db.collection("biomonitor")
                .document(LocalPrefs.getPhoneNumber(mActivity))
                .collection("exams_ecg").document(item.getId()).set(map).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                RealmUtilityECG.updateStatus(mActivity.getRealm(), item.getId(), LocalPrefs.getPhoneNumber(mActivity));

                mActivity.dismissProgressDialog();

                syncData();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mActivity.dismissProgressDialog();
                new AlertDialogBuilder(mActivity)
                        .setTitle("Erro")
                        .setMessage(e.getMessage())
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
    }

    @OnClick(R.id.btSyncData)
    public void syncData(){

        if (Utility.isOnline()) {

            ECG btToBeSyncronized = RealmUtilityECG.getUnsyncECG(mActivity.getRealm(),LocalPrefs.getPhoneNumber(mActivity));

            //1.1 Sincronizar um por um
            if (btToBeSyncronized != null) {
                sendData();
            }
            else{
                //1.2 Atualizar lista
                mActivity.showProgressDialog(getResources().getString(R.string.loading_data));

                String userPhone = LocalPrefs.getPhoneNumber(mActivity);

                FirebaseFirestore db = FirebaseFirestore.getInstance();

                CollectionReference collectionReference = db.collection("biomonitor")
                        .document(userPhone)
                        .collection("exams_ecg");

                collectionReference.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            mActivity.dismissProgressDialog();

                            // apagar
                            RealmUtilityECG.deleteDataSyncronized(mActivity.getRealm(),userPhone);

                            int count = task.getResult().size();

                            if (count > 0){
                                ArrayList<ECG> exams = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    //Log.d(TAG, document.getId() + " => " + document.getData());

                                    Map<String, Object> map = document.getData();

                                    ECG item =  new ECG();
                                    item.setId(document.getId());
                                    item.setTs((long) map.get("ts"));
                                    item.setDuration((long) map.get("duration"));
                                    item.setRrMax((long) map.get("rrMax"));
                                    item.setRrMin((long) map.get("rrMin"));
                                    item.setHr((long) map.get("hr"));
                                    item.setHrv((long) map.get("hrv"));
                                    item.setMood((long) map.get("mood"));
                                    item.setBr((long) map.get("br"));
                                    item.setWave((String) map.get("wave"));
                                    item.setPagerSpeed((long) map.get("pagerSpeed"));
                                    item.setGain((double) map.get("gain"));
                                    Date createdDate = Utility.stringToDate((String) map.get("createdDate"),"yyyy-MM-dd HH:mm:ss");
                                    item.setCreatedDate(createdDate);

                                    exams.add(item);
                                }
                                // add data do server
                                RealmUtilityECG.addFromServer(mActivity.getRealm(),exams,userPhone);
                            }
                        }
                        else {
                            //Log.d(TAG, "Error getting documents: ", task.getException());
                            mActivity.dismissProgressDialog();

                            UToast.show(mActivity,task.getException().getMessage());
                        }
                    }
                });
            }
        }
        else{
            UToast.show(getActivity(),getResources().getString(R.string.no_connection));
        }
    }

}
