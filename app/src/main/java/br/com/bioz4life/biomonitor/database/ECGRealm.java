package br.com.bioz4life.biomonitor.database;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ECGRealm extends RealmObject {

    @PrimaryKey
    public String id;
    // CountryCode+DDD+Phone
    public String user;
    public Date createdDate;
    public boolean isSync;

    public long ts;
    public long duration;
    public long rrMax;
    public long rrMin;
    public long hr;
    public long hrv;
    public long mood;
    public long br;
    public String wave;
    public long pagerSpeed;
    public double gain;
}
