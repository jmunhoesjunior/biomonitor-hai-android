package br.com.bioz4life.biomonitor.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.annotation.Nullable;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ccl on 2017/3/22.
 * <p>
 * Página de inicialização do APP
*/
public class  LauncherActivity extends BaseActivity {

    @BindView(R.id.tvVersion)
    TextView tvVersion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_launcher);
        ButterKnife.bind(this);

        PackageManager manager = this.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(this.getPackageName(), PackageManager.GET_ACTIVITIES);
            tvVersion.setText("Versão: " + info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String phone = LocalPrefs.getPhoneNumber(LauncherActivity.this);
                if(phone == null ){
                    /*Option to go to a select biomonitor or thermometer*/
                    Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if(phone.equals("")){
                    /*Option to go to a select biomonitor or thermometer*/
                    Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    Intent intent = new Intent(getBaseContext(), HealthMonitorActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        },3000);
    }
}
