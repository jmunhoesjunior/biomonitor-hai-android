package br.com.bioz4life.biomonitor.database;

import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.bean.ECG;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmUtilityECG {

    public static List<ECG>getAll(Realm realm, String user){
        List<ECG> result = new ArrayList<>();
            RealmResults<ECGRealm> realmRealmResults = realm
                .where(ECGRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .findAll();

        if (realmRealmResults != null) {
            for(ECGRealm item : realmRealmResults){
                ECG ecg = new ECG();
                ecg.setCreatedDate(item.createdDate);
                ecg.setTs(item.ts);
                ecg.setId(item.id);
                ecg.setBr(item.br);
                ecg.setHr(item.hr);
                ecg.setDuration(item.duration);
                ecg.setGain(item.gain);
                ecg.setHrv(item.hrv);
                ecg.setMood(item.mood);
                ecg.setPagerSpeed(item.pagerSpeed);
                ecg.setRrMax(item.rrMax);
                ecg.setRrMin(item.rrMin);
                ecg.setWave(item.wave);
                result.add(ecg);
            }
        }

        return result;
    }
    public static List<ECG> getLastSamplesLimit5(Realm realm, String user){

        List<ECG> result = new ArrayList<>();
        RealmResults<ECGRealm> realmRealmResults = realm
                .where(ECGRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .limit(5)
                .findAll();

        if (realmRealmResults != null) {
            for(ECGRealm item : realmRealmResults){
                ECG ecg = new ECG();
                ecg.setCreatedDate(item.createdDate);
                ecg.setTs(item.ts);
                ecg.setId(item.id);
                ecg.setBr(item.br);
                ecg.setHr(item.hr);
                ecg.setDuration(item.duration);
                ecg.setGain(item.gain);
                ecg.setHrv(item.hrv);
                ecg.setMood(item.mood);
                ecg.setPagerSpeed(item.pagerSpeed);
                ecg.setRrMax(item.rrMax);
                ecg.setRrMin(item.rrMin);
                ecg.setWave(item.wave);
                result.add(ecg);
            }
        }

        return result;
    }

    public static ECG getLastSample(Realm realm, String user){

        ECG result = null;
        RealmResults<ECGRealm> realmRealmResults = realm
                .where(ECGRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .limit(1)
                .findAll();

        if (realmRealmResults != null) {
            for(ECGRealm item : realmRealmResults){
                result = new ECG();
                result.setCreatedDate(item.createdDate);
                result.setTs(item.ts);
                result.setId(item.id);
                result.setBr(item.br);
                result.setHr(item.hr);
                result.setDuration(item.duration);
                result.setGain(item.gain);
                result.setHrv(item.hrv);
                result.setMood(item.mood);
                result.setPagerSpeed(item.pagerSpeed);
                result.setRrMax(item.rrMax);
                result.setRrMin(item.rrMin);
                result.setWave(item.wave);
            }
        }

        return result;
    }

    public static ECG getUnsyncECG(Realm realm, String user){
        ECG ecg = null;
        ECGRealm itemRealm = realm
                .where(ECGRealm.class)
                .equalTo("user", user)
                .equalTo("isSync",false)
                .sort("createdDate", Sort.ASCENDING)
                .findFirst();

        if (itemRealm != null) {
            ecg = new ECG();
            ecg.setCreatedDate(itemRealm.createdDate);
            ecg.setId(itemRealm.id);
            ecg.setTs(itemRealm.ts);
            ecg.setGain(itemRealm.gain);
            ecg.setPagerSpeed(itemRealm.pagerSpeed);
            ecg.setWave(itemRealm.wave);
            ecg.setRrMin(itemRealm.rrMin);
            ecg.setRrMax(itemRealm.rrMax);
            ecg.setMood(itemRealm.mood);
            ecg.setHrv(itemRealm.hrv);
            ecg.setHr(itemRealm.hr);
            ecg.setDuration(itemRealm.duration);
            ecg.setBr(itemRealm.br);
        }
        return ecg;
    }

    public static void addFromServer(Realm realm, List<ECG> list, String user){
        if (list != null) {
            realm.beginTransaction();
            for(ECG c : list){
                ECGRealm ecgRealm = new ECGRealm();

                ecgRealm.id = c.getId();
                ecgRealm.user = user;
                ecgRealm.isSync = true;
                ecgRealm.createdDate = c.getCreatedDate();
                ecgRealm.ts = c.getTs();
                ecgRealm.gain = c.getGain();
                ecgRealm.pagerSpeed = c.getPagerSpeed();
                ecgRealm.wave = c.getWave();
                ecgRealm.rrMin = c.getRrMin();
                ecgRealm.rrMax = c.getRrMax();
                ecgRealm.mood = c.getMood();
                ecgRealm.hrv = c.getHrv();
                ecgRealm.hr = c.getHr();
                ecgRealm.duration = c.getDuration();
                ecgRealm.br = c.getBr();
                realm.copyToRealm(ecgRealm);
            }
            realm.commitTransaction();
        }
    }

    public static void deleteDataSyncronized(Realm realm, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<ECGRealm> results = realm
                        .where(ECGRealm.class)
                        .equalTo("user", user)
                        .equalTo("isSync",true)
                        .findAll();

                if (results != null) {
                    results.deleteAllFromRealm();
                }
            }
        });
    }

    public static void updateStatus(Realm realm, final String id, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                ECGRealm itemRealm = realm
                        .where(ECGRealm.class)
                        .equalTo("id", id)
                        .equalTo("user", user)
                        .findFirst();

                if(itemRealm != null){
                    itemRealm.isSync = true;
                }
            }
        });
    }
}
