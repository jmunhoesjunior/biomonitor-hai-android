package br.com.bioz4life.biomonitor.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.Bg;
import br.com.bioz4life.biomonitor.bean.Bp;
import br.com.bioz4life.biomonitor.bean.Bt;
import br.com.bioz4life.biomonitor.bean.ECG;
import br.com.bioz4life.biomonitor.bean.SPO2H;
import br.com.bioz4life.biomonitor.carenet.RetrofitCarenet;
import br.com.bioz4life.biomonitor.carenet.model.CarenetSender;
import br.com.bioz4life.biomonitor.carenet.model.Paciente;
import br.com.bioz4life.biomonitor.carenet.model.Resultado;
import br.com.bioz4life.biomonitor.carenet.model.ResultadoCompleto;
import br.com.bioz4life.biomonitor.carenet.model.ResultadoObs;
import br.com.bioz4life.biomonitor.database.RealmUtilityBg;
import br.com.bioz4life.biomonitor.database.RealmUtilityBp;
import br.com.bioz4life.biomonitor.database.RealmUtilityBt;
import br.com.bioz4life.biomonitor.database.RealmUtilityECG;
import br.com.bioz4life.biomonitor.database.RealmUtilitySPO2;
import br.com.bioz4life.biomonitor.database.RealmUtilityUserAccount;
import br.com.bioz4life.biomonitor.database.UserAccountRealm;
import br.com.bioz4life.biomonitor.hai.RetrofitHai;
import br.com.bioz4life.biomonitor.hai.models.HaiData;
import br.com.bioz4life.biomonitor.hai.models.HaiObject;
import br.com.bioz4life.biomonitor.hai.models.HaiObjectBpm;
import br.com.bioz4life.biomonitor.hai.models.HaiRoot;
import br.com.bioz4life.biomonitor.initstateapi.api.RetrofitIInitialState;
import br.com.bioz4life.biomonitor.initstateapi.model.ModelInitialState;
import br.com.bioz4life.biomonitor.initstateapi.model.ModelPayloadInitialState;
import br.com.bioz4life.biomonitor.printer.PrinterActivity;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.Utility;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

public class AccountActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    private static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION =9000;
    private static final int REQUEST_CAMERA_PERMISSION = 9001;
    private static final int REQUEST_TAKE_PICTURE = 9002;

    @BindView(R.id.imgProfile)
    CircleImageView imgProfile;

    @BindView(R.id.tvPhone)
    TextView tvPhone;

    @BindView(R.id.etNickname)
    TextInputEditText etNickname;

    @BindView(R.id.etFullname)
    TextInputEditText etFullname;

    @BindView(R.id.etCpf)
    TextInputEditText etCpf;

    @BindView(R.id.etAddress)
    TextInputEditText etAddress;

    @BindView(R.id.etEmail)
    TextInputEditText etEmail;

    @BindView(R.id.etGender)
    TextInputEditText etGender;

    @BindView(R.id.etBirthday)
    TextInputEditText etBirthday;

    Calendar myCalendar = Calendar.getInstance();

    private int checkedItemGender = -1;

    @BindView(R.id.btCarenet)
    Button btCarenet;

    @BindView(R.id.btHai)
    Button btHai;

    @BindView(R.id.relativePrinter)
    RelativeLayout relativePrinter;

    @BindView(R.id.relativeShare)
    RelativeLayout relativeShare;

    Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_account);

        ButterKnife.bind(this);

        setUpActionBar("Usuário", getString(R.string.app_name), true);

        etGender.setInputType(InputType.TYPE_NULL);
        etBirthday.setInputType(InputType.TYPE_NULL);

        tvPhone.setText("");
        etAddress.setText("");
        etBirthday.setText("");
        etEmail.setText("");
        etFullname.setText("");
        etGender.setText("");
        etNickname.setText("");

        realm = realm.getDefaultInstance();

        loadInfos();


        relativePrinter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AccountActivity.this, PrinterActivity.class));
            }
        });

        relativeShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AccountActivity.this, PdfViewActivity.class));
            }
        });

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(AccountActivity.this, AccountActivity.this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        etGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String[] options = getResources().getStringArray(R.array.list_gender);

                new AlertDialogBuilder(AccountActivity.this)
                        .setTitle("")
                        .setSingleChoiceItems(options, -1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                etGender.setText(options[which]);

                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton(R.string.close, null)
                        .create()
                        .show();
            }
        });

        btHai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get user info
                UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm,LocalPrefs.getPhoneNumber(AccountActivity.this));

                if (userAccountRealm != null) {
                    if (userAccountRealm.Cpf != null) {
                        if (!userAccountRealm.Cpf.equals("")) {
                            showProgressDialog(getResources().getString(R.string.sending_data));


                            // bp
                            Bp bp = RealmUtilityBp.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            // bt
                            Bt bt = RealmUtilityBt.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            // bg
                            Bg bg = RealmUtilityBg.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            // spo2h
                            SPO2H spo2H = RealmUtilitySPO2.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            // ecg
                            ECG ecg = RealmUtilityECG.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            HaiRoot root = new HaiRoot();
                            root.data = new HaiData();

                            android.text.format.DateFormat df = new android.text.format.DateFormat();
                            HaiObject dt = new HaiObject();
                            dt.label = "Data/Hora";
                            dt.value = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date()).toString();
                            dt.unit = "";
                            root.data.DateTime = dt;
                            root.id = userAccountRealm.Cpf.replace("-","").replace(".","").replace(" ","");

                            if (bp != null) {
                                HaiObjectBpm o = new HaiObjectBpm();
                                o.label = "Pressão Arterial";
                                o.value = String.valueOf(bp.getSbp()) + "/" + String.valueOf(bp.getDbp());
                                o.unit = "mmHg";
                                o.bpm = "("+String.valueOf(bp.getHr())+"bpm)";
                                root.data.BP = o;
                            }

                            if (spo2H != null) {
                                HaiObjectBpm o = new HaiObjectBpm();
                                o.label = "Oximetria";
                                o.value = String.valueOf(spo2H.getValue());
                                o.unit = "%";
                                o.bpm = "("+String.valueOf(spo2H.getHr())+"bpm)";
                                root.data.SPO2 = o;
                            }

                            if (bt != null) {
                                HaiObject o = new HaiObject();
                                o.label = "Temperatura";
                                o.value = String.valueOf(bt.getTemp());
                                o.unit = "oC";
                                root.data.TEMP = o;
                            }

                            if (bg != null) {
                                double mgdl = 18.018 * bg.getValue();
                                int mgdlInt = (int)Math.round(mgdl);

                                HaiObject o = new HaiObject();
                                o.label = "Glicemia";
                                o.value = String.valueOf(mgdlInt);
                                o.unit = "mg/dL";
                                root.data.GLUCOSE = o;
                            }

                            if (ecg != null) {
                                ArrayList<HaiObject> objects = new ArrayList<>();

                                HaiObject o = new HaiObject();
                                o.label = "Freq.Cardíaca";
                                o.value = String.valueOf(ecg.getHr());
                                o.unit = "bpm";
                                objects.add(o);

                                o = new HaiObject();
                                o.label = "Freq.Respiratória";
                                o.value = String.valueOf(ecg.getBr());
                                o.unit = "rpm";
                                objects.add(o);

                                o = new HaiObject();
                                o.label = "Variabilidade FC";
                                o.value = String.valueOf(ecg.getTs());
                                o.unit = "ms";
                                objects.add(o);

                                o = new HaiObject();
                                o.label = "R-R máximo";
                                o.value = String.valueOf(ecg.getRrMax());
                                o.unit = "ms";
                                objects.add(o);

                                o = new HaiObject();
                                o.label = "R-R mínimo";
                                o.value = String.valueOf(ecg.getRrMin());
                                o.unit = "ms";
                                objects.add(o);

                                root.data.ECG = objects;
                            }

                            RetrofitHai api = new RetrofitHai();
                            api.send(root, new RetrofitHai.OnResponseListener() {
                                @Override
                                public void onResponse(Boolean success, String message) {
                                    dismissProgressDialog();
                                    if (!success) {
                                        if (message != null) {
                                            new AlertDialogBuilder(AccountActivity.this)
                                                    .setTitle("Erro")
                                                    .setMessage(message)
                                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    }).create().show();
                                        } else {
                                            new AlertDialogBuilder(AccountActivity.this)
                                                    .setTitle("Erro")
                                                    .setMessage("Erro interno desconhecido")
                                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    }).create().show();
                                        }
                                    } else {
                                        new AlertDialogBuilder(AccountActivity.this)
                                                .setTitle("Sucesso")
                                                .setMessage("Dados enviados com sucesso")
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        dialog.dismiss();
                                                    }
                                                }).create().show();
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });

        btCarenet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get last data

                // get user info
                UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm,LocalPrefs.getPhoneNumber(AccountActivity.this));

                if (userAccountRealm != null) {
                    if (userAccountRealm.Gender != null &&
                            userAccountRealm.FullName != null &&
                            userAccountRealm.Cpf != null &&
                            userAccountRealm.Birthday != null &&
                            userAccountRealm.Nickname != null) {

                        if (!userAccountRealm.Gender.equals("") &&
                                !userAccountRealm.FullName.equals("") &&
                        !userAccountRealm.Cpf.equals("") &&
                                !userAccountRealm.Birthday.equals("") &&
                                !userAccountRealm.Nickname.equals("") &&
                        Utility.onlyDigits(userAccountRealm.Nickname) &&
                        userAccountRealm.Nickname.length() == 4) {

                            showProgressDialog(getResources().getString(R.string.sending_data));

                            // bp
                            Bp bp = RealmUtilityBp.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            // bt
                            Bt bt = RealmUtilityBt.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            // bg
                            Bg bg = RealmUtilityBg.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            // spo2h
                            SPO2H spo2H = RealmUtilitySPO2.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));

                            // ecg
                            ECG ecg = RealmUtilityECG.getLastSample(realm, LocalPrefs.getPhoneNumber(AccountActivity.this));


                            CarenetSender carenetSender = new CarenetSender();
                            carenetSender.fabricante = "PHILIPS";
                            carenetSender.modelo = "MDHL-IN";
                            carenetSender.uti = "BIOZ";
                            carenetSender.tipoMensagem = "ORU";
                            carenetSender.idiomaMensagem = "8859/1";
                            carenetSender.leito = userAccountRealm.Nickname;
                            carenetSender.hospital = "5f2853390e6a5b0011624e0f";
                            android.text.format.DateFormat df = new android.text.format.DateFormat();
                            carenetSender.dataHoraMensagem = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date()).toString();
                            carenetSender.paciente = new Paciente();
                            carenetSender.paciente.pid = userAccountRealm.Cpf;
                            carenetSender.paciente.nome = userAccountRealm.FullName;
                            carenetSender.paciente.sexo = userAccountRealm.Gender;
                            carenetSender.paciente.dataNascimento = userAccountRealm.Birthday.replace("-","");
                            carenetSender.leitoStatus = "ACTIVE";
                            carenetSender.paciente.resultados = new ArrayList<>();

                            if (bp != null) {
                                ResultadoCompleto r1 = new ResultadoCompleto();
                                r1.valor = String.valueOf(bp.getHr());
                                r1.typeResult = "FREQUENCIA_CARDIACA";
                                r1.units = "bpm";
                                r1.observationResultsStatus = "F";
                                r1.initials = "FC";
                                r1.codSignVital = 2;
                                carenetSender.paciente.resultados.add(r1);

                                ResultadoCompleto r2 = new ResultadoCompleto();
                                r2.valor = String.valueOf(bp.getSbp());
                                r2.typeResult = "SISTOLICA";
                                r2.units = "mmHg";
                                r2.observationResultsStatus = "F";
                                r2.initials = "PAS";
                                r2.codSignVital = 4;
                                carenetSender.paciente.resultados.add(r2);

                                ResultadoCompleto r3 = new ResultadoCompleto();
                                r3.valor = String.valueOf(bp.getDbp());
                                r3.typeResult = "DIASTOLICA";
                                r3.units = "mmHg";
                                r3.observationResultsStatus = "F";
                                r3.initials = "PAD";
                                r3.codSignVital = 5;
                                carenetSender.paciente.resultados.add(r3);

                                r2 = new ResultadoCompleto();
                                r2.valor = String.valueOf(bp.getSbp());
                                r2.typeResult = "PRESSAO";
                                r2.units = "mmHg";
                                r2.observationResultsStatus = "F";
                                r2.initials = "PAM";
                                r2.codSignVital = 6;
                                carenetSender.paciente.resultados.add(r2);
                            }

                            if (bt != null) {
                                ResultadoCompleto r1 = new ResultadoCompleto();
                                r1.valor = String.valueOf(bt.getTemp());
                                r1.typeResult = "TEMPERATURA";
                                r1.units = "ºC";
                                r1.observationResultsStatus = "F";
                                r1.initials = "TEMP";
                                r1.codSignVital = 1;
                                carenetSender.paciente.resultados.add(r1);
                            }

                            if(spo2H != null) {
                                ResultadoObs r1 = new ResultadoObs();
                                r1.valor = String.valueOf(spo2H.getValue());
                                r1.typeResult = "OXIGENIO_SATURACAO";
                                r1.units = "%";
                                r1.observationResultsStatus = "F";
                                r1.initials = "PAM";
                                carenetSender.paciente.resultados.add(r1);
                            }
                            if(bg != null) {
                                Resultado r1 = new Resultado();
                                double mgdl = 18.018 * bg.getValue();
                                int mgdlInt = (int)Math.round(mgdl);
                                r1.valor = String.valueOf(mgdlInt);
                                r1.typeResult = "GLICOSE";
                                r1.units = "mg/dl";
                                r1.initials = "GLIC";
                                carenetSender.paciente.resultados.add(r1);
                            }
                            if (ecg != null) {
                                Resultado r1 = new Resultado();
                                r1.valor = String.valueOf(ecg.getMood());
                                r1.typeResult = "STRESS";
                                r1.units = "!";
                                r1.initials = "STR";
                                carenetSender.paciente.resultados.add(r1);

                                ResultadoCompleto r2 = new ResultadoCompleto();
                                r2.valor = String.valueOf(ecg.getBr());
                                r2.typeResult = "FREQUENCIA_RESPIRATORIA";
                                r2.units = "rpm";
                                r2.initials = "FR";
                                r2.codSignVital = 3;
                                r2.observationResultsStatus = "F";
                                carenetSender.paciente.resultados.add(r2);
                            }

                            // set request initial state request body
                            ArrayList<ModelInitialState> states = new ArrayList<>();
                            ModelInitialState m1 = new ModelInitialState();
                            m1.key = "NOME";
                            m1.value = userAccountRealm.FullName;
                            states.add(m1);

                            m1 = new ModelInitialState();
                            m1.key = "FREQ CARDIACA";
                            m1.value = String.valueOf(ecg.getHr()) + " bpm :heart:";
                            states.add(m1);

                            m1 = new ModelInitialState();
                            m1.key = "FREQ RESPIRATORIA";
                            m1.value = String.valueOf(ecg.getBr()) + " rpm :nose:";
                            states.add(m1);

                            double mgdl = 18.018 * bg.getValue();
                            int mgdlInt = (int)Math.round(mgdl);
                            m1 = new ModelInitialState();
                            m1.key = "GLICOSE";
                            m1.value = String.valueOf(mgdlInt) + " mg/dL :syringe:";

                            m1 = new ModelInitialState();
                            m1.key = "PA DIASTOLICA";
                            m1.value = String.valueOf(bp.getDbp()) + " mmHg :vertical_traffic_light:";
                            states.add(m1);

                            m1 = new ModelInitialState();
                            m1.key = "PA SISTOLICA";
                            m1.value = String.valueOf(bp.getSbp()) + " mmHg :vertical_traffic_light:";
                            states.add(m1);

                            m1 = new ModelInitialState();
                            m1.key = "SPO2";
                            m1.value = String.valueOf(spo2H.getValue()) + " % :cloud:";
                            states.add(m1);

                            m1 = new ModelInitialState();
                            m1.key = "TEMPERATURA";
                            m1.value = String.valueOf(bt.getTemp()) + " oC :snowflake:";
                            states.add(m1);

                            // set create initial state request body
                            ModelPayloadInitialState payloadCreate = new ModelPayloadInitialState();
                            payloadCreate.bucketKey = LocalPrefs.getPhoneNumber(AccountActivity.this)
                                    .replace("+55","");
                            payloadCreate.bucketName = userAccountRealm.FullName;

                            RetrofitCarenet api = new RetrofitCarenet();
                            RetrofitIInitialState apiInitState = new RetrofitIInitialState();
                            api.send(carenetSender, new RetrofitCarenet.OnResponseListener() {
                                @Override
                                public void onResponse(Boolean success) {
                                    apiInitState.createBucket(payloadCreate, new RetrofitCarenet.OnResponseListener() {
                                        @Override
                                        public void onResponse(Boolean success) {
                                            apiInitState.sendData(payloadCreate.bucketKey, states, new RetrofitCarenet.OnResponseListener() {
                                                @Override
                                                public void onResponse(Boolean success) {
                                                    dismissProgressDialog();
                                                    new AlertDialogBuilder(AccountActivity.this)
                                                            .setTitle("Sucesso")
                                                            .setMessage("Dados enviados com sucesso")
                                                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    dialog.dismiss();
                                                                }
                                                            }).create().show();
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            // dados incompletos
                           showErrorCarenet();
                        }
                    } else {
                        // dados incompletos
                        showErrorCarenet();
                    }
                } else {
                    // dados incompletos
                    showErrorCarenet();
                }
            }
        });
    }

    public void showErrorCarenet() {
        new AlertDialogBuilder(AccountActivity.this)
                .setTitle("Erro")
                .setMessage("Dados do usuário incompleto")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create().show();
    }

    private void loadInfos(){

        if (Utility.isOnline()){

            showProgressDialog(getResources().getString(R.string.loading_data));

            FirebaseFirestore db = FirebaseFirestore.getInstance();

            DocumentReference ref = db.collection("biomonitor").document(LocalPrefs.getPhoneNumber(this));

            ref.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        // Document found in the offline cache
                        DocumentSnapshot document = task.getResult();
                        //Log.d(TAG, "Cached document data: " + document.getData());

                        Map<String, Object> map = document.getData();
                        Map<String, Object>  items = (Map<String, Object>) map.get("user_info");

                        String birthday = (String) items.get("birthday");
                        String email = (String) items.get("email");
                        String fulladdress = (String) items.get("fulladdress");
                        String fullname = (String) items.get("fullname");
                        String gender = (String) items.get("gender");
                        String nickname = (String) items.get("nickname");
                        String phone = (String) items.get("phone");
                        String photo = (String) items.get("photo");
                        String cpf = (String) items.get("cpf");
                        String regcode = (String) items.get("regcode");

                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                UserAccountRealm itemRealm = new UserAccountRealm();
                                itemRealm.id = phone;
                                itemRealm.Birthday = birthday;
                                itemRealm.Email = email;
                                itemRealm.FullAddress = fulladdress;
                                itemRealm.FullName = fullname;
                                itemRealm.Gender = gender;
                                itemRealm.Nickname = nickname;
                                itemRealm.Photo = photo;
                                itemRealm.Cpf = cpf;
                                itemRealm.RegCode = regcode;

                                realm.copyToRealmOrUpdate(itemRealm);
                            }
                        });
                    }

                    showInfos();
                }
            });
        }
        else{
            showInfos();
        }
    }

    private void showInfos(){
        dismissProgressDialog();

        UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm,LocalPrefs.getPhoneNumber(this));

        if (userAccountRealm != null) {
            if (userAccountRealm.Nickname != null) {
                etNickname.setText(userAccountRealm.Nickname);
            }
            if (userAccountRealm.Birthday != null) {
                etBirthday.setText(Utility.formatDate(userAccountRealm.Birthday,"yyyy-MM-dd","dd/MM/yyyy"));
            }
            if (userAccountRealm.Email != null) {
                etEmail.setText(userAccountRealm.Email);
            }
            if (userAccountRealm.FullAddress != null) {
                etAddress.setText(userAccountRealm.FullAddress);
            }
            if (userAccountRealm.FullName != null) {
                etFullname.setText(userAccountRealm.FullName);
            }
            if (userAccountRealm.Gender != null) {
                etGender.setText(userAccountRealm.Gender);
            }
            if (userAccountRealm.id != null) {
                tvPhone.setText(userAccountRealm.id);
            }

            if (userAccountRealm.Cpf != null){
                etCpf.setText(userAccountRealm.Cpf);
            }
            Utility.loadUserProfile(AccountActivity.this,imgProfile,userAccountRealm.FullName,userAccountRealm.Photo);
        }
    }


    @OnClick(R.id.relativePhoto)
    public void changeImageProfile(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION);
            }
            else if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA_PERMISSION);
            }
            else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, REQUEST_TAKE_PICTURE);
            }
        }
        else {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, REQUEST_TAKE_PICTURE);
        }
    }


    @OnClick(R.id.btUpdate)
    public void updateData(){

        showProgressDialog(getResources().getString(R.string.updating_data));

        UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm, LocalPrefs.getPhoneNumber(this));

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        DocumentReference ref = db.collection("biomonitor").document(LocalPrefs.getPhoneNumber(this));

        String birthday = "";
        if(!etBirthday.getText().toString().equals("")) {
            birthday = Utility.formatDate(etBirthday.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd'T'HH:mm:ss.sss'Z'");
        }
        ref.update("user_info.email", etEmail.getText().toString(),
                "user_info.fulladdress", etAddress.getText().toString(),
                "user_info.fullname", etFullname.getText().toString(),
                "user_info.gender", etGender.getText().toString(),
                "user_info.nickname", etNickname.getText().toString(),
                "user_info.cpf", etCpf.getText().toString(),
                "user_info.birthday", birthday,
                "user_info.photo", userAccountRealm.Photo).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                dismissProgressDialog();

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        final UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm,LocalPrefs.getPhoneNumber(AccountActivity.this));

                        userAccountRealm.Cpf = etCpf.getText().toString();
                        userAccountRealm.Gender = etGender.getText().toString();
                        userAccountRealm.FullName =  etFullname.getText().toString();
                        userAccountRealm.FullAddress = etAddress.getText().toString();
                        if(!etBirthday.getText().toString().equals("")) {
                            userAccountRealm.Birthday = Utility.formatDate(etBirthday.getText().toString(), "dd/MM/yyyy", "yyyy-MM-dd");
                        }
                        userAccountRealm.Email = etEmail.getText().toString();
                        userAccountRealm.Nickname = etNickname.getText().toString();
                    }
                });

                new AlertDialogBuilder(AccountActivity.this)
                        .setTitle("Sucesso")
                        .setMessage("Dados armazenados com sucesso!")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                dismissProgressDialog();

                new AlertDialogBuilder(AccountActivity.this)
                        .setTitle("Erro")
                        .setMessage(e.getMessage())
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        // Atualiza texto edittet
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        etBirthday.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_TAKE_PICTURE && resultCode == RESULT_OK) {
            Bitmap thumpPhoto = (Bitmap) data.getExtras().get("data");

            if (Utility.isOnline()) {
                showProgressDialog(getResources().getString(R.string.uploading));

                /*android.text.format.DateFormat df = new android.text.format.DateFormat();
                String date = df.format("yyyy-MM-dd hh:mm:ss", new java.util.Date()).toString();
                String filename = date.replace("-","_")
                        .replace(" ","_")
                        .replace(":","_") + ".png";
                File sd = Environment.getExternalStorageDirectory();
                File dest = new File(sd, filename);
                FileOutputStream out = new FileOutputStream(dest);
                thumpPhoto.compress(Bitmap.CompressFormat.PNG, 100, out);
                out.flush();
                out.close();*/


                FirebaseStorage storage = FirebaseStorage.getInstance();

                StorageReference storageRef = storage.getReference().child("images");
                StorageReference appRef = storageRef.child("biomonitor");

                String pathReference = LocalPrefs.getPhoneNumber(this) + "profile.jpg";
                StorageReference imageRef = appRef.child(pathReference);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thumpPhoto.compress(Bitmap.CompressFormat.JPEG, 75, baos);
                byte[] dataBytes = baos.toByteArray();

                UploadTask uploadTask = imageRef.putBytes(dataBytes);
                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //StorageMetadata storageMetadata =taskSnapshot.getMetadata();
                        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {

                                dismissProgressDialog();
                                // get file path, save local
                                String url = uri.toString();

                                final UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm,LocalPrefs.getPhoneNumber(AccountActivity.this));

                                realm.executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        //final UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm,LocalPrefs.getPhoneNumber(AccountActivity.this));
                                        userAccountRealm.Photo = url;
                                    }
                                });
                                updateData();

                                Utility.loadUserProfile(AccountActivity.this,imgProfile,userAccountRealm.FullName,url);
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dismissProgressDialog();
                    }
                });
            }
            else{
                new AlertDialogBuilder(this)
                        .setTitle("Erro")
                        .setMessage("Sem conexão com a Internet!")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        realm.close();

        super.onDestroy();
    }
}
