package br.com.bioz4life.biomonitor.database;

import io.realm.Realm;

public class RealmUtilityUserAccount {

    public static UserAccountRealm find(Realm realm, String id){
        return realm
                .where(UserAccountRealm.class)
                .equalTo("id", id).findFirst();
    }
}
