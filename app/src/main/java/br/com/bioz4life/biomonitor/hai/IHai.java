package br.com.bioz4life.biomonitor.hai;

import com.google.gson.JsonElement;

import br.com.bioz4life.biomonitor.hai.models.HaiRoot;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IHai {

    @POST("biomonitor")
    Call<JsonElement> add(@Body HaiRoot request);
}
