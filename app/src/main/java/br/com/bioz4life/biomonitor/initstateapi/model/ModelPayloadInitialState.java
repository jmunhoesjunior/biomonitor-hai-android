package br.com.bioz4life.biomonitor.initstateapi.model;

import java.io.Serializable;

public class ModelPayloadInitialState implements Serializable {

    public String bucketKey;
    public String bucketName;
}
