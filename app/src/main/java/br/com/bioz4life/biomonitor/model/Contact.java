package br.com.bioz4life.biomonitor.model;

import java.io.Serializable;

public class Contact implements Serializable {

    public String id;
    public String phone;
    public String fullname;
    public String relationship;
    public String email;
    public String cpf;
    public boolean isPhysician;
    public boolean isNotify;
    public boolean isFavorite;
}
