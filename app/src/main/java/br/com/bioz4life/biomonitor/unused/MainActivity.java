package br.com.bioz4life.biomonitor.unused;

import android.os.Bundle;
import android.view.View;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.activity.BaseActivity;
import br.com.bioz4life.biomonitor.activity.HealthMonitorActivity;
import br.com.bioz4life.biomonitor.utils.ActivityHelper;

/**
 * Created by ccl on 2017/2/1.
 * <p>
 * O uso e a configuração do SDK no Demo devem ser usados ​​em estrita conformidade com o modo de demonstração.
 * Mas a interface fornecida pelo SDK deve ser capaz de atender ao desenvolvimento específico do projeto.
 * Os desenvolvedores podem consultar a demonstração para que o SDK corresponda melhor ao desenvolvimento de projetos específicos.
 * <p>
 * A interface fornecida pelo SDK ainda pode existir e pode haver locais no SDK que não são bem tratados pela lógica.
 * Se você encontrar algum desses problemas, você pode feedback.
 */
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        setUpActionBar("Monitor de Sinais Vitais", "BioMonitor by BioZ4Life (C) 2019", false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /*
    * Abra a página do termômetro
     * */
    public void clickOpenThermometerPage(View v) {
        ActivityHelper.launcher(this, ThermometerActivity.class);
    }

    /*
   * Abra a página do Health Detector
     * */
    public void clickOpenHealthMonitorPage(View v) {
        ActivityHelper.launcher(this, HealthMonitorActivity.class);
    }
}
