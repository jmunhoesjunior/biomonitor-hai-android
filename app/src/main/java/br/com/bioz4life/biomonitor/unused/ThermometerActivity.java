package br.com.bioz4life.biomonitor.unused;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableBoolean;

import com.linktop.DeviceType;
import com.linktop.MonitorDataTransmissionManager;
import com.linktop.constant.BluetoothState;
import com.linktop.infs.OnBleConnectListener;
import com.linktop.infs.OnScanTempListener;
import com.linktop.infs.OnThermInfoListener;
import com.linktop.whealthService.OnBLEService;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.activity.BaseActivity;
import br.com.bioz4life.biomonitor.adapter.DataBindingAdapter;
import br.com.bioz4life.biomonitor.binding.ObservableString;
import br.com.bioz4life.biomonitor.databinding.ActivityThermometerBinding;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.PermissionManager;
import br.com.bioz4life.biomonitor.widget.CustomRecyclerView;

/**
 * Created by ccl on 2017/2/10.
 * Interface de demonstração do termômetro
 */

public class ThermometerActivity extends BaseActivity
        implements MonitorDataTransmissionManager.OnServiceBindListener
        , CustomRecyclerView.RecyclerItemClickListener
        , OnBleConnectListener
        , OnScanTempListener {

    private static final int REQUEST_OPEN_BT = 0x23;

    private final ObservableString tempText = new ObservableString("");
    private final ObservableString qrCode = new ObservableString("");
    private final ObservableString btnText = new ObservableString("打开蓝牙");
    private final ObservableBoolean stopScanTempBtnShow = new ObservableBoolean(false);
    private final ObservableBoolean connBtnShow = new ObservableBoolean(false);
    private final ObservableBoolean isDevBind = new ObservableBoolean(false);
    private final ObservableBoolean qrCodeBtnShow = new ObservableBoolean(false);
    //private final ObservableBoolean isLogin = App.isLogin;
    private DataBindingAdapter<OnBLEService.DeviceSort> mAdapter;
    //private DataBindingAdapter<ThermBean> mLoadListAdapter;
    //private final List<Device> devList = new ArrayList<>();
    private BluetoothDevice mBluetoothDevice;
    private ObservableString mDevId = new ObservableString("");
    private long currTs = 0L;
    //private BindDevListAdapter mBindDevListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityThermometerBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_thermometer);
        setUpActionBar(binding.toolbar, "体温计", "Thermometer", true);
        //binding.setIsLogin(isLogin);
        binding.setBtnText(btnText);
        mAdapter = new DataBindingAdapter<>(this, R.layout.item_ble_dev);
        binding.setRecyclerAdapter(mAdapter);
        binding.setListCount(mAdapter.getListSize());
        binding.setItemClickListener(this);
        binding.setDevBind(isDevBind);
        binding.setStopScanTempBtnShowFlag(stopScanTempBtnShow);
        binding.setConnectBtnShowFlag(connBtnShow);
        binding.setQrCodeBtnShowFlag(qrCodeBtnShow);
        binding.setTempText(tempText);
        binding.setQrCode(qrCode);

        // Vincular o serviço,
        // type is Thermometer (termômetro termômetro),
        // A verificação por Bluetooth só verifica os dispositivos cujo nome do Bluetooth começa com "LinkTC" (este campo muda de acordo com o tipo de dispositivo desenvolvido, dá uma castanha,
        // Se você estiver usando este SDK para o desenvolvimento de um aplicativo com um nome Bluetooth começando com "RuihTC", esse parâmetro será preenchido com "RuihTC", entenda-o)
        MonitorDataTransmissionManager.getInstance().bind(DeviceType.Thermometer, this);

        //mLoadListAdapter = new DataBindingAdapter<>(this, R.layout.item_therm_load_data);
        //binding.setRecyclerAdapter3(mLoadListAdapter);

        /*if (isLogin.get()) {
            mBindDevListAdapter = new BindDevListAdapter(this, mDevId);
            binding.setRecyclerAdapter2(mBindDevListAdapter);
            //从服务器确认是否绑定
            getDevList(false);
        }*/
    }

    @Override
    protected void onDestroy() {
        //Serviço não vinculado
        MonitorDataTransmissionManager.getInstance().unBind();
        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean permissionGranted = PermissionManager.isPermissionGranted(grantResults);
        switch (requestCode) {
            case PermissionManager.requestCode_location:
                if (permissionGranted) {
                    clickConnect(null);
                } else {
                    toast("Nenhum direito de segmentação");
                }
                break;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_OPEN_BT:
                //蓝牙启动结果
                toast(resultCode == Activity.RESULT_OK ? "Bluetooth está ligado" : "Bluetooth não conseguiu abrir");
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (MonitorDataTransmissionManager.getInstance().isScanning())
            MonitorDataTransmissionManager.getInstance().autoScan(false);
        super.onBackPressed();
    }

    @Override
    public void onItemClick(View view, int position) {
        if (MonitorDataTransmissionManager.getInstance().isScanning()) {
            final OnBLEService.DeviceSort item = mAdapter.getItem(position);
            mBluetoothDevice = item.bleDevice;
            MonitorDataTransmissionManager.getInstance().startScanTemp(mBluetoothDevice, this);
            stopScanTempBtnShow.set(true);
            /*if (isLogin.get()) {
                final String bleDevName = item.bleDevice.getName();
                final String devId = getDevIdByDevName(bleDevName);
                mDevId.set(devId);
                ThermLoadDataTool.getInstance().setDevId(devId);
                checkDevIsBind(false);
            }*/
        } else {
            toast("A digitalização Bluetooth não está ativada");
        }
    }

    @Override
    public void onServiceBind() {
        //Registre a interface de retorno de chamada da conexão Bluetooth após a ligação do serviço ser bem-sucedida.
        MonitorDataTransmissionManager.getInstance().setOnBleConnectListener(this);
        //Inicialize o status do Bluetooth exibido pelo controle primeiro.
        onBleState(MonitorDataTransmissionManager.getInstance().getBleState());
        /*
        * Para evitar que alguns dispositivos indesejados apareçam na lista de verificação de dispositivos Bluetooth, você precisa chamar essa API para definir a lista de permissões de prefixo de nome de dispositivo Bluetooth.
         * Quando um dispositivo Bluetooth varre, o dispositivo com o campo de lista de desbloqueio como o prefixo do nome do dispositivo será adicionado à lista de varredura do dispositivo Bluetooth e o restante será filtrado.
         * PS: Não use esta API, a lista de dispositivos mostra todos os dispositivos de integridade semelhantes, não filtrados.
         * Se você usar esta API, insira um ID de recurso válido.
        * */
        MonitorDataTransmissionManager.getInstance().setScanDevNamePrefixWhiteList(R.array.thermometer_dev_name_prefixes);
    }

    @Override
    public void onServiceUnbind() {

    }

    @Override
    public void onBLENoSupported() {
        toast("Bluetooth não suporta");
    }

    @Override
    public void onOpenBLE() {
        startActivityForResult(new Intent("android.bluetooth.adapter.action.REQUEST_ENABLE"), REQUEST_OPEN_BT);
    }

    @Override
    public void onBleState(int bleState) {
        switch (bleState) {
            case BluetoothState.BLE_CLOSED:
                btnText.set("打开蓝牙");
                MonitorDataTransmissionManager.getInstance().stopScanTemp();
                reset();
                break;
            case BluetoothState.BLE_OPENED_AND_DISCONNECT:
                boolean scanning = MonitorDataTransmissionManager.getInstance().isScanning();
                Log.e("onBleState", "scanning ? " + scanning);
                if (scanning) {
                    btnText.set("Na verificação, clique para parar");
                } else {
                    btnText.set("Começar a digitalizar");
                    reset();
                }
                break;
//            case BluetoothState.BLE_CONNECTING_DEVICE:
//                btnText.set("连接中");
//                break;
            case BluetoothState.BLE_CONNECTED_DEVICE:
                btnText.set("Conectado, clique desconectado");
                connBtnShow.set(false);
                tempText.set("");
                stopScanTempBtnShow.set(false);
                qrCodeBtnShow.set(true);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        toast("Conectada, a conexão de ponto é bem-sucedida, a temperatura pára de escanear e desconectada");
                    }
                });
                break;
            case BluetoothState.BLE_CONNECTING_DEVICE:
                btnText.set("Conectando ...");
                break;
//            case BluetoothState.BLE_NOTIFICATION_ENABLED:
//                MonitorDataTransmissionManager.getInstance().getDevInfo(this);
//                break;
        }
    }

    @Override
    public void onUpdateDialogBleList() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mAdapter != null) {
                    mAdapter.setItems(MonitorDataTransmissionManager.getInstance().getDeviceList());
                }
            }
        });
    }


    public void clickConnect(View v) {
        final MonitorDataTransmissionManager manager = MonitorDataTransmissionManager.getInstance();
        switch (manager.getBleState()) {
            case BluetoothState.BLE_CLOSED:
                //Quando o módulo Bluetooth do celular não estiver ligado, clique para ativar o módulo Bluetooth.
                manager.bleCheckOpen();
                break;
            case BluetoothState.BLE_OPENED_AND_DISCONNECT:
                //O módulo Bluetooth do telefone está ligado, mas o dispositivo não está conectado
                if (manager.isScanning()) {
                    //Parar o dispositivo de digitalização
                    manager.autoScan(false);
                    btnText.set("Começar a digitalizar");
                    reset();
                } else {

                    // Ligue o dispositivo de digitalização, Android 6.0 e acima do sistema para prestar atenção para determinar se deve abrir [direitos de posicionamento] e [switch GPS],
                    // Ninguém consegue analisar o dispositivo. Não existe tal limitação no Android 6.0 e abaixo.
                    final boolean isObtain = PermissionManager.isObtain(this, PermissionManager.PERMISSION_LOCATION, PermissionManager.requestCode_location);
                    if (isObtain) {
                        if (PermissionManager.canScanBluetoothDevice(this)) {
                            //Iniciar o dispositivo de digitalização
                            manager.autoScan(true);
                            btnText.set("Na verificação, clique para parar");
                        } else {
                            new AlertDialogBuilder(this)
                                    .setTitle("提示")
                                    .setMessage("O Android 6.0 e superior requer o GPS para ativar dispositivos Bluetooth.")
                                    .setNegativeButton(android.R.string.cancel, null)
                                        .setPositiveButton("Ligue o GPS", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            PermissionManager.openGPS(ThermometerActivity.this);
                                        }
                                    }).create().show();
                        }
                    }
                }
                break;
            case BluetoothState.BLE_CONNECTING_DEVICE:
                toast("Conectado, por favor aguarde");
                break;
            case BluetoothState.BLE_CONNECTED_DEVICE:
                //O dispositivo está conectado ao telefone Bluetooth, clique para desconectar e redefinir o estado inicial dos controles relevantes.
                manager.disConnectBle();
                break;
        }
    }

    /**
     * Clique para parar de escanear a temperatura, observe que o Bluetooth não parou de desenhar o dispositivo neste momento.
     */
    public void clickStopScanTemp(View v) {
        MonitorDataTransmissionManager.getInstance().stopScanTemp();
        tempText.set("");
        stopScanTempBtnShow.set(false);
        connBtnShow.set(false);
        qrCodeBtnShow.set(false);
        qrCode.set("");
    }

    /**
     * Depois que o dispositivo estiver conectado ao Bluetooth, clique para obter o código QR do dispositivo.
     */
    public void clickToGetQRCode(View v) {
        if (MonitorDataTransmissionManager.getInstance().isConnected()) {
            MonitorDataTransmissionManager.getInstance().getDevInfo(new OnThermInfoListener() {
                //回调方法获取到的二维码，显示成二维码图片，并开启绑定
                @Override
                public void onThermQRCode(String result) {
                    //Exibir o código QR
                    qrCode.set(result);
                    //如果已登录，通过二维码绑定
                    //if (isLogin.get())
                    //    startBind(result);
                }
            });
        } else {
            toast("O dispositivo não está conectado ao telefone Bluetooth e o código QR não pode ser obtido.");
        }
    }

    /**
     * 点击连接设备，目的是为了获取设备二维码并绑定
     * 点击解绑设备
     */
    public void clickToConnectDev(View v) {
        final String btnText = ((Button) v).getText().toString();
        if (btnText.contains("connect")) {
            if (null != mBluetoothDevice) {
                MonitorDataTransmissionManager.getInstance().connectToBle(mBluetoothDevice);
            }
        }
    }

    public void clickUpload(View v) {
        /*final List<ThermBean> items = mLoadListAdapter.getItems();
        //.O conteúdo da lista deve ser classificado em ordem decrescente de registro de data e hora, ou seja,
         os primeiros dados são os dados mais recentes da lista e os últimos dados são os dados mais antigos da lista.

        if (items == null || items.size() == 0) {
            toast("Nenhum dado para upload\);
            return;
        }
        //Sem essa restrição parcial, o objeto de assinante a seguir responderá on error
        if (items.size() > 100) {
            toast("Até 100 dados de temperatura podem ser gerados em um upload simultâneo");
            return;
        }
        */
    }

    public void clickDownload(View v) {
        /*ThermLoadDataTool.getInstance().download()
                .subscribe(new CssSubscriber<List<ThermBean>>() {
                    @Override
                    public void onNextRequestSuccess(List<ThermBean> list) {
                        if (list.isEmpty()) {
                            toast("服务器无数据");
                        } else {
                            mLoadListAdapter.setItems(list);
                        }
                    }

                    @Override
                    public void onNextRequestFailed(int status) {

                    }

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });*/
    }

    public void clickCleanList(View v) {
        //mLoadListAdapter.clearItems();
    }

    private void reset() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.clearItems();
            }
        });
        mBluetoothDevice = null;
        //ThermLoadDataTool.getInstance().setDevId("");
        tempText.set("");
        qrCode.set("");
        isDevBind.set(false);
        connBtnShow.set(false);
        stopScanTempBtnShow.set(false);
        qrCodeBtnShow.set(false);
    }



    private void collectUploadData(long ts, double temp) {
        if (ts - currTs >= (10 * 1000L)) {
            if (temp > 33.0d) {
                currTs = ts;
                //final ThermBean thermBean = new ThermBean(ts, temp);
                //Log.e("collectUploadData", thermBean.toString());
                //mLoadListAdapter.addItemToFirstAndNotifyAll(thermBean);
            }
        }
    }

    /**
     * @param devAddress 设备蓝牙地址
     * @param temp       温度
     * @param battery    0 有电，1没电
     */
    @Override
    public void onScanTempResult(String devAddress, double temp, int battery) {
        tempText.set("Address:" + devAddress + "\nTemp:" + temp + "℃\nbattery:" + (battery == 0 ? "有电" : "没电"));
        connBtnShow.set(true);
        /*if (isLogin.get() && isDevBind.get()) {
            collectUploadData(System.currentTimeMillis(), temp);
        }*/
    }

    @Override
    public void onNoTemp() {
        String msg = "Nenhuma temperatura é escaneada por um longo tempo, possíveis condições：\n" +
                "（1）O dispositivo desliga-se automaticamente - a temperatura digitalizada pelo dispositivo durante cerca de 3 minutos é inferior a 28,0 ° C e o dispositivo considera que não está a ser medido, pelo que será automaticamente desligado.\n" +
                "（2）A bateria do dispositivo está morta\n" +
                "（3）A distância entre o dispositivo e o telefone é muito grande para receber a temperatura digitalizada.";
        Log.e("OnNoTemp", msg);
        toast("A temperatura não foi digitalizada por um longo período e a varredura foi interrompida.");
    }
}
