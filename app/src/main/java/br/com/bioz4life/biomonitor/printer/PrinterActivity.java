package br.com.bioz4life.biomonitor.printer;

import android.Manifest;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.activity.BaseActivity;
import br.com.bioz4life.biomonitor.bean.Bg;
import br.com.bioz4life.biomonitor.bean.Bp;
import br.com.bioz4life.biomonitor.bean.Bt;
import br.com.bioz4life.biomonitor.bean.ECG;
import br.com.bioz4life.biomonitor.bean.SPO2H;
import br.com.bioz4life.biomonitor.database.RealmUtilityBg;
import br.com.bioz4life.biomonitor.database.RealmUtilityBp;
import br.com.bioz4life.biomonitor.database.RealmUtilityBt;
import br.com.bioz4life.biomonitor.database.RealmUtilityECG;
import br.com.bioz4life.biomonitor.database.RealmUtilitySPO2;
import br.com.bioz4life.biomonitor.database.RealmUtilityUserAccount;
import br.com.bioz4life.biomonitor.database.UserAccountRealm;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import br.com.bioz4life.biomonitor.utils.Utility;
import io.realm.Realm;

// https://github.com/imrankst1221/Thermal-Printer-in-Android

public class PrinterActivity extends BaseActivity {
    private String TAG = "Main Activity";
    EditText message;
    Button btnPrint;

    byte FONT_TYPE;
    private static BluetoothSocket btsocket;
    private static OutputStream outputStream;

    Realm realm;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.printer_activity);
        message = (EditText)findViewById(R.id.txtMessage);
        btnPrint = (Button)findViewById(R.id.btnPrint);

        realm = realm.getDefaultInstance();

        setUpActionBar("Thermal Printer", getString(R.string.app_name), true);

        btnPrint.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                printDemo();
            }
        });

        // buscar o ultimo resultado de exames e os dados do usuário
        UserAccountRealm userAccountRealm = RealmUtilityUserAccount.find(realm, LocalPrefs.getPhoneNumber(PrinterActivity.this));

        // bp
        Bp bp = RealmUtilityBp.getLastSample(realm, LocalPrefs.getPhoneNumber(PrinterActivity.this));

        // bt
        Bt bt = RealmUtilityBt.getLastSample(realm, LocalPrefs.getPhoneNumber(PrinterActivity.this));

        // bg
        Bg bg = RealmUtilityBg.getLastSample(realm, LocalPrefs.getPhoneNumber(PrinterActivity.this));

        // spo2h
        SPO2H spo2H = RealmUtilitySPO2.getLastSample(realm, LocalPrefs.getPhoneNumber(PrinterActivity.this));

        // ecg
        ECG ecg = RealmUtilityECG.getLastSample(realm, LocalPrefs.getPhoneNumber(PrinterActivity.this));

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        Date now = new java.util.Date();
        String data = df.format("dd/MM/yyyy", now).toString();
        data = data + " as " + df.format("hh:mm",now).toString();

        String name = "";
        String cpf = "";
        String gender = "";
        String birth = "";
        if (userAccountRealm != null) {
            if (userAccountRealm.FullName != null) {
                name = userAccountRealm.FullName;
            }
            if (userAccountRealm.Cpf != null) {
                cpf = userAccountRealm.Cpf;
            }
            if(userAccountRealm.Gender != null) {
                gender = userAccountRealm.Gender;
            }
            if (userAccountRealm.Birthday != null) {
                birth = Utility.formatDate(userAccountRealm.Birthday,"yyyy-MM-dd","dd/MM/yyyy");
                if (!birth.isEmpty()) {
                    // calcula a idade (29a)
                    int age = Utility.getAge(userAccountRealm.Birthday,"yyyy-MM-dd");
                    birth = birth + "("+ String.valueOf(age) + "a)";
                }
            }
        }

        String bpValue = "";
        String spo2Value = "";
        String tempValue = "";
        String bgValue = "";

        String brValue = "";
        String ecgHrValue = "";
        String tsValue = "";
        String rrMinValue = "";
        String rrMaxValue = "";

        if (bp != null) {
            bpValue = String.valueOf(bp.getSbp()) + "/" + String.valueOf(bp.getDbp()) + "mmHg (" + String.valueOf(bp.getHr()) + "bpm)";
        }
        if (bt != null) {
            tempValue = String.valueOf(bt.getTemp()) + "oC";
        }
        if(spo2H != null) {
            spo2Value = String.valueOf(spo2H.getValue()) + "% (" + String.valueOf(spo2H.getHr())+"bpm)";
        }
        if (bg != null) {
            double mgdl = 18.018 * bg.getValue();
            int mgdlInt = (int)Math.round(mgdl);
            bgValue = String.valueOf(mgdlInt) + "mg/dL";
        }
        if(ecg != null) {
            // freq respiratoria
            brValue = String.valueOf(ecg.getBr()) + "rpm";

            // freq cardiaca
            ecgHrValue = String.valueOf(ecg.getHr())+"bpm";

            // variabilidade
            tsValue = String.valueOf(ecg.getTs())+"ms";

            //rr min
            rrMinValue = String.valueOf(ecg.getRrMin())+"ms";

            // rr max
            rrMaxValue = String.valueOf(ecg.getRrMax()) +"ms";
        }

        message.setText("Data: "+data+ "\nNome: "+name+"\nCPF: "+cpf+"\nSexo: "+gender+"\nDt.Nasc.: "+birth +"\n\nPRESSAO ARTERIAL  "+bpValue+"\nOXIMETRIA SPO2  "+spo2Value+"\nTEMPERATURA  "+tempValue+"\nGLICEMIA  "+bgValue+"\n\nELETROCARDIOGRAMA\nFreq.Cardiaca  "+ecgHrValue+"\nFreq.Respiratoria  "+brValue+"\nVariabilidade FC  "+tsValue+"\nR-R maximo  "+rrMaxValue+"\nR-R minimo  "+rrMinValue);
    }

    protected void printDemo() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH)
                        != PackageManager.PERMISSION_GRANTED  ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.BLUETOOTH,
                            Manifest.permission.BLUETOOTH_ADMIN,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    },
                    100);
        } else {
            if(btsocket == null){
                Intent BTIntent = new Intent(getApplicationContext(), DeviceList.class);
                this.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
            }
            else{
                OutputStream opstream = null;
                try {
                    opstream = btsocket.getOutputStream();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                outputStream = opstream;

                //print command
                try {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    outputStream = btsocket.getOutputStream();

                    byte[] printformat = { 0x1B, 0*21, FONT_TYPE };
                    //outputStream.write(printformat);

                    //print title
                    printUnicode();
                    printCustom("Exame",0,0);
                    printUnicode();
                    //print normal text
                    printCustom(message.getText().toString(),0,0);
                    printPhoto(R.drawable.mini_logo);
                    printNewLine();
                    printNewLine();

                    outputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void printBill() {
        if(btsocket == null){
            Intent BTIntent = new Intent(getApplicationContext(), DeviceList.class);
            this.startActivityForResult(BTIntent, DeviceList.REQUEST_CONNECT_BT);
        }
        else{
            OutputStream opstream = null;
            try {
                opstream = btsocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            outputStream = opstream;

            //print command
            try {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                outputStream = btsocket.getOutputStream();
                byte[] printformat = new byte[]{0x1B,0x21,0x03};
                outputStream.write(printformat);


                printCustom("Fair Group BD",2,1);
                printCustom("Pepperoni Foods Ltd.",0,1);
                printPhoto(R.drawable.logo);
                printCustom("H-123, R-123, Dhanmondi, Dhaka-1212",0,1);
                printCustom("Hot Line: +88000 000000",0,1);
                printCustom("Vat Reg : 0000000000,Mushak : 11",0,1);
                String dateTime[] = getDateTime();
                printText(leftRightAlign(dateTime[0], dateTime[1]));
                printText(leftRightAlign("Qty: Name" , "Price "));
                printCustom(new String(new char[32]).replace("\0", "."),0,1);
                printText(leftRightAlign("Total" , "2,0000/="));
                printNewLine();
                printCustom("Thank you for coming & we look",0,1);
                printCustom("forward to serve you again",0,1);
                printNewLine();
                printNewLine();

                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //print custom
    private void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B,0x21,0x03};  // 0- normal size text
        //byte[] cc1 = new byte[]{0x1B,0x21,0x00};  // 0- normal size text
        byte[] bb = new byte[]{0x1B,0x21,0x08};  // 1- only bold text
        byte[] bb2 = new byte[]{0x1B,0x21,0x20}; // 2- bold with medium text
        byte[] bb3 = new byte[]{0x1B,0x21,0x10}; // 3- bold with large text
        try {
            switch (size){
                case 0:
                    outputStream.write(cc);
                    break;
                case 1:
                    outputStream.write(bb);
                    break;
                case 2:
                    outputStream.write(bb2);
                    break;
                case 3:
                    outputStream.write(bb3);
                    break;
            }

            switch (align){
                case 0:
                    //left align
                    outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    outputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            outputStream.write(msg.getBytes());
            outputStream.write(PrinterCommands.LF);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print photo
    public void printPhoto(int img) {
        try {
            Bitmap bmp = BitmapFactory.decodeResource(getResources(),
                    img);
            if(bmp!=null){
                byte[] command = UtilsPrinter.decodeBitmap(bmp);
                outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText(command);
            }else{
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    //print unicode
    public void printUnicode(){
        try {
            outputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
            printText(UtilsPrinter.UNICODE_TEXT);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //print new line
    private void printNewLine() {
        try {
            outputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void resetPrint() {
        try{
            outputStream.write(PrinterCommands.ESC_FONT_COLOR_DEFAULT);
            outputStream.write(PrinterCommands.FS_FONT_ALIGN);
            outputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
            outputStream.write(PrinterCommands.ESC_CANCEL_BOLD);
            outputStream.write(PrinterCommands.LF);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print text
    private void printText(String msg) {
        try {
            // Print normal text
            outputStream.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print byte[]
    private void printText(byte[] msg) {
        try {
            // Print normal text
            outputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private String leftRightAlign(String str1, String str2) {
        String ans = str1 +str2;
        if(ans.length() <31){
            int n = (31 - str1.length() + str2.length());
            ans = str1 + new String(new char[n]).replace("\0", " ") + str2;
        }
        return ans;
    }


    private String[] getDateTime() {
        final Calendar c = Calendar.getInstance();
        String dateTime [] = new String[2];
        dateTime[0] = c.get(Calendar.DAY_OF_MONTH) +"/"+ c.get(Calendar.MONTH) +"/"+ c.get(Calendar.YEAR);
        dateTime[1] = c.get(Calendar.HOUR_OF_DAY) +":"+ c.get(Calendar.MINUTE);
        return dateTime;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
        try {
            if(btsocket!= null){
                outputStream.close();
                btsocket.close();
                btsocket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            btsocket = DeviceList.getSocket();
            if(btsocket != null){
                printText(message.getText().toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}