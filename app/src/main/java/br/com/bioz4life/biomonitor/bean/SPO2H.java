package br.com.bioz4life.biomonitor.bean;

// Model CSServerAPI

import java.util.Date;

/**
 * Created by ccl on 2017/2/7.
 */

public class SPO2H {

    private String id;
    // CountryCode+DDD+Phone
    private String user;
    private Date createdDate;
    private boolean isSync;

    private long ts = 0L;
    private long value = 0;
    private long hr = 0;

    public SPO2H() {
    }

    public SPO2H(long ts, long value, long hr) {
        this.ts = ts;
        this.value = value;
        this.hr = hr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isSync() {
        return isSync;
    }

    public void setSync(boolean sync) {
        isSync = sync;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
        //notifyChange();
    }

    public long getHr() {
        return hr;
    }

    public void setHr(long hr) {
        this.hr = hr;
        //notifyChange();
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public void reset() {
        value = 0;
        hr = 0;
        ts = 0L;
        //notifyChange();
    }

    @Override
    public String toString() {
        return "SPO2H{" +
                "value=" + value +
                ", hr=" + hr +
                ", ts=" + ts +
                '}';
    }

    public boolean isEmptyData() {
        return value == 0 || hr == 0 || ts == 0L;
    }
}
