package br.com.bioz4life.biomonitor.database;

import java.util.ArrayList;
import java.util.List;

import br.com.bioz4life.biomonitor.bean.Bg;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class RealmUtilityBg {

    public static List<Bg>getAll(Realm realm, String user){

        List<Bg> result = new ArrayList<>();
        RealmResults<BgRealm> realmRealmResults = realm
                .where(BgRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .findAll();

        if (realmRealmResults != null) {
            for(BgRealm item : realmRealmResults){
                Bg bg = new Bg();
                bg.setId(item.id);
                bg.setCreatedDate(item.createdDate);
                bg.setTs(item.ts);
                bg.setValue(item.value);
                result.add(bg);
            }
        }

        return result;
    }

    public static List<Bg> getLastSamplesLimit5(Realm realm, String user){

        List<Bg> result = new ArrayList<>();
        RealmResults<BgRealm> realmRealmResults = realm
                .where(BgRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .limit(5)
                .findAll();

        if (realmRealmResults != null) {
            for(BgRealm item : realmRealmResults){
                Bg bg = new Bg();
                bg.setId(item.id);
                bg.setCreatedDate(item.createdDate);
                bg.setTs(item.ts);
                bg.setValue(item.value);
                result.add(bg);
            }
        }

        return result;
    }

    public static Bg getLastSample(Realm realm, String user){

        Bg result = null;
        RealmResults<BgRealm> realmRealmResults = realm
                .where(BgRealm.class)
                .equalTo("user", user)
                .sort("createdDate", Sort.DESCENDING)
                .limit(1)
                .findAll();

        if (realmRealmResults != null) {
            for(BgRealm item : realmRealmResults){
                result = new Bg();
                result.setId(item.id);
                result.setCreatedDate(item.createdDate);
                result.setTs(item.ts);
                result.setValue(item.value);
            }
        }

        return result;
    }

    public static Bg getUnsyncBg(Realm realm, String user){
        Bg bg = null;
        BgRealm itemRealm = realm
                .where(BgRealm.class)
                .equalTo("user", user)
                .equalTo("isSync",false)
                .sort("createdDate", Sort.ASCENDING)
                .findFirst();

        if (itemRealm != null) {
            bg = new Bg();
            bg.setCreatedDate(itemRealm.createdDate);
            bg.setId(itemRealm.id);
            bg.setTs(itemRealm.ts);
            bg.setValue(itemRealm.value);
        }
        return bg;
    }

    public static void addFromServer(Realm realm, List<Bg> list, String user){
        if (list != null) {
            realm.beginTransaction();
            for(Bg c : list){
                BgRealm bgRealm = new BgRealm();

                bgRealm.id = c.getId();
                bgRealm.user = user;
                bgRealm.createdDate = c.getCreatedDate();
                bgRealm.value = c.getValue();
                bgRealm.ts = c.getTs();
                bgRealm.isSync = true;
                realm.copyToRealm(bgRealm);
            }
            realm.commitTransaction();
        }
    }

    public static void deleteDataSyncronized(Realm realm, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RealmResults<BgRealm> results = realm
                        .where(BgRealm.class)
                        .equalTo("user", user)
                        .equalTo("isSync",true)
                        .findAll();

                if (results != null) {
                    results.deleteAllFromRealm();
                }
            }
        });
    }

    public static void updateStatus(Realm realm, final String id, final String user){

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                BgRealm itemRealm = realm
                        .where(BgRealm.class)
                        .equalTo("id", id)
                        .equalTo("user", user)
                        .findFirst();

                if(itemRealm != null){
                    itemRealm.isSync = true;
                }
            }
        });
    }
}
