package br.com.bioz4life.biomonitor.hai.models;

import java.io.Serializable;
import java.util.ArrayList;

public class HaiData implements Serializable {
    public HaiObject DateTime;
    public HaiObjectBpm BP;
    public HaiObjectBpm SPO2;
    public HaiObject TEMP;
    public HaiObject GLUCOSE;
    public ArrayList<HaiObject> ECG;
}
