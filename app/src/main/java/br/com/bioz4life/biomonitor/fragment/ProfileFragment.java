package br.com.bioz4life.biomonitor.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.activity.AccountActivity;
import br.com.bioz4life.biomonitor.activity.ContactsActivity;
import br.com.bioz4life.biomonitor.activity.LauncherActivity;
import br.com.bioz4life.biomonitor.activity.PrivacyActivity;
import br.com.bioz4life.biomonitor.adapter.AdapterString;
import br.com.bioz4life.biomonitor.databinding.FragmentProfileBinding;
import br.com.bioz4life.biomonitor.utils.AlertDialogBuilder;
import br.com.bioz4life.biomonitor.utils.LocalPrefs;
import butterknife.BindView;

public class ProfileFragment extends BaseFragment implements AdapterString.OnClickItemProfile {


    @BindView(R.id.rvList)
    RecyclerView rvList;

    AdapterString adapterString;

    @Override
    public String getTitle() {
        return "Perfil";
    }

    @Override
    public void reset() {

    }

    @Override
    protected ViewDataBinding onCreateBindingView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentProfileBinding binding = setBindingContentView(inflater, R.layout.fragment_profile, container);

        return binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        List<String> arr = Arrays.asList(getResources().getStringArray(R.array.list_profile));

        adapterString = new AdapterString(mActivity,arr,this);

        rvList.setLayoutManager(new LinearLayoutManager(mActivity));
        rvList.setAdapter(adapterString);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClickProfile(int position) {
        switch (position){

            case 0:
                mActivity.startActivityForResult(new Intent(mActivity, AccountActivity.class),mActivity.REQUEST_ACTIVITY_ACCOUNT);
                break;
            case 1:
                mActivity.startActivityForResult(new Intent(mActivity, ContactsActivity.class),mActivity.REQUEST_ACTIVITY_MEMBERS);
                break;

            case 2:
                new AlertDialogBuilder(mActivity)
                        .setTitle("Confirmação")
                        .setMessage("Tem certeza que deseja fazer o logout?")
                        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                LocalPrefs.storePhoneNumber(mActivity,null);

                                Intent intent = new Intent(mActivity, LauncherActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create().show();
                break;
            case 3:
                mActivity.startActivity(new Intent(mActivity, PrivacyActivity.class));


                break;
            default:
                break;
        }
    }
}
