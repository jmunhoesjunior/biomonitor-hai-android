package br.com.bioz4life.biomonitor.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import br.com.bioz4life.biomonitor.R;
import br.com.bioz4life.biomonitor.bean.Bg;

public class AdapterBg extends RecyclerView.Adapter {

    private Context mContext;
    private List<Bg> mItems;

    private class Holder extends RecyclerView.ViewHolder {

        TextView tvData;
        TextView tvValues;
        TextView tvStatus;

        public Holder(@NonNull View itemView) {
            super(itemView);

            tvData = (TextView)itemView.findViewById(R.id.tvData);
            tvValues = (TextView) itemView.findViewById(R.id.tvValues);
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
        }
    }
    public AdapterBg(Context ctx, List<Bg> items){
        this.mContext = ctx;
        this.mItems = items;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(mContext)
                .inflate(R.layout.item_bg, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder,final int i) {

        Holder holder = (Holder)viewHolder;
        Bg bg = mItems.get(i);

        holder.tvStatus.setText("");
        holder.tvValues.setText("");
        holder.tvData.setText("");

        if (bg.getCreatedDate() != null) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM HH:mm");

            //to convert Date to String, use format method of SimpleDateFormat class.
            String strDate = dateFormat.format(bg.getCreatedDate());
            holder.tvData.setText(strDate);
        }

        holder.tvValues.setText(String.valueOf(bg.getValue()));

        String tempStatus = "";
        //double value = Utility.mmolToMg(bg.getValue());

        if(bg.getValue() >= 3.9 && bg.getValue() <= 5.5){
            tempStatus = "Normal";
        }
        else if(bg.getValue() >= 5.6 && bg.getValue() <= 7.0){
            tempStatus = "Pré-diabetes";
        }
        else if(bg.getValue() > 7.0){
            tempStatus = "Diabetes";
        }
        else{
            tempStatus = "Hipoglicemia";
        }
        holder.tvStatus.setText(tempStatus);

    }

    @Override
    public int getItemCount() {
        if(mItems == null)
            return 0;

        return mItems.size();
    }
}
